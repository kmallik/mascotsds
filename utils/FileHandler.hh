/* FileHandler.hh
 *
 *  Created by: Kaushik
 *  Date: 12/11/2019 */

#include "boost/lexical_cast.hpp"
#include <cstring>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <locale>
#include <map>
#include <sstream>
#include <stack>
#include <sys/stat.h>
#include <unordered_set>
#include <vector>

/*! Read a member from a file. (A member is an attribute whose value is a scalar.)
 * \param[in] filename  Name of the file
 * \param[in] member_value  Reference to the variable that will contain the read member value
 * \param[in] member_name     The name of the member whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template <class T>
int readMember(const std::string &filename,
               T &member_value,
               const std::string &member_name) {
    std::ifstream file;
    file.open(filename);
    std::string line;
    if (file.is_open()) {
        while (std::getline(file, line)) {
            /* check if a match is found with the member_name */
            if (line.find(member_name) != std::string::npos) {
                /* the data is in the next line*/
                if (std::getline(file, line)) {
                    std::istringstream stream(line);
                    stream >> member_value;
                } else {
                    try {
                        throw std::runtime_error("FileHandler:readMember: Unable to read data member");
                    } catch (std::exception &e) {
                        std::cout << e.what() << "\n";
                        return 0;
                    }
                }
                file.close();
                return 1;
            }
        }
        /* while loop exited and data member was not found */
        try {
            std::string msg = "FileHandler:readMember: " + member_name + " not found in file " + filename;
            throw std::runtime_error(msg);
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            std::string msg = "FileHandler:readMember: Unable to open input file " + filename;
            throw std::runtime_error(msg);
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Extract the content within the outermost braces
 *  \param[in] data       the input data
 *  \param[in] extracted_data             the extracted data
 *  \param[out] pos         the position of the closing brace, and is equal to -1 if a matching closing brace was not found  */
int getDataWithinBraces(const std::string &data, std::string &data_extracted) {
    std::stack<char> matching_braces; /* stack to remember the unmatched braces */
    bool flag = false;
    int pos = -1;
    for (size_t i = 0; i < data.size(); i++) {
        if (data[i] == '{') {
            matching_braces.push('{');
            if (!flag) {
                flag = true;
                continue;
            }
        } else if (data[i] == '}') {
            matching_braces.pop();
            pos = i;
        }
        if (flag) {
            if (matching_braces.size() == 0) {
                break;
            } else {
                data_extracted += data[i];
            }
        }
    }
    if (matching_braces.size() != 0) {
        try {
            throw std::runtime_error("FileHandler:getDataWithinBraces: Unmatched braces.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return -1;
        }
    }
    return pos;
}

/* removes the spaces and the newlines from a given string object */
inline void clean(std::string &data) {
    data.erase(std::remove(data.begin(), data.end(), '\n'), data.end()); /* remove the newline characters */
    data.erase(std::remove(data.begin(), data.end(), ' '), data.end());  /* remove the blank spaces */
}

/*! Read 1-dimensional vector from file
 * \param[in] filename  Name of the file
 * \param[in] v                  Reference to the vector that will contain the read vector value
 * \param[in] vec_name     The name of the vector whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template <class T>
int readVec(const std::string &filename,
            std::vector<T> &v,
            const std::string &vec_name) {
    std::ifstream file;
    file.open(filename);
    // get length of file from this point:
    file.seekg(0, file.end);
    int length = file.tellg();
    file.seekg(0, file.beg);
    v.clear(); /* the previous data in v is over-written */
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line, '{')) {             /* read upto '{' in a line */
            if (line.find(vec_name) != std::string::npos) { /* if this part contains the vector name then the vector content follows as a comma-separated list within braces */

                char *buffer = new char[length];

                // read data as a block:
                file.seekg((int)file.tellg() - 1);
                file.read(buffer, length);

                std::string data(buffer); /* the string containing the data of the rest of the file */
                std::string data_extracted;
                getDataWithinBraces(data, data_extracted);          /* the extracted data within the outermost matched braces*/
                std::stringstream extracted_stream(data_extracted); /* convert to a stream object */
                std::string value;
                while (getline(extracted_stream, value, ',')) /* register each of the entries one by one */
                {
                    clean(value);
                    v.push_back(boost::lexical_cast<T>(value));
                }
                file.close();
                delete[] buffer;
                return 1;
            }
        }
        /* while loop exited and the vec was not found */
        try {
            throw std::runtime_error("FileHandler:readVec: Vector not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readVec: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Read 2-dimensional vector from file
 * \param[in] filename  Name of the file
 * \param[in] v                  Reference to the vector that will contain the read vector value
 * \param[in] vec_name     The name of the vector whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template <class T>
int readVecVec(const std::string &filename,
               std::vector<std::vector<T>> &vec,
               const std::string &vec_name) {
    std::ifstream file;
    file.open(filename);
    // get length of file from this point:
    file.seekg(0, file.end);
    int length = file.tellg();
    file.seekg(0, file.beg);
    vec.clear(); /* the previous data in v is over-written */
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line, '{')) {             /* read upto '{' in a line */
            if (line.find(vec_name) != std::string::npos) { /* if this part contains the vector name then the vector content follows as a comma-separated list within braces */

                char *buffer = new char[length];

                // read data as a block:
                file.seekg((int)file.tellg() - 1);
                file.read(buffer, length);

                std::string data(buffer); /* the string containing the data of the rest of the file */
                clean(data);
                std::string data_extracted;
                getDataWithinBraces(data, data_extracted); /* the extracted data within the outermost matched braces*/

                while (data_extracted.size() != 0) {
                    std::string d;
                    size_t pos = getDataWithinBraces(data_extracted, d);
                    data_extracted = data_extracted.substr(pos + 1, std::string::npos);

                    std::stringstream extracted_stream(d); /* convert to a stream object */
                    std::string value;

                    std::vector<T> v;
                    while (getline(extracted_stream, value, ',')) /* register each of the entries one by one */
                    {
                        v.push_back(boost::lexical_cast<T>(value));
                    }
                    vec.push_back(v);
                }

                file.close();
                delete[] buffer;
                return 1;
            }
        }
        /* while loop exited and the vec was not found */
        try {
            throw std::runtime_error("FileHandler:readVec: Vector not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readVec: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Read 2-dimensional vector from file
 * \param[in] filename  Name of the file
 * \param[in] v                  Reference to the vector that will contain the read vector value
 * \param[in] vec_name     The name of the vector whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template <class T, std::size_t SIZE>
int readVecArr(const std::string &filename,
               std::vector<std::array<T, SIZE>> &vec,
               const std::string &vec_name) {
    std::ifstream file;
    file.open(filename);
    // get length of file from this point:
    file.seekg(0, file.end);
    int length = file.tellg();
    file.seekg(0, file.beg);
    vec.clear(); /* the previous data in v is over-written */
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line, '{')) {             /* read upto '{' in a line */
            if (line.find(vec_name) != std::string::npos) { /* if this part contains the vector name then the vector content follows as a comma-separated list within braces */

                char *buffer = new char[length];

                // read data as a block:
                file.seekg((int)file.tellg() - 1);
                file.read(buffer, length);

                std::string data(buffer); /* the string containing the data of the rest of the file */
                clean(data);
                std::string data_extracted;
                getDataWithinBraces(data, data_extracted); /* the extracted data within the outermost matched braces*/

                while (data_extracted.size() != 0) {
                    std::string d;
                    int pos = getDataWithinBraces(data_extracted, d);
                    if (pos == -1)
                        break;
                    data_extracted = data_extracted.substr(pos + 1, std::string::npos);

                    std::stringstream extracted_stream(d); /* convert to a stream object */
                    std::string value;

                    std::array<T, SIZE> arr;
                    size_t ind = 0;
                    while (getline(extracted_stream, value, ',')) /* register each of the entries one by one */
                    {
                        arr[ind] = boost::lexical_cast<T>(value);
                        ind++;
                    }
                    vec.push_back(arr);
                }

                file.close();
                delete[] buffer;
                return 1;
            }
        }
        /* while loop exited and the vec was not found */
        try {
            throw std::runtime_error("FileHandler:readVec: Vector not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readVec: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Read 3-dimensional vector from file
 * \param[in] filename  Name of the file
 * \param[in] v                  Reference to the vector that will contain the read vector value
 * \param[in] vec_name     The name of the vector whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template <class T, std::size_t SIZE>
int readVecArrVec(const std::string &filename,
                  std::vector<std::array<std::vector<T>, SIZE>> &vec,
                  const std::string &vec_name) {
    std::ifstream file;
    file.open(filename);
    // get length of file from this point:
    file.seekg(0, file.end);
    int length = file.tellg();
    file.seekg(0, file.beg);
    vec.clear(); /* the previous data in v is over-written */
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line, '{')) {             /* read upto '{' in a line */
            if (line.find(vec_name) != std::string::npos) { /* if this part contains the vector name then the vector content follows as a comma-separated list within braces */

                char *buffer = new char[length];

                // read data as a block:
                file.seekg((int)file.tellg() - 1);
                file.read(buffer, length);

                std::string data(buffer); /* the string containing the data of the rest of the file */
                clean(data);
                std::string data_extracted;
                getDataWithinBraces(data, data_extracted); /* the extracted data within the outermost matched braces*/

                while (data_extracted.size() != 0) {
                    std::string data_extracted_inner;
                    size_t pos = getDataWithinBraces(data_extracted, data_extracted_inner);
                    data_extracted = data_extracted.substr(pos + 1, std::string::npos);

                    std::array<std::vector<T>, SIZE> arr;
                    size_t ind = 0;

                    while (data_extracted_inner.size() != 0) {
                        std::string d;
                        size_t pos_inner = getDataWithinBraces(data_extracted_inner, d);
                        data_extracted_inner = data_extracted_inner.substr(pos_inner + 1, std::string::npos);

                        std::stringstream extracted_stream(d); /* convert to a stream object */
                        std::string value;
                        std::vector<T> v;
                        while (getline(extracted_stream, value, ',')) /* register each of the entries one by one */
                        {
                            v.push_back(boost::lexical_cast<T>(value));
                        }
                        arr[ind] = v;
                        ind++;
                    }
                    vec.push_back(arr);
                }

                file.close();
                delete[] buffer;
                return 1;
            }
        }
        /* while loop exited and the vec was not found */
        try {
            throw std::runtime_error("FileHandler:readVec: Vector not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readVec: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Read 1-dimensional native array from file
 * \param[in] filename  Name of the file
 * \param[in] arr                  Reference to the array that will contain the read array value
 * \param[in] arr_name     The name of the array whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template <class T>
int readArr(const std::string &filename, T arr[], const std::string &vec_name) {
    std::ifstream file;
    file.open(filename);
    // get length of file from this point:
    file.seekg(0, file.end);
    int length = file.tellg();
    file.seekg(0, file.beg);
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line, '{')) {             /* read upto '{' in a line */
            if (line.find(vec_name) != std::string::npos) { /* if this part contains the vector name then the vector content follows as a comma-separated list within braces */

                char *buffer = new char[length];

                // read data as a block:
                file.seekg((int)file.tellg() - 1);
                file.read(buffer, length);

                std::string data(buffer); /* the string containing the data of the rest of the file */
                std::string data_extracted;
                getDataWithinBraces(data, data_extracted);          /* the extracted data within the outermost matched braces*/
                std::stringstream extracted_stream(data_extracted); /* convert to a stream object */
                std::string value;
                size_t index = 0;
                while (getline(extracted_stream, value, ',')) /* register each of the entries one by one */
                {
                    clean(value);
                    arr[index] = boost::lexical_cast<T>(value);
                    index++;
                }
                file.close();
                delete[] buffer;
                return 1;
            }
        }
        /* while loop exited and the vec was not found */
        try {
            throw std::runtime_error("FileHandler:readVArr: Array not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readArr: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Read block of data as string object, ignoring the newlines
 * \param[in] filename  Name of the file
 * \param[in] str                  Reference to the string object
 * \param[in] str_name     The name of the object whose value is to be read
 * \param[in] str_begin     A string marking the beginning of the block of data to be read
 * \param[in] str_end           A string marking the end of the block of the data to be read
 */
void readStrBlock(const std::string &filename,
                  std::string &str,
                  const std::string &str_begin,
                  const std::string &str_end) {
    std::ifstream file;
    file.open(filename);
    std::string str_file;
    str.clear(); /* the previous data in v is over-written */
    if (file.is_open()) {
        std::ostringstream ss;
        ss << file.rdbuf();
        str_file = ss.str();
        size_t begin_index = str_file.find(str_begin);
        if (str_file.find(str_begin) != std::string::npos) {
            begin_index += str_begin.size();
        } else {
            throw std::runtime_error("Beginning of string block not marked.");
        }
        size_t end_index = str_file.find(str_end);
        if (str_file.find(str_end) == std::string::npos) {
            throw std::runtime_error("Ending of string block not marked.");
        }
        str = str_file.substr(begin_index, end_index - begin_index);
    } else {
        throw std::runtime_error("Unable to open input file.");
    }
}

/*! Read a std::map from a file.
 * \param[in] filename  Name of the file
 * \param[in] map_data  Reference to the variable that will contain the map
 * \param[in] map_start     The starting of the map
 * \param[in] map_end         The end of the map
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template <class T1, class T2>
int readMap(const std::string &filename,
            std::map<T1, T2> &map_data,
            const std::string &map_start,
            const std::string &map_end) {
    std::ifstream file;
    file.open(filename);
    std::string line;
    if (file.is_open()) {
        bool done = false;
        while (std::getline(file, line) && !done) {
            /* check if the map begins from this line */
            if (line.find(map_start) != std::string::npos) {
                /* the data starts from the next line*/
                std::string line2;
                while (std::getline(file, line2, ':')) {
                    /* check if map has ended */
                    if (line2.find(map_end) != std::string::npos) {
                        done = true;
                        break;
                    }
                    clean(line2);
                    T1 key = boost::lexical_cast<T1>(line2);
                    T2 val;
                    std::getline(file, line2);
                    clean(line2);
                    val = boost::lexical_cast<T2>(line2);
                    map_data.insert({key, val});
                }
            }
        }
        file.close();
        return 1;
    } else {
        try {
            throw std::runtime_error("FileHandler:readMember: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Read a std::map from a file.
 * \param[in] filename  Name of the file
 * \param[in] map_data  Reference to the variable that will contain the map
 * \param[in] map_start     The starting of the map
 * \param[in] map_end         The end of the map
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template <class T1, class T2>
int readMap(const std::string &filename,
            std::map<T1, std::vector<T2>> &map_data,
            const std::string &map_start,
            const std::string &map_end) {
    std::ifstream file;
    file.open(filename);
    std::string line;
    if (file.is_open()) {
        bool done = false;
        while (std::getline(file, line) && !done) {
            /* check if the map begins from this line */
            if (line.find(map_start) != std::string::npos) {
                /* the data starts from the next line*/
                std::string line2;
                while (std::getline(file, line2, ':')) {
                    /* check if map has ended */
                    if (line2.find(map_end) != std::string::npos) {
                        done = true;
                        break;
                    }
                    clean(line2);
                    T1 key = boost::lexical_cast<T1>(line2);
                    std::vector<T2> val;
                    std::getline(file, line2);
                    std::string data_extracted;
                    getDataWithinBraces(line2, data_extracted);         /* the extracted data within the outermost matched braces*/
                    std::stringstream extracted_stream(data_extracted); /* convert to a stream object */
                    std::string value;
                    while (getline(extracted_stream, value, ',')) /* register each of the entries one by one */
                    {
                        clean(value);
                        val.push_back(boost::lexical_cast<T2>(value));
                    }
                    map_data.insert({key, val});
                }
            }
        }
        file.close();
        return 1;
    } else {
        try {
            throw std::runtime_error("FileHandler:readMember: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Create a file OR erase previous data written to a file
 * \param[in] filename  The name of the file*/
void create(const std::string &filename) {
    std::ofstream file;
    file.open(filename, std::ofstream::out | std::ofstream::trunc);
    if (file.is_open()) {
        file.close();
    }
}
/* some functions for writing data to file */
/*! Write a member to a file. (A member is an attribute whose value is a scalar.)
 * \param[in] filename  Name of the file
 * \param[in] member_name     The name of the member
 * \param[in] member_value   The value of the member
 * \param[in] mode                     [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template <class T>
void writeMember(const std::string &filename, const std::string &member_name, T member_value, const char *mode = "a") {
    std::ofstream file;
    if (!strcmp(mode, "a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode, "w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeMember: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }

    if (file.is_open()) {
        file << "# " << member_name << "\n";
        file << member_value << "\n";
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeMember: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write 1-dimensional integer vector to file
 * \param[in] filename  Name of the file
 * \param[in] vec_name     The name of the vector to be written
 * \param[in] v                     The vector
 * \param[in] mode              [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template <class T>
void writeVec(const std::string &filename, const std::string &vec_name, std::vector<T> &v, const char *mode = "a") {
    std::ofstream file;
    if (!strcmp(mode, "a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode, "w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVec: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << vec_name << "\n";
        for (size_t i = 0; i < v.size(); i++) {
            file << v[i] << "\n";
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVec: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write 1-dimensional vector of pointers to file
 * \param[in] filename  Name of the file
 * \param[in] vec_name     The name of the vector
 * \param[in] v                    The vector
 * \param[in] mode             [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template <class T>
void writeVec(const std::string &filename, const std::string &vec_name, std::vector<T *> &v, const char *mode = "a") {
    std::ofstream file;
    if (!strcmp(mode, "a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode, "w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVec: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << vec_name << "\n";
        for (size_t i = 0; i < v.size(); i++) {
            file << *v[i] << "\n";
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVec: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write 1-dimensional integer set (unordered) to file
 * \param[in] filename  Name of the file
 * \param[in] set_name     The name of the set
 * \param[in] s                    The set
 * \param[in] mode              [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template <class T>
void writeSet(const std::string &filename, const std::string &set_name, std::unordered_set<T> &s, const char *mode = "a") {
    std::ofstream file;
    if (!strcmp(mode, "a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode, "w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeSet: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << set_name << "\n";
        for (auto i = s.begin(); i != s.end(); ++i) {
            file << *i << "\n";
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeSet: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write array of vectors (can be thought of as a 2-d table) from file
 * \param[in] filename  Name of the file
 * \param[in] arr_name     The name of the array
 * \param[in] arr                The array
 * \param[in] mode              [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template <class T>
void writeArrVec(const std::string &filename, const std::string &arr_name, std::vector<T> **arr, size_t no_elem, const char *mode = "a") {
    std::ofstream file;
    if (!strcmp(mode, "a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode, "w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeArrVec: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << arr_name << "\n";
        for (size_t i = 0; i < no_elem; i++) {
            if (arr[i]->size() == 0) {
                file << "x\n";
            } else {
                for (size_t j = 0; j < arr[i]->size(); j++) {
                    file << (*arr[i])[j] << " ";
                }
                file << "\n";
            }
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeArrVec: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write array of unordered sets (can be thought of as a 2-d table) to file
 * \param[in] filename  Name of the file
 * \param[in] arr_name     The name of the array
 * \param[in] arr                 The array
 * \param[in] mode                [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template <class T>
void writeArrSet(const std::string &filename, const std::string &arr_name, std::unordered_set<T> **arr, size_t no_elem, const char *mode = "a") {
    std::ofstream file;
    if (!strcmp(mode, "a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode, "w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeArrSet: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << arr_name << "\n";
        for (size_t i = 0; i < no_elem; i++) {
            if (arr[i]->size() == 0) {
                file << "x\n";
            } else {
                for (auto it = arr[i]->begin(); it != arr[i]->end(); it++) {
                    file << (*it) << " ";
                }
                file << "\n";
            }
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeArrVec: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write vector of references to unordered sets (can be thought of as a 2-d table) to file
 * \param[in] filename  Name of the file
 * \param[in] vec_name     The name of the vector
 * \param[in] vec                The vector
 * \param[in] mode                     [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template <class T>
void writeVecSet(const std::string &filename, const std::string &vec_name, std::vector<std::unordered_set<T> *> vec, const char *mode = "a") {
    std::ofstream file;
    if (!strcmp(mode, "a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode, "w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVecSet: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << vec_name << "\n";
        for (size_t i = 0; i < vec.size(); i++) {
            if (vec[i]->size() == 0) {
                file << "x\n";
            } else {
                for (auto it = vec[i]->begin(); it != vec[i]->end(); it++) {
                    file << (*it) << " ";
                }
                file << "\n";
            }
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVecSet: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}
