/*
 * vehicle_dyn_2d.cc
 *
 *  created on: 17.02.2021
 *      author: kaushik
 */

/*!
 * a c++ function containing the decomposition function of a
 * simple vehicle dynamics (sampled time) with bounded external noise
 *
 * \param[in] u     control input
 * \param[in] w     environmental input (like opening/closing of a given door)
 * \param[in] z1   state coordinate of one of the corners of a given state hyper-rectangle
 * \param[in] z2   state coordinate of one of the corners of a given state hyper-rectangle
 *
 */

#include <cmath>
#include <vector>

#define tau 0.2
//#define eps 0.000001

extern "C" {

std::vector<double> vehicle_dyn(std::vector<double> &u,
                                std::vector<double> &w,
                                std::vector<double> &z1,
                                std::vector<double> &z2) {
    std::vector<double> y(3);
    y[0] = z1[0] + u[0] * tau;
    y[1] = z1[1] + u[1] * tau;
    y[2] = w[0];

    // if (z1[0] < z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
    //     /* return the lower left corner of the reachable set approximation */
    //     y[2] = w[0];
    // } else { /* z1 is the upper bound and z2 is the lower bound */
    //     /* return the upper right corner of the reachable set approximation */
    //     y[2] = w[0];
    // }
    return y;
}
}
