/*
 * vehicle_dyn_3d.cc
 *
 *  created on: 17.02.2021
 *      author: kaushik
 */

/*!
 * a c++ function containing the decomposition function of a
 * Dubins vehicle dynamics (sampled time) with bounded external noise
 *
 * \param[in] u     control input
 * \param[in] w     environmental input (like opening/closing of a given door)
 * \param[in] z1   state coordinate of one of the corners of a given state hyper-rectangle
 * \param[in] z2   state coordinate of one of the corners of a given state hyper-rectangle
 *
 */

#include <algorithm>
#include <cmath>
#include <vector>

#define tau 0.2
//#define V 1

extern "C" {

std::vector<double> vehicle_dyn(std::vector<double> &u,
                                std::vector<double> &w,
                                std::vector<double> &z1,
                                std::vector<double> &z2) {
    std::vector<double> y(4);
    double V = 1.0;
    double S = u[0];

    std::vector<double> list0, list1;
    list0.push_back(V * cos(z1[2]) * tau);
    list0.push_back(V * cos(z2[2]) * tau);
    list1.push_back(V * sin(z1[2]) * tau);
    list1.push_back(V * sin(z2[2]) * tau);
    if ((z1[2] < 0 && z2[2] > 0) ||
        (z1[2] > 0 && z2[2] < 0)) {
        list0.push_back(V * tau);
    }
    if ((z1[2] < M_PI / 2 && z2[2] > M_PI / 2) ||
        (z1[2] > M_PI / 2 && z2[2] < M_PI / 2)) {
        list1.push_back(V * tau);
    }
    if ((z1[2] < -M_PI / 2 && z2[2] > -M_PI / 2) ||
        (z1[2] > -M_PI / 2 && z2[2] < -M_PI / 2)) {
        list1.push_back(-V * tau);
    }
    if ((z1[2] < M_PI && z2[2] > M_PI) ||
        (z1[2] > M_PI && z2[2] < M_PI)) {
        list0.push_back(-V * tau);
    }
    if ((z1[2] < -M_PI && z2[2] > -M_PI) ||
        (z1[2] > -M_PI && z2[2] < -M_PI)) {
        list0.push_back(-V * tau);
    }

    /* next convert z1[2] and z2[2] to be within -pi and +pi */
    if (z1[2] > M_PI) {
        double rem = fmod(z1[2], M_PI);
        z1[2] = -M_PI + rem;
    }
    if (z1[2] < -M_PI) {
        double rem = fmod(-z1[2], M_PI);
        z1[2] = M_PI - rem;
    }
    if (z2[2] > M_PI) {
        double rem = fmod(z2[2], M_PI);
        z2[2] = -M_PI + rem;
    }
    if (z2[2] < -M_PI) {
        double rem = fmod(-z2[2], M_PI);
        z2[2] = M_PI - rem;
    }

    if (z1[0] < z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
        /* return the lower left corner of the reachable set approximation */
        y[0] = z1[0] + *std::min_element(std::begin(list0), std::end(list0));
        y[1] = z1[1] + *std::min_element(std::begin(list1), std::end(list1));
        y[2] = z1[2] + S * tau;
        y[3] = w[0];
    } else { /* z1 is the upper bound and z2 is the lower bound */
        /* return the upper right corner of the reachable set approximation */
        y[0] = z1[0] + *std::max_element(std::begin(list0), std::end(list0));
        y[1] = z1[1] + *std::max_element(std::begin(list1), std::end(list1));
        y[2] = z1[2] + S * tau;
        y[3] = w[0];
    }
    return y;
}
}
