import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle

def draw_rectangle(corners, color):
    x = corners[0]
    y = corners[2]
    width = corners[1] - corners[0]
    height = corners[3] - corners[2]
    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='k', facecolor=color)
    return rect

plt.close('all')
fig = plt.figure(num = 'MASCOT_SIMULATION')
# plt.show(block=False)
# plt.ion()
plt.pause(1)

if not fig.get_axes():
    ax = fig.add_subplot(111)
else:
    ax = fig.get_axes()[0]

ax.set_xlim(0, 2)
ax.set_ylim(0, 2)

obstacle = draw_rectangle([ 0.8, 1.2, 0.8, 1.2], 'k')
ax.add_patch(obstacle)
# obstacle = draw_rectangle([ 5.0, 6.0, 0.0,2.0], 'k')
# ax.add_patch(obstacle)
kitchen = draw_rectangle([ 0.0, 0.5, 0.0, 0.5], 'g')
ax.add_patch(kitchen)
table = draw_rectangle([ 1.5, 2.0, 1.5, 2.0], 'b')
ax.add_patch(table)
print("plotting state space done")

with open('state_space.pickle', 'wb') as f:
    pickle.dump(fig, f)
    print("state space plot saved")
plt.close()
