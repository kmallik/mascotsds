#!/bin/bash

for spec in {1..2}
do
	for abs in {1..6}
	do
		for mode in {0..1}
		do
			{ env time -v ../../../build/bin/MascotSDS ./switch_abs_${abs}_spec_${spec}.cfg ${mode} ; } 2>> switch.log
		done
	done
done

