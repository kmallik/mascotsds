/*
 * switch_dyn.cc
 *
 *  created on: 17.02.2021
 *      author: kaushik
 */

/*!
 * a c++ function containing the decomposition function of a
 * switched oscillator dynamics (sampled time) with bounded external noise
 * taken from: https://arxiv.org/pdf/2001.09236.pdf
 *
 * \param[in] u     control input
 * \param[in] z1   state coordinate of one of the corners of a given state hyper-rectangle
 * \param[in] z2   state coordinate of one of the corners of a given state hyper-rectangle
 *
 */

#include <cmath>
#include <vector>

extern "C" {
std::vector<double> switch_dyn(std::vector<double> &u,
                               std::vector<double> &w,
                               std::vector<double> &z1,
                               std::vector<double> &z2) {
    /* ******* the system parameters ********* */
    /* sampling time */
    const double tau = 0.05;
    /* constants */
    const double a = 1.3;
    const double b = 0.25;
    const double eps = 0.000001; /* a small constant smaller than the state space discretization */
    /* the output: the next state coordinates */
    std::vector<double> y(2);

    if ((1 - a * tau) >= 0) {
        y[0] = z1[0] + (-a * z1[0] + z1[1]) * tau + u[0] - 0.3;
    } else {
        y[0] = z2[0] + (-a * z2[0] + z1[1]) * tau + u[0] - 0.3;
    }
    if ((1 - b * tau) >= 0) {
        y[1] = z1[1] + ((pow(z1[0], 2) / (pow(z1[0], 2) + 1)) - b * z1[1]) * tau + u[1] - 0.3;
    } else {
        y[1] = z2[1] + ((pow(z1[0], 2) / (pow(z1[0], 2) + 1)) - b * z2[1]) * tau + u[1] - 0.3;
    }
    /* trajectories are maintained in the boundary of the domain */
    y[0] = std::min(y[0], 4.0 - 0.1 - eps);
    y[0] = std::max(y[0], 0.0 + 0.1 + eps);
    y[1] = std::min(y[1], 4.0 - 0.1 - eps);
    y[1] = std::max(y[1], 0.0 + 0.1 + eps);
    
    w[0] = w[0];
    w[1] = w[1];

    return y;
}
}
