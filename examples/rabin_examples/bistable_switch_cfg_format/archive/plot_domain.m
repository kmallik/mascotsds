%
%   plot_domain.m
%
%   created on: 05.02.2021
%       author: kaushik
%
%   this program plots the over and under-approximation of winning region
%
%   you need to run ./switch binary first
%   so that the controller bdds are generated
%   

% path of the SymbolicSet.m and the associated mex-file
addpath('/home/mrychlicki/Desktop/mascot-sds/mfiles/');
addpath('/home/mrychlicki/Desktop/mascot-sds/build/bin');

% load the over and the under-approximations of the winning regions
cu=SymbolicSet('../../../build/data/controllers/C1.bdd','projection',[1 2]);
co=SymbolicSet('../../../build/data/controllers_over/C1.bdd','projection',[1 2]);

% prepare the figure window
figure;
hold on;

%plot the state space
X = SymbolicSet('../../../build/data/X.bdd');
plotCells(X,'FaceColor','r');

% plot the over and the under-approximation
% try
%     plotCells(co,'FaceColor','y');
% catch
%     warning(['Points could no be loaded from C' num2str(ii) '.bdd\n']);
% end
% 
% 
% try
%     plotCells(cu,'FaceColor','g');
% catch
%     warning(['Points could no be loaded from C' num2str(ii) '.bdd\n']);
% end
    

