#!/bin/bash

for spec in {1..2}
do
	for abs in {3..4}
	do
		echo "START Rabin Pair ${spec} with cudd, experiment: ./switch_abs_${abs}_spec_${spec}.cfg"
		{ env time -v ../../../build/bin/Synthesize ./switch_abs_${abs}_spec_${spec}.cfg 0 ; } 2>> switch.log >> inside_cav23_short.txt
		echo "START Rabin Pair ${spec} with sylvan, experiment: ./switch_abs_${abs}_spec_${spec}.cfg"
		{ env time -v ../../../build/bin/Synthesize ./switch_abs_${abs}_spec_${spec}.cfg 1 ; } 2>> switch.log >> inside_cav23_short.txt 
	done
done

