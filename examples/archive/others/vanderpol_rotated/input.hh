/* Header file for inputs */
/* Created by: Kaushik */
/*      Date: 02/09/2019 */

#ifndef INPUT_HH_
#define INPUT_HH_

/* transformations for rotation of the coordinate plane */
const double transform[4] = {1, -1, 1, 1};
const double inv_transform[4] = {0.5, 0.5, -0.5, 0.5};

/* state space dim */
#define sDIM 2
#define iDIM 1

/* data types for the ode solver */
typedef std::array<double, sDIM> state_type;
typedef std::array<double, iDIM> input_type;

/* sampling time */
const double tau = 0.1;
/* upper bounds on the noise which is considered to be centered around the origin */
const state_type W_ub = {0.02, 0.02};

/* parallel or sequential implementation of computation of the
transition functions */
const bool parallel = false;

/* The following function is a hacky version of the decomposition function for a mixed monotone system, written solely for the purpose of use of the more accurate reachable set approximation of the van der pol example */
auto decomposition = [](input_type &u, state_type &z1, state_type &z2) -> state_type {
    state_type y;
    state_type x1 = z1;
    state_type x2 = z2;
    /* inverse transform the states to obtain the 'normal' vander pol states */
    state_type temp1;
    temp1[0] = inv_transform[0] * x1[0] + inv_transform[1] * x1[1];
    temp1[1] = inv_transform[2] * x1[0] + inv_transform[3] * x1[1];
    x1 = temp1;
    temp1[0] = inv_transform[0] * x2[0] + inv_transform[1] * x2[1];
    temp1[1] = inv_transform[2] * x2[0] + inv_transform[3] * x2[1];
    x2 = temp1;
    /* for minimization and maximization of the second state variable,
     * we evaluate the r.h.s. in six points */
    auto rhs = [](double y1, double y2) -> double {
        return (y2 + tau * y2 * (1 - pow(y1, 2)) - tau * y1);
    };
    std::vector<double> list;
    list.push_back(rhs(x1[0], x1[1]));
    list.push_back(rhs(x1[0], x2[1]));
    list.push_back(rhs(x2[0], x1[1]));
    list.push_back(rhs(x2[0], x2[1]));
    double temp = -1 / (2 * x2[0]);
    if (x1[0] <= temp && x2[0] >= temp && x2[0] != 0) {
        list.push_back(rhs(temp, x2[0]));
    }
    temp = -1 / (2 * x2[1]);
    if (x1[0] <= temp && x2[0] >= temp && x2[1] != 0) {
        list.push_back(rhs(temp, x2[1]));
    }
    if (x1[0] <= x2[0]) { /* x1 is the lower bound and x2 is the upper bound */
        /* return the lower left corner of the reachable set approximation */
        y[0] = x1[0] + tau * x1[1];
        y[1] = *std::min_element(std::begin(list), std::end(list));
    } else { /* x1 is the upper bound and x2 is the lower bound */
        /* return the upper right corner of the reachable set approximation */
        y[0] = x1[0] + tau * x1[1];
        y[1] = *std::max_element(std::begin(list), std::end(list));
    }
    /* transform y back to the rotated van der pol states */
    state_type q;
    q[0] = transform[0] * y[0] + transform[1] * y[1];
    q[1] = transform[2] * y[0] + transform[3] * y[1];
    /* to silence the warning message */
    (void)u;
    return q;
};

/* lower bounds of the hyper rectangle */
double lb[sDIM] = {-5.0, -5.0};
/* upper bounds of the hyper rectangle */
double ub[sDIM] = {5.0, 5.0};
/* grid node distance diameter */
double eta[sDIM] = {0.02, 0.02};

/* input space parameters */
double ilb[iDIM] = {-0.5};
double iub[iDIM] = {0.5};
double ieta[iDIM] = {1};

/* target states */
double h[4] = {2.4, -1.8, 5.8, -4.0};
// double h[4] = {4.0,0.0,4.0,0.0};

#endif /* INPUT_HH_ */
