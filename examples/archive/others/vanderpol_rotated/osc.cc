/*
 * osc.cc
 *
 *  created on: 02.09.2019
 *      author: kaushik
 */

/*
 * 45 deg rotated van der pol  oscillator with bounded noise
 *
 */

#include <array>
#include <iostream>

#include "cuddObj.hh"

#include "SymbolicSet.hh"
// #include "SymbolicModelGrowthBound.hh"
#include "SymbolicModelMonotonic.hh"

#include "FixedPoint.hh"
#include "RungeKutta4.hh"
#include "input.hh"
#include "utils/TicToc.hh"


/* The variables sDIM, iDIM, tau, W_ub are read from input.hh */
/* The types state_type and input_type are defined in input.hh */

/****************************************************************************/
/* main computation */
/****************************************************************************/
int main() {
    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    Cudd mgr;
    /* create a log file */
    std::freopen("osc.log", "w", stderr);
    std::clog << "osc.log" << '\n';

    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from input.hh */
    mascot::SymbolicSet ss(mgr, sDIM, lb, ub, eta);
    ss.addGridPoints();
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from input.hh */
    mascot::SymbolicSet is(mgr, iDIM, ilb, iub, ieta);
    is.addGridPoints();

    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    mascot::SymbolicSet sspost(ss, 1); /* create state space for post variables */
    // mascot::SymbolicModelGrowthBound<state_type,input_type> abstraction(&ss, &is, &sspost);
    mascot::SymbolicModelMonotonic<state_type, input_type> abstraction(&ss, &is, &sspost);
    /* compute the transition relation */
    timer.tic();
    // abstraction.computeTransitionRelation(osc_post, radius_post, W_ub);
    abstraction.computeTransitions(decomposition, W_ub);
    std::cout << std::endl;
    double time_abstraction = timer.toc();
    std::clog << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
    /* get the number of elements in the transition relation */
    std::clog << std::endl
              << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
    std::clog << std::endl
              << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;

    /****************************************************************************/
    /* we continue with the controller synthesis for GF (target) */
    /****************************************************************************/
    /* construct an under and over approximating SymbolicSet for target (it is a subset of the state space)  */
    mascot::SymbolicSet target_under(ss), target_over(ss);
    /* add inner approximation of P={ x | H x<= h } form state space */
    double H[4 * sDIM] = {-1, -1,
                          1, 1,
                          1, -1,
                          -1, 1};
    /* h is loaded from input.hh */
    target_under.addPolytope(4, H, h, mascot::INNER);
    target_over.addPolytope(4, H, h, mascot::OUTER);
    std::cout << "Target set details:" << std::endl;
    target_under.writeToFile("osc_target_under.bdd");
    target_over.writeToFile("osc_target_over.bdd");

    /* we setup a fixed point object to compute reach and stay controller */
    mascot::FixedPoint fp(&abstraction);
    std::cout << "FixedPoint initialized." << '\n';
    // /* the fixed point algorithm operates on the BDD directly */
    // BDD T = target.getSymbolicSet();
    int verbose = 2; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */


    /****************************************************************************/
    /* Over-approximation of almost sure winning region */
    /****************************************************************************/
    timer.tic();
    BDD WO = fp.BuchiOver(target_over.getSymbolicSet(), verbose);
    mascot::SymbolicSet winning_over(ss);
    winning_over.setSymbolicSet(WO);
    std::clog << "Over-approximation Domain size: " << winning_over.getSize() << std::endl;
    double time_over_approx = timer.toc();
    std::clog << "Time taken by the over-approximation computation: " << time_over_approx << "s.\n";
    winning_over.writeToFile("osc_winning_over.bdd");

    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    timer.tic();
    BDD CU = fp.BuchiUnder(target_under.getSymbolicSet(), mgr.bddOne(), verbose);
    mascot::SymbolicSet controller_under(ss, is);
    controller_under.setSymbolicSet(CU);
    std::clog << "Under-approximation Domain size: " << controller_under.getSize() << std::endl;
    controller_under.writeToFile("osc_controller_under.bdd");
    double time_under_approx = timer.toc();
    std::clog << "Time taken by the under-approximation computation: " << time_under_approx << "s.\n";
    /****************************************************************************/
    /* We also compute the buchi controller when the noise is interpreted
   * as the worst case adversary */
    /****************************************************************************/
    timer.tic();
    BDD C_wc = fp.BuchiWC(target_under.getSymbolicSet(), verbose);
    mascot::SymbolicSet controller_wc(ss, is);
    controller_wc.setSymbolicSet(C_wc);
    std::clog << "Worst case controller domain size: " << controller_wc.getSize() << std::endl;
    controller_wc.writeToFile("osc_controller_wc.bdd");
    double time_worst_case = timer.toc();
    std::clog << "Time taken by the worst case computation: " << time_worst_case << "s.\n";
    /****************************************************************************/
    /* append the results in the log file */
    /****************************************************************************/
    /* yet to come */

    return 1;
}
