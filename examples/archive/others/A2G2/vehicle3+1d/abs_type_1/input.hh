/* Header file for inputs */
/* Created by: Kaushik */
/*      Date: 16/10/2019 */

#ifndef INPUT_HH_
#define INPUT_HH_

#include <float.h>

/* state space dim */
#define sDIM 4
#define iDIM 1

/* data types for the ode solver */
typedef std::array<double, sDIM> state_type;
typedef std::array<double, iDIM> input_type;
typedef std::array<double, 2> state_xy_dim_type;

/* parallel or sequential implementation of computation of the
 transition functions */
const bool parallel = false;
int verbose = 1; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */
/* read transitions from file flag */
const bool RFF = true;
///* scaling */
//const double scale = 1.5;
/* sampling time */
const double tau = 0.1;
/* upper bounds on the noise which is considered to be centered around the origin */
//const state_type W_ub = {0.06,0.06,0.06,0.0,0.0};
/* no disturbance */
const state_type W_ub = {0.06, 0.06, 0.06, 0.0};
const double rad_reach_boolean_dim = 0.25; /* the radius of the reachable set from the state dimensions 3 and 4*/
const double eps = 10e-6;                  /* some small number for tolerance of numerical errors */
/* velocity */
const double V = 1;
double offset[sDIM] = {-0.6, -0.2, 0.0, 0.0};
/* there is an intentional overlap between the two ends in the 3rd dimension to allow out-of-bound behavior */
/* IMPORTANT: the overlap amount is ad-hoc and needs to be updated with eta */
/* lower bounds of the hyper rectangle */
double lb[sDIM] = {-0.6, -1.2, -M_PI - 0.46, -0.5};
/* upper bounds of the hyper rectangle */
double ub[sDIM] = {0.96, 1.98, M_PI + 0.46, 2.0};
/* grid node distance diameter */
double eta[sDIM] = {0.1, 0.1, 0.1, 1.0};

/* input space parameters */
double ilb[iDIM] = {-20.0};
double iub[iDIM] = {20.0};
double ieta[iDIM] = {2.0};

/* goal states */
/* add inner approximation of P={ x | H x<= h } form state space */
/* office */
double H1[4 * sDIM] = {
        -1,
        0,
        0,
        0,
        1,
        0,
        0,
        0,
        0,
        -1,
        0,
        0,
        0,
        1,
        0,
        0,
};
double g1[6] = {-0.20, 0.80, -1.20, 1.80};
///* entire state space */
//double g1[6] = {-lb[0],ub[0],-lb[1],ub[1],0.49,1.51};
/* kitchen */
double H2[4 * sDIM] = {
        -1,
        0,
        0,
        0,
        1,
        0,
        0,
        0,
        0,
        -1,
        0,
        0,
        0,
        1,
        0,
        0,
};
double g2[4] = {-0.20 - offset[0], 0.80 + offset[0], 0.70 - offset[1], -0.10 + offset[1]};
///* entire state space */
//double g2[4] = {-lb[0],ub[0],-lb[1],ub[1]};

/* static obstacle set */
double HO[4 * sDIM] = {-1, 0, 0, 0,
                       1, 0, 0, 0,
                       0, -1, 0, 0,
                       0, 1, 0, 0};
double ho1[4] = {-1.15, 1.19, -0.9, 1.7};
double ho2[4] = {-1.15, 1.2, -1.7, 2.0};
///* no obstacles */
//double ho1[4] = {0,0,0,0};
//double ho2[4] = {0,0,0,0};
/* dynamic obstacle: closed door */
double HDO[6 * sDIM] = {-1, 0, 0, 0,
                        1, 0, 0, 0,
                        0, -1, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 0, -1,
                        0, 0, 0, 1};
double ho3[6] = {0 - offset[0], 1.1 + offset[0], -0.85 - offset[1], 0.9 + offset[1], 0.49 - offset[2], 0.49 + offset[2]};
///* no door */
//double ho3[6] = {0,0,0,0,0.49,0.49};
/* assumptions */
/* coffee appears */
//double HC[2*sDIM]={0, 0, 0, 0, -1,
//    0, 0, 0, 0, 1};
//double a1[2] = {-0.51,1.49};
/* door opens */
double HD[2 * sDIM] = {0, 0, 0, -1,
                       0, 0, 0, 1};
double a2[2] = {-0.51, 1.49};

auto check_intersection = [](state_xy_dim_type lb1, state_xy_dim_type ub1, state_xy_dim_type lb2, state_xy_dim_type ub2) -> bool {
    bool is_intersection = true; /* boolean flag which is positive if there is an intersection */
    /* if there is no intersection in either the first or the second state dimension, then there is no intersection */
    for (int i = 0; i < 2; i++) {
        if ((lb1[i] - ub2[i] > eps) || (lb2[i] - ub1[i] > eps)) {
            is_intersection = false;
            break;
        }
    }
    return is_intersection;
};

/* returns true if [lb2,ub2] is included in [lb1,ub1] */
auto check_inclusion = [](state_xy_dim_type lb1, state_xy_dim_type ub1, state_xy_dim_type lb2, state_xy_dim_type ub2) -> bool {
    bool is_included = true; /* boolean flag which is positive if there is an intersection */
    /* if there is no intersection in either the first or the second state dimension, then there is no intersection */
    for (int i = 0; i < 2; i++) {
        if ((lb1[i] - lb2[i] > eps) || (ub2[i] - ub1[i] > eps)) {
            is_included = false;
            break;
        }
    }
    return is_included;
};

auto decomposition = [](input_type &u, state_type &z1, state_type &z2) -> state_type {
    /* when the boolean flags are 2 then ignore (just make a self loop) */
    if ((abs(0.5 * (z1[3] + z2[3]) - 2.0) <= eps) || (abs(0.5 * (z1[4] + z2[4]) - 2.0) <= eps)) {
        state_type y = z1;
        return y;
    }
    /* first convert z1[2] to be within -pi and +pi */
    if (z1[2] > M_PI) {
        double rem = fmod(z1[2], M_PI);
        z1[2] = -M_PI + rem;
    }
    if (z1[2] < -M_PI) {
        double rem = fmod(-z1[2], M_PI);
        z1[2] = M_PI - rem;
    }
    auto rhs0 = [](double x3, input_type &u) -> double {
        double r;
        if (u[0] != 0) {
            r = V * sin(x3 + u[0] * tau) / u[0] - V * sin(x3) / u[0];
        } else {
            r = V * cos(x3) * tau;
        }
        return r;
    };
    auto rhs1 = [](double x3, input_type &u) -> double {
        double r;
        if (u[0] != 0) {
            r = -V * cos(x3 + u[0] * tau) / u[0] + V * cos(x3) / u[0];
        } else {
            r = -V * sin(x3) * tau;
        }
        return r;
    };
    std::vector<double> list0, list1;
    list0.push_back(z1[0] + rhs0(z1[2], u));
    list0.push_back(z1[0] + rhs0(z2[2], u));
    list0.push_back(z2[0] + rhs0(z1[2], u));
    list0.push_back(z2[0] + rhs0(z2[2], u));

    list1.push_back(z1[1] + rhs1(z1[2], u));
    list1.push_back(z1[1] + rhs1(z2[2], u));
    list1.push_back(z2[1] + rhs1(z1[2], u));
    list1.push_back(z2[1] + rhs1(z2[2], u));
    if (u[0] != 0) {
        double alpha0 = atan2((cos(u[0] * tau) - 1), sin(u[0] * tau));
        if ((alpha0 >= z1[2] && alpha0 <= z2[2]) ||
            (alpha0 <= z1[2] && alpha0 >= z2[2])) {
            list0.push_back(z1[0] + rhs0(alpha0, u));
        }
        double alpha1 = atan2(sin(u[0] * tau), (1 - cos(u[0] * tau)));
        if ((alpha1 >= z1[2] && alpha1 <= z2[2]) ||
            (alpha1 <= z1[2] && alpha1 >= z2[2])) {
            list1.push_back(z1[1] + rhs1(alpha1, u));
        }
    } else {
        double z2_min = std::min(z1[2], z2[2]);
        double z2_max = std::max(z1[2], z2[2]);
        if (0 >= z2_min && 0 <= z2_max) {
            list0.push_back(z1[0] + V * tau);
            list1.push_back(z1[1]);
        }
        if ((-M_PI >= z2_min && -M_PI <= z2_max) ||
            (M_PI >= z2_min && M_PI <= z2_max)) {
            list0.push_back(z1[0] - V * tau);
            list1.push_back(z1[1]);
        }
        /* extremum occurs for cos(x[2])=0 */
        if (M_PI / 2 >= z2_min && M_PI / 2 <= z2_max) {
            list0.push_back(z1[0]);
            list1.push_back(z1[1] - V * tau);
        }
        if (-M_PI / 2 >= z2_min && -M_PI / 2 <= z2_max) {
            list0.push_back(z1[0]);
            list1.push_back(z1[1] + V * tau);
        }
    }
    state_type y;
    if (z1[0] < z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
        /* return the lower left corner of the reachable set approximation */
        y[0] = *std::min_element(std::begin(list0), std::end(list0));
        y[1] = *std::min_element(std::begin(list1), std::end(list1));
        y[2] = z1[2] + u[0] * tau;
    } else { /* z1 is the upper bound and z2 is the lower bound */
        /* return the upper right corner of the reachable set approximation */
        y[0] = *std::max_element(std::begin(list0), std::end(list0));
        y[1] = *std::max_element(std::begin(list1), std::end(list1));
        y[2] = z1[2] + u[0] * tau;
    }
    //    /* update the flags */
    //    if ((z1[3]!=z2[3]) | (z1[4]!=z2[4])) {
    //        std::runtime_error("The two diagonal vertices of the cube should agree on the status of the environment flags.");
    //    }
    /* the X-Y dimension of the corners of the current state */
    state_xy_dim_type z1_xy_dim, z2_xy_dim;
    z1_xy_dim[0] = z1[0];
    z1_xy_dim[1] = z1[1];
    z2_xy_dim[0] = z2[0];
    z2_xy_dim[1] = z2[1];
    if (abs(z1[3] + z2[3]) <= eps) {
        /* if the door is closed, it may open or close in the next time step */
        if (z1[0] < z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
            /* return the lower left corner of the reachable set approximation */
            y[3] = 0.5 - rad_reach_boolean_dim;
        } else { /* z1 is the upper bound and z2 is the lower bound */
            /* return the upper right corner of the reachable set approximation */
            y[3] = 0.5 + rad_reach_boolean_dim;
        }
    } else if (abs(0.5 * (z1[3] + z2[3]) - 1.0) <= eps) {
        //        /* the hyperrectangle representing the location of the office */
        //        state_xy_dim_type lb1, ub1;
        //        lb1[0]=-g1[0];
        //        lb1[1]=-g1[2];
        //        ub1[0]=g1[1];
        //        ub1[1]=g1[3];
        /* the hyperrectangle representing the location of the kitchen */
        state_xy_dim_type lb2, ub2;
        lb2[0] = -g2[0];
        lb2[1] = -g2[2];
        ub2[0] = g2[1];
        ub2[1] = g2[3];
        /* when the door opens, it remains open until the robot reaches the kitchen */
        if (z1[0] < z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
            if (check_inclusion(lb2, ub2, z1_xy_dim, z2_xy_dim)) {
                y[3] = 0.0 - rad_reach_boolean_dim;
            } else {
                y[3] = 1.0 - rad_reach_boolean_dim;
            }
        } else { /* z1 is the upper bound and z2 is the lower bound */
            /* return the upper right corner of the reachable set approximation */
            if (check_inclusion(lb2, ub2, z2_xy_dim, z1_xy_dim)) {
                y[3] = 0.0 + rad_reach_boolean_dim;
            } else {
                y[3] = 1.0 + rad_reach_boolean_dim;
            }
        }
    }
    //    if (abs(z1[4]+z2[4])<=eps) {
    //        /* if there is no request, a request may appear any time */
    //        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
    //            /* return the lower left corner of the reachable set approximation */
    //            y[4] = 0.5-rad_reach_boolean_dim;
    //        } else { /* z1 is the upper bound and z2 is the lower bound */
    //            /* return the upper right corner of the reachable set approximation */
    //            y[4] = 0.5+rad_reach_boolean_dim;
    //        }
    //    } else if (abs(0.5*(z1[4]+z2[4])-1.0)<=eps) {
    //        /* the hyperrectangle representing the location of the request */
    //        state_xy_dim_type lb1, ub1;
    //        lb1[0]=-g1[0];
    //        lb1[1]=-g1[2];
    //        ub1[0]=g1[1];
    //        ub1[1]=g1[3];
    //        /* when a request appears, it remains active until the request is served */
    //        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
    //            /* return the lower left corner of the reachable set */
    //            if (check_inclusion(lb1,ub1,z1_xy_dim,z2_xy_dim)) {
    //                y[4]=0-rad_reach_boolean_dim;
    //            } else {
    //                y[4]=1-rad_reach_boolean_dim;
    //            }
    //        } else { /* z1 is the upper bound and z2 is the lower bound */
    //            /* return the upper right corner of the reachable set approximation */
    //            if (check_inclusion(lb1,ub1,z2_xy_dim,z1_xy_dim)) {
    //                /* return the lower left corner of the hyper-rectangle */
    //                y[4]=0+rad_reach_boolean_dim;
    //            } else {
    //                y[4]=1+rad_reach_boolean_dim;
    //            }
    //        }
    //    }

    return y;
};

#endif /* INPUT_HH_ */
