/*
 * vehicle.cc
 *
 *  created on: 16.10.2019
 *      author: kaushik
 */

/*
 * dubin's vehicle (sampled time) with bounded external noise
 *
 */

#include <array>
#include <iostream>
#include <math.h>

#include "cuddObj.hh"

#include "SymbolicSet.hh"
// #include "SymbolicModelGrowthBound.hh"
#include "SymbolicModelMonotonic.hh"

#include "FixedPoint.hh"
#include "RungeKutta4.hh"
#include "input.hh"
#include "utils/TicToc.hh"


/****************************************************************************/
/* main computation */
/****************************************************************************/
int main() {
    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    Cudd mgr;
    /* create a log file */
    std::freopen("vehicle.log", "w", stderr);
    std::clog << "vehicle.log" << '\n';

    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from input.hh */
    mascot::SymbolicSet ss(mgr, sDIM, lb, ub, eta);
    ss.addGridPoints();
    /* debug */
    ss.writeToFile("X.bdd");
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from input.hh */
    mascot::SymbolicSet is(mgr, iDIM, ilb, iub, ieta);
    is.addGridPoints();

    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    mascot::SymbolicSet sspost(ss, 1); /* create state space for post variables */
    mascot::SymbolicModelMonotonic<state_type, input_type> abstraction(&ss, &is, &sspost);
    if (RFF) {
        std::cout << "Reading transition relations from file." << '\n';
        mascot::SymbolicSet mt(mgr, "maybeTransitions.bdd");
        mascot::SymbolicSet st(mgr, "sureTransitions.bdd");
        abstraction.setTransitionRelations(mt.getSymbolicSet(), st.getSymbolicSet());
        /* get the number of elements in the transition relation */
        std::cout << std::endl
                  << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl
                  << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
    } else {
        /* compute the transition relation */
        timer.tic();
        abstraction.computeTransitions(decomposition, W_ub);
        std::cout << std::endl;
        double time_abstraction = timer.toc();
        std::clog << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
        /* get the number of elements in the transition relation */
        std::cout << std::endl
                  << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl
                  << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        /* save transition relations */
        (abstraction.getMaybeTransitions()).writeToFile("maybeTransitions.bdd");
        (abstraction.getSureTransitions()).writeToFile("sureTransitions.bdd");
    }

    /****************************************************************************/
    /* we continue with the controller synthesis for
     *    GF A1 -> GF G1 & GF G2
     */
    /****************************************************************************/
    /* construct an underapproximating SymbolicSet for the guarantees (it is a subset of the state space)  */
    mascot::SymbolicSet G1(ss), G2(ss);
    //    G1.setSymbolicSet(mgr.bddOne());
    //    G2.setSymbolicSet(mgr.bddOne());
    /* h is loaded from input.hh */
    G1.addPolytope(4, H1, g1, mascot::INNER);
    G2.addPolytope(4, H2, g2, mascot::INNER);
    // std::cout << "Target set details:" << std::endl;
    G1.writeToFile("G1.bdd");
    G2.writeToFile("G2.bdd");
    std::cout << "Guarantee sets wrote to files." << std::endl;

    /* construct an over-approximation of the avoid set */
    mascot::SymbolicSet avoid_over(ss);
    //    avoid_over.addPolytope(4,HO,ho1,mascot::OUTER);
    //    avoid_over.addPolytope(4,HO,ho2,mascot::OUTER);
    avoid_over.addPolytope(6, HDO, ho3, mascot::OUTER);
    avoid_over.writeToFile("vehicle_avoid_over.bdd");
    std::cout << "Avoid set wrote to file." << std::endl;

    /* construct an over-approximation of the assumptions */
    mascot::SymbolicSet A1(ss);
    A1.addPolytope(2, HD, a2, mascot::OUTER);
    A1.writeToFile("A1.bdd");
    std::cout << "Assumption sets wrote to file." << std::endl;

    /* we setup a fixed point object to compute reach and stay controller */
    mascot::FixedPoint fp(&abstraction);
    std::cout << "FixedPoint initialized." << '\n';
    // /* the fixed point algorithm operates on the BDD directly */

    std::vector<BDD> A;
    std::vector<BDD> G;
    A = {A1.getSymbolicSet()};
    G = {G1.getSymbolicSet(), G2.getSymbolicSet()};
    /* trivial spec */
    //    A = {mgr.bddZero(),mgr.bddZero()};
    //    G = {mgr.bddOne(),mgr.bddOne()};
    /****************************************************************************/
    /* Over-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of over-approximation of the a.s. controller.\n";
    timer.tic();
    std::vector<BDD> CO = fp.GR1("over", A, G, mgr.bddOne(), avoid_over.getSymbolicSet(), verbose);
    double time_over_approx = timer.toc();
    mascot::SymbolicSet controller_over_0(ss, is), controller_over_1(ss, is);
    controller_over_0.setSymbolicSet(CO[0]);
    controller_over_1.setSymbolicSet(CO[1]);
    controller_over_0.writeToFile("vehicle_controller_over_0.bdd");
    controller_over_1.writeToFile("vehicle_controller_over_1.bdd");
    std::clog << "Time taken by the over-approximation computation: " << time_over_approx << "s.\n";
    std::clog << "Number of states in the over-approximation: " << CO[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;
    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of under-approximation of the a.s. controller.\n";
    timer.tic();
    std::vector<BDD> CU = fp.GR1("under", A, G, controller_over_0.getSymbolicSet(), avoid_over.getSymbolicSet(), verbose);
    double time_under_approx = timer.toc();
    mascot::SymbolicSet controller_under_0(ss, is), controller_under_1(ss, is);
    controller_under_0.setSymbolicSet(CU[0]);
    controller_under_1.setSymbolicSet(CU[1]);
    controller_under_0.writeToFile("vehicle_controller_under_0.bdd");
    controller_under_1.writeToFile("vehicle_controller_under_1.bdd");
    std::clog << "Time taken by the under-approximation computation: " << time_under_approx << "s.\n";
    std::clog << "Number of states in the under-approximation: " << CU[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;
    std::clog << "Absolute volume of the error region: " << (CO[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) - CU[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars())) * eta[0] * eta[1] * eta[2] << std::endl;
    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of under-approximation of the worst case controller.\n";
    timer.tic();
    std::vector<BDD> CWC = fp.GR1("wc", A, G, controller_over_0.getSymbolicSet(), avoid_over.getSymbolicSet(), verbose);
    double time_wc = timer.toc();
    mascot::SymbolicSet controller_wc_0(ss, is), controller_wc_1(ss, is);
    controller_wc_0.setSymbolicSet(CWC[0]);
    controller_wc_1.setSymbolicSet(CWC[1]);
    controller_wc_0.writeToFile("vehicle_controller_wc_0.bdd");
    controller_wc_1.writeToFile("vehicle_controller_wc_1.bdd");
    std::clog << "Time taken by the worst case winning region computation: " << time_wc << "s.\n";
    std::clog << "Number of states in the worst-case winning region: " << CWC[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;

    return 1;
}
