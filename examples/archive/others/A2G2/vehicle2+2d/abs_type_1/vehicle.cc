/*
 * vehicle.cc
 *
 *  created on: 13.10.2020
 *      author: kaushik
 */

/*
 * dubin's vehicle (sampled time) with bounded external noise
 *
 */

#include <array>
#include <iostream>
#include <math.h>

#include "cuddObj.hh"

#include "SymbolicSet.hh"
// #include "SymbolicModelGrowthBound.hh"
#include "SymbolicModelMonotonic.hh"

#include "FixedPoint.hh"
#include "RungeKutta4.hh"
#include "input.hh"
#include "utils/TicToc.hh"


/****************************************************************************/
/* main computation */
/****************************************************************************/
int main() {
    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    Cudd mgr;
    /* create a log file */
    std::freopen("vehicle.log", "w", stderr);
    std::clog << "vehicle.log" << '\n';

    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from input.hh */
    mascot::SymbolicSet ss(mgr, sDIM, lb, ub, eta);
    ss.addGridPoints();
    /* debug */
    ss.printInfo();
    ss.writeToFile("X.bdd");
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from input.hh */
    mascot::SymbolicSet is(mgr, iDIM, ilb, iub, ieta);
    is.addGridPoints();
    /* debug */
    is.writeToFile("U.bdd");

    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    mascot::SymbolicSet sspost(ss, 1); /* create state space for post variables */
    mascot::SymbolicModelMonotonic<state_type, input_type> abstraction(&ss, &is, &sspost);
    if (RFF) {
        std::cout << "Reading transition relations from file." << '\n';
        mascot::SymbolicSet mt(mgr, "maybeTransitions.bdd");
        mascot::SymbolicSet st(mgr, "sureTransitions.bdd");
        abstraction.setTransitionRelations(mt.getSymbolicSet(), st.getSymbolicSet());
        /* get the number of elements in the transition relation */
        std::cout << std::endl
                  << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl
                  << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
    } else {
        /* compute the transition relation */
        timer.tic();
        abstraction.computeTransitions(decomposition, W_ub);
        std::cout << std::endl;
        double time_abstraction = timer.toc();
        std::clog << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
        /* get the number of elements in the transition relation */
        std::cout << std::endl
                  << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl
                  << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        /* save transition relations */
        (abstraction.getMaybeTransitions()).writeToFile("maybeTransitions.bdd");
        (abstraction.getSureTransitions()).writeToFile("sureTransitions.bdd");
    }

    /****************************************************************************/
    /* we continue with the controller synthesis for
     *    GF A1 & GF A2 -> GF G1 & GF G2
     */
    /****************************************************************************/
    /* construct an underapproximating SymbolicSet for the guarantees (it is a subset of the state space)  */
    mascot::SymbolicSet G1(ss), G2(ss);
    //    G1.setSymbolicSet(mgr.bddOne());
    //    G2.setSymbolicSet(mgr.bddOne());
    /* h is loaded from input.hh */
    G1.addPolytope(6, H1, g1, mascot::INNER);
    G2.addPolytope(4, H2, g2, mascot::INNER);
    // std::cout << "Target set details:" << std::endl;
    G1.writeToFile("G1.bdd");
    G2.writeToFile("G2.bdd");
    std::cout << "Guarantee sets wrote to files." << std::endl;

    /* construct an over-approximation of the avoid set */
    mascot::SymbolicSet avoid_over(ss);
    avoid_over.addPolytope(4, HO, ho1, mascot::OUTER);
    avoid_over.addPolytope(4, HO, ho2, mascot::OUTER);
    avoid_over.addPolytope(6, HDO, ho3, mascot::OUTER);
    avoid_over.writeToFile("vehicle_avoid_over.bdd");
    std::cout << "Avoid set wrote to file." << std::endl;

    /* construct an over-approximation of the assumptions */
    mascot::SymbolicSet A1(ss), A2(ss);
    A1.addPolytope(2, HC, a1, mascot::OUTER);
    A2.addPolytope(2, HD, a2, mascot::OUTER);
    A1.writeToFile("A1.bdd");
    A2.writeToFile("A2.bdd");
    std::cout << "Assumption sets wrote to file." << std::endl;

    /* we setup a fixed point object to compute reach and stay controller */
    mascot::FixedPoint fp(&abstraction);
    std::cout << "FixedPoint initialized." << '\n';
    // /* the fixed point algorithm operates on the BDD directly */


    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of under-approximation of the a.s. controller.\n";
    std::vector<BDD> A, G;
    //    A = {mgr.bddZero(),mgr.bddZero()};
    A = {A1.getSymbolicSet(), A2.getSymbolicSet()};
    //    G = {mgr.bddZero(), ss.getSymbolicSet()};
    G = {G1.getSymbolicSet(), G2.getSymbolicSet()};
    timer.tic();
    std::vector<BDD> CU = fp.GR1("under", A, G, mgr.bddOne(), avoid_over.getSymbolicSet(), verbose);
    mascot::SymbolicSet controller_under_0(ss, is), controller_under_1(ss, is);
    controller_under_0.setSymbolicSet(CU[0]);
    controller_under_1.setSymbolicSet(CU[1]);
    //   std::clog << "Under-approximation Domain size: " << controller_under.getSize() << std::endl;
    //    std::clog << "Under-approximation Domain size: " << CU.ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;
    controller_under_0.writeToFile("vehicle_controller_under_0.bdd");
    controller_under_1.writeToFile("vehicle_controller_under_1.bdd");
    double time_under_approx = timer.toc();
    std::clog << "Time taken by the under-approximation computation: " << time_under_approx << "s.\n";

    return 1;
}
