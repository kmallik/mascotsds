/*
 * vehicle.cc
 *
 *  created on: 16.10.2019
 *      author: kaushik
 */

/*
 * dubin's vehicle (sampled time) with bounded external noise
 *
 */

#include <array>
#include <iostream>
#include <math.h>

#include "cuddObj.hh"

#include "SymbolicSet.hh"
// #include "SymbolicModelGrowthBound.hh"
#include "SymbolicModelMonotonic.hh"

#include "FixedPoint.hh"
#include "RungeKutta4.hh"
#include "input.hh"
#include "utils/TicToc.hh"


/****************************************************************************/
/* main computation */
/****************************************************************************/
int main() {
    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    Cudd mgr;
    /* create a log file */
    std::freopen("vehicle.log", "w", stderr);
    std::clog << "vehicle.log" << '\n';

    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from input.hh */
    mascot::SymbolicSet ss(mgr, sDIM, lb, ub, eta);
    ss.addGridPoints();
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from input.hh */
    mascot::SymbolicSet is(mgr, iDIM, ilb, iub, ieta);
    is.addGridPoints();

    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    mascot::SymbolicSet sspost(ss, 1); /* create state space for post variables */
    mascot::SymbolicModelMonotonic<state_type, input_type> abstraction(&ss, &is, &sspost);
    if (RFF) {
        std::cout << "Reading transition relations from file." << '\n';
        mascot::SymbolicSet mt(mgr, "maybeTransitions.bdd");
        mascot::SymbolicSet st(mgr, "sureTransitions.bdd");
        abstraction.setTransitionRelations(mt.getSymbolicSet(), st.getSymbolicSet());
    } else {
        /* compute the transition relation */
        timer.tic();
        abstraction.computeTransitions(decomposition, W_ub);
        std::cout << std::endl;
        double time_abstraction = timer.toc();
        std::clog << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
        /* get the number of elements in the transition relation */
        std::clog << std::endl
                  << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::clog << std::endl
                  << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        /* save transition relations */
        (abstraction.getMaybeTransitions()).writeToFile("maybeTransitions.bdd");
        (abstraction.getSureTransitions()).writeToFile("sureTransitions.bdd");
    }

    /****************************************************************************/
    /* we continue with the controller synthesis for GF (target) */
    /****************************************************************************/
    /* construct an under and over approximating SymbolicSet for target (it is a subset of the state space)  */
    mascot::SymbolicSet target_under(ss), target_over(ss);
    /* h is loaded from input.hh */
    target_under.addPolytope(4, H, h, mascot::INNER);
    target_over.addPolytope(4, H, h, mascot::OUTER);
    // std::cout << "Target set details:" << std::endl;
    target_under.writeToFile("vehicle_target_under.bdd");
    target_over.writeToFile("vehicle_target_over.bdd");
    std::cout << "Target set wrote to files." << std::endl;

    /* construct an under and over-approximation of the avoid set */
    mascot::SymbolicSet avoid_under(ss), avoid_over(ss);
    avoid_under.addPolytope(4, HO, ho1, mascot::INNER);
    // avoid_under.addPolytope(4,HO,ho2,mascot::INNER);
    avoid_over.addPolytope(4, HO, ho1, mascot::OUTER);
    // avoid_over.addPolytope(4,HO,ho2,mascot::OUTER);
    avoid_under.writeToFile("vehicle_avoid_under.bdd");
    avoid_over.writeToFile("vehicle_avoid_over.bdd");
    std::cout << "Avoid set wrote to files." << std::endl;

    /* we setup a fixed point object to compute reach and stay controller */
    mascot::FixedPoint fp(&abstraction);
    std::cout << "FixedPoint initialized." << '\n';
    // /* the fixed point algorithm operates on the BDD directly */
    int verbose = 1; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */

    /* the specification needs to be passed as a GR1 spec. */
    std::vector<BDD> A, G; /* A is the set of assumptions and G is the set of guarantees*/
    A = {mgr.bddOne()};    /* for Buchi spec, the assumption is trivially true */

    /****************************************************************************/
    /* Over-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of over-approximation of the a.s. controller domain.\n";
    G = {target_over.getSymbolicSet()}; /* the guarantee is the target */
    timer.tic();
    std::vector<BDD> WO = fp.GR1("over", A, G, mgr.bddOne(), avoid_under.getSymbolicSet(), verbose);
    mascot::SymbolicSet winning_over(ss);
    winning_over.setSymbolicSet(WO[0]);
    std::clog << "Over-approximation Domain size: " << winning_over.getSize() << std::endl;
    double time_over_approx = timer.toc();
    std::clog << "Time taken by the over-approximation computation: " << time_over_approx << "s.\n";
    winning_over.writeToFile("vehicle_winning_over.bdd");

    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of under-approximation of the a.s. controller.\n";
    G = {target_under.getSymbolicSet()}; /* the guarantee is the target */
    timer.tic();
    std::vector<BDD> CU = fp.GR1("under", A, G, winning_over.getSymbolicSet(), avoid_over.getSymbolicSet(), verbose);
    mascot::SymbolicSet controller_under(ss, is);
    controller_under.setSymbolicSet(CU[0]);
    //   std::clog << "Under-approximation Domain size: " << controller_under.getSize() << std::endl;
    std::clog << "Under-approximation Domain size: " << CU[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;
    controller_under.writeToFile("vehicle_controller_under.bdd");
    double time_under_approx = timer.toc();
    std::clog << "Time taken by the under-approximation computation: " << time_under_approx << "s.\n";
    /****************************************************************************/
    /* We also compute the buchi controller when the noise is interpreted
   * as the worst case adversary */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of the worst-case controller.\n";
    G = {target_under.getSymbolicSet()}; /* the guarantee is the target */
    timer.tic();
    std::vector<BDD> C_wc = fp.GR1("wc", A, G, winning_over.getSymbolicSet(), avoid_over.getSymbolicSet(), verbose);
    mascot::SymbolicSet controller_wc(ss, is);
    controller_wc.setSymbolicSet(C_wc[0]);
    std::clog << "Worst case controller domain size: " << C_wc[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;
    controller_wc.writeToFile("vehicle_controller_wc.bdd");
    double time_worst_case = timer.toc();
    std::clog << "Time taken by the worst case computation: " << time_worst_case << "s.\n";
    return 1;
}
