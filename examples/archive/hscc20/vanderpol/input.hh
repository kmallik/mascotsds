/* Header file for inputs */
/* Created by: Kaushik */
/*      Date: 04/07/2019 */

#ifndef INPUT_HH_
#define INPUT_HH_

/* state space dim */
#define sDIM 2
#define iDIM 1

/* data types for the ode solver */
typedef std::array<double, sDIM> state_type;
typedef std::array<double, iDIM> input_type;

/* sampling time */
const double tau = 0.1;
/* upper bounds on the noise which is considered to be centered around the origin */
const state_type W_ub = {0.02, 0.02};

/* parallel or sequential implementation of computation of the
transition functions */
const bool parallel = false;

/* The following function is a hacky version of the decomposition function for a mixed monotone system, written solely for the purpose of use of the more accurate reachable set approximation of the van der pol example */
auto decomposition = [](input_type &u, state_type &x1, state_type &x2) -> state_type {
    state_type y;
    /* for minimization and maximization of the second state variable,
     * we evaluate the r.h.s. in six points */
    auto rhs = [](double y1, double y2) -> double {
        return (y2 + tau * y2 * (1 - pow(y1, 2)) - tau * y1);
    };
    std::vector<double> list;
    list.push_back(rhs(x1[0], x1[1]));
    list.push_back(rhs(x1[0], x2[1]));
    list.push_back(rhs(x2[0], x1[1]));
    list.push_back(rhs(x2[0], x2[1]));
    if (x1[1] != 0) {
        double temp = -1 / (2 * x1[1]);
        if (x1[0] <= temp && x2[0] >= temp) {
            list.push_back(rhs(temp, x1[1]));
        }
    }
    if (x2[1] != 0) {
        double temp = -1 / (2 * x2[1]);
        if (x1[0] <= temp && x2[0] >= temp) {
            list.push_back(rhs(temp, x2[1]));
        }
    }
    if (x1[0] <= x2[0] && x1[1] <= x2[1]) { /* x1 is the lower bound and x2 is the upper bound */
        /* return the lower left corner of the reachable set approximation */
        y[0] = x1[0] + tau * x1[1];
        y[1] = *std::min_element(std::begin(list), std::end(list));
    } else if (x1[0] >= x2[0] && x1[1] >= x2[1]) { /* x1 is the upper bound and x2 is the lower bound */
        /* return the upper right corner of the reachable set approximation */
        y[0] = x1[0] + tau * x1[1];
        y[1] = *std::max_element(std::begin(list), std::end(list));
    } else { /* invalid input */
        std::ostringstream os;
        os << "Error: Invalid input.";
        throw std::runtime_error(os.str().c_str());
    }
    /* to get rid of the warning message */
    (void)u;
    return y;
};

/* lower bounds of the hyper rectangle */
double lb[sDIM] = {-5.0, -5.0};
/* upper bounds of the hyper rectangle */
double ub[sDIM] = {5.0, 5.0};
/* grid node distance diameter */
double eta[sDIM] = {0.02, 0.02};

/* input space parameters */
double ilb[iDIM] = {-0.5};
double iub[iDIM] = {0.5};
double ieta[iDIM] = {1};

/* target states */
double h[4] = {1.2, -0.9, 2.9, -2};

#endif /* INPUT_HH_ */
