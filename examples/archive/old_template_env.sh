# Begin Variables to change:
export CUDD_PATH="/home/user/Desktop/cudd"
export MASCOT_ROOT="/home/user/Desktop/mascot-sds"
export HOAF_LIB_PATH="/home/user/Desktop/cpphoafparser-0.99.2/include/"
export MATLAB_ROOT="/home/user/Desktop/matlab"
export CUDD_LIBRARY_PATH="/home/user/libraries/lib"
export GOOGLE_TEST_PATH="/home/user/Desktop/googletest"
export SYLVAN_PATH="/home/user/libraries/"
# End  Variables to change

# Helpful variables which were created using the above variables:

#
# cudd
#
export CUDD_INCa=" -I$CUDD_PATH/include"
export CUDD_INCb=" -I$CUDD_PATH/include/cudd"
export CUDD_LIBS=" -lcudd"
export CUDD_LIB_PATH=" -L$CUDD_PATH/lib"
export CUDD_INC=" $CUDD_INCa $CUDD_INCb"
export CUDD_ALL="$CUDD_INC -I$CUDD_PATH -I$CUDD_PATH/include/ -I$CUDD_PATH/cudd -I$CUDD_PATH/lib -I$CUDD_PATH/util"
#
# mascot
#
export MASCOT_INC=" -I$MASCOT_ROOT/utils -I$MASCOT_ROOT/lib"
export MASCOT_BIN="$MASCOT_ROOT/bin"
export MASCOT_SRC="$MASCOT_ROOT/src"

#
# hoaf library
#
export HOAF_INC=" -isystem $HOAF_LIB_PATH/consumer -isystem $HOAF_LIB_PATH/parser"