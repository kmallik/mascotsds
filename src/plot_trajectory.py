import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
import pickle

def plotTrajectory(x, y, xx, yy):
    if len(plt.get_fignums())==0:
        with open('state_space.pickle', 'rb') as f:
            fig = pickle.load(f)
            plt.ion()
            plt.show()
    else:
        fig = plt.gcf()
        plt.ion()
        plt.show()


    if not fig.get_axes():
        ax = fig.add_subplot(111)
    else:
        ax = fig.get_axes()[0]

    # ax.set_xlim(0, 8)
    # ax.set_ylim(0, 4)

    verts = [
        (x, y),
        (xx, yy)
        ]

    codes = [Path.MOVETO,
             Path.LINETO]

    path = Path(verts, codes)

    patch = patches.PathPatch(path, facecolor='white', lw=2)
    ax.add_patch(patch)
    plt.draw()
    plt.show()
    plt.pause(0.1)
