/*
 * mascot-header.hh
 *  created on: 20/10/2022
 */

 /*
  * A header containing all the necessary headers needed by the source files.
  */

#include <array>
#include <chrono>
#include <cstdlib>
#include <dlfcn.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>


#include "FileHandler.hh"
#include "boost/lexical_cast.hpp"

#include "FixpointRabin.hh"
#include "Helper.hh"
#include "RungeKutta4.hh"
#include "SymbolicModelMonotonic.hh"
#include "SymbolicSet.hh"
#include "lib/ParallelRabinRecurse.hh"
#include "ubdd/CuddUBDD.hh"
#include "ubdd/SylvanUBDD.hh"
#include "utils/TicToc.hh"
#include <sys/resource.h>

#define MASCOT_SYS_NAME "sys_name"
#define MASCOT_SDIM "sDIM"
#define MASCOT_IDIM "iDIM"
#define MASCOT_DDIM "dDIM"
#define MASCOT_SETA "sETA"
#define MASCOT_SLB "sLB"
#define MASCOT_SS_ALIGNMENT "ss_grid_alignment"
#define MASCOT_SUB "sUB"
#define MASCOT_ILB "iLB"
#define MASCOT_IUB "iUB"
#define MASCOT_IETA "iETA"
#define MASCOT_DLB "dLB"
#define MASCOT_DUB "dUB"
#define MASCOT_DETA "dETA"
#define MASCOT_NOISE_UB "W_ub"

#define MASCOT_LOGFILE "logfile"
#define MASCOT_ACCL_ON "accl_on"
#define MASCOT_ACCL_MEM_BOUND "accl_mem_bound"
#define MASCOT_ABS_RFF "abs_RFF"
#define MASCOT_WARM_START_UNDER_APPROX "warm_start_under_approx"
#define MASCOT_VERBOSE "verbose"
#define MASCOT_OVER_AND_UNDER "over_and_under_approx"
#define MASCOT_SYS_FILE "system_file_name"

#define MASCOT_RABIN_FILE_HOA "rabin_hoa_file_name"
#define MASCOT_SPEC_TYPE "spec_type"
#define MASCOT_OWL_PATH "owl_path"
#define MASCOT_PRED_TO_POLY_BEGIN "predicate_to_polytope_mapping_begin"
#define MASCOT_PRED_TO_POLY_END "predicate_to_polytope_mapping_end"
#define MASCOT_PRED_OUTER_APPROX "_outer"
#define MASCOT_PRED_INNER_APPROX "_inner"
#define MASCOT_STATE_POLYTOPE_H "H_"
#define MASCOT_STATE_POLYTOPE_h "h_"
#define MASCOT_STATE_POLYTOPE_NOF_HS "nof_hs_"
#define MASCOT_STATE_POLYTOPE_APPROX_TYP "poly_approx_typ_"
#define MASCOT_PRED_APPROX_TYP_BEGIN "predicate_approx_type_begin"
#define MASCOT_PRED_APPROX_TYP_END "predicate_approx_type_end"
#define MASCOT_LTL_FORMULA_BEGIN "LTL_BEGIN"
#define MASCOT_LTL_FORMULA_END "LTL_END"

#define MASCOT_CONTROLLER_FOLDER "controller_folder_name"
#define MASCOT_RABIN_AUT_FOLDER "rabin_aut_folder_name"
#define MASCOT_PLOT_STATE_SPACE_FILE_NAME "plot_state_space_file_name"
#define MASCOT_INIT_STATE "initial_state"
#define MASCOT_SIM_HORIZON "simulation_horizon"

/* data types for the ode solver */
typedef std::vector<double> state_type, input_type, dist_type;

/* function: dec2bin
 * decimal to binary conversion
 *
 * Input:
 * n - a nonnegative integer in decimal
 * Output:
 * b - a vector representing the bits of the binary representation, where b[0] is LSB */
std::vector<bool> dec2bin(const size_t n) {
    std::vector<bool> b;
    if (n == 0) {
        b.push_back(0);
        return b;
    }
    size_t m = n;
    while (m > 0) {
        div_t d = div(m, 2);
        b.push_back(d.rem);
        m = d.quot;
    }
    return b;
}

int str2int(string temp) {
    string s = temp;
    if (s[0] == '-') {
        return str2int(s.substr(1)) * -1;
    }
    std::stringstream ss(s);
    int x = 0;
    ss >> x;
    return x;
}

void init_sylvan() {
    /* initiate the lace work-stealing framework and the sylvan parallel bdd library */
    int dqsize = 10000000;
    unsigned int n_workers = 0;
    lace_start(n_workers, dqsize);
    // use at most 1 GB, nodes:cache ratio 2:1, initial size 1/32 of maximum
    sylvan::sylvan_set_limits(1 * 1024 * 1024 * 1024, 1, 5);
    sylvan::sylvan_init_package();
    sylvan::sylvan_init_mtbdd();
}

void stop_sylvan() {
    sylvan::sylvan_stats_report(stdout);
    sylvan::sylvan_quit();
    lace_stop();
    genie::SylvanUBDD::nodes_map = std::map<size_t, genie::SylvanUBDD *>();
    genie::SylvanUBDD::size_ = 0;
}
