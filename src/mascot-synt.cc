/*
 * mascot-synt.cc
 *
 *  created on: 23.03.2021
 *  author: kaushik
 */

/*
  * An interface program to synthesize controller
  *  The inputs are to be read from a configuration file.
  */

#include "mascot-header.hh"

std::vector<SylvanUBDD> run_rabin(mascot::FixpointRabin<SylvanUBDD> fp, bool accl_on, size_t M, const SylvanUBDD& initial_seed, int verbose, const mascot::RabinAutomaton<SylvanUBDD> &rabin, bool parallel) {
    std::vector<SylvanUBDD> result;
    if (parallel) {
        cout << "START PARALLEL RABIN\n";
        result = fp.RabinEnd(fp.Rabin(accl_on, M, initial_seed, verbose, genie::ParallelRabinRecurse), &rabin);
        cout << "DONE PARALLEL RABIN\n";
    } else {
        cout << "START SEQUENTIAL RABIN\n";
        result = fp.RabinEnd(fp.Rabin(accl_on, M, initial_seed, verbose), &rabin);
        cout << "DONE SEQUENTIAL RABIN\n";
    }
    return result;
}

std::vector<CuddUBDD> run_rabin(mascot::FixpointRabin<CuddUBDD> fp, bool accl_on, size_t M, const CuddUBDD& initial_seed, int verbose, const mascot::RabinAutomaton<CuddUBDD> &rabin, bool parallel) {
    cout << "START SEQUENTIAL RABIN\n";
    std::vector<CuddUBDD> result = fp.RabinEnd(fp.Rabin(accl_on, M, initial_seed, verbose), &rabin);
    cout << "DONE SEQUENTIAL RABIN\n";
    return result;
}

template <class UBDD>
int run_experiment(const std::string &cfg_file_str, const bool parallel) {
    /* total run-time */
    TicToc overall_timer;
    overall_timer.tic();
    /* ********************************************************** */
    /* open the configuration file and read various parameters */
    /* ********************************************************** */
    const char *cfg_file = cfg_file_str.c_str();
    size_t p = cfg_file_str.find_last_of("/");
    if (p == std::string::npos) {
        throw std::invalid_argument("Error: you need to specify the location of the cfg file. If the file is in the current directory, type ./<file.cfg>.\n");
    }
    std::string dir = cfg_file_str.substr(0, p);
    dir = dir.size() ? dir : ".";
    if (!std::freopen(cfg_file, "r", stderr)) {
        throw std::invalid_argument( "Error: Cannot open '" + (std::string)(cfg_file) +  "' (cfg_file).");
    }
    char logfile[30];
    readMember(cfg_file, logfile, MASCOT_LOGFILE); /* read the name of the logfile */
    if (!std::freopen(logfile, "a", stderr)) {
        throw std::invalid_argument( "Error: Cannot open '" + (std::string)(logfile) +  "' (logfile).");
    }
    int verbose = 0; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */
    readMember(cfg_file, verbose, MASCOT_VERBOSE);
    size_t M = 25; /* the memory bound (in terms of the maximum number of past iterations to be stored) in case acceleration is on */
    readMember(cfg_file, M, MASCOT_ACCL_MEM_BOUND);
    bool accl_on = true;                 /* whether accelerated fixpoint is on or not */
    bool RFF = false;                    /* read transitions from file flag */
    bool warm_start_under_approx = true; /* warm-start the under-approximation from the outcome of the over-approximation */
    bool over_and_under_approx = false;  /* compute both the over and the under approximation of the almost sure winning controller domain */
    char accl_on_char[10], RFF_char[10], warm_start_under_approx_char[10], over_and_under_approx_char[10];
    if (readMember(cfg_file, accl_on_char, MASCOT_ACCL_ON)) {
        if (!strcmp(accl_on_char, "true")) {
            accl_on = true;
        } else if (!strcmp(accl_on_char, "false")) {
            accl_on = false;
        } else {
            throw std::invalid_argument("Error: invalid flag for " + (std::string)(MASCOT_ACCL_ON) + "; using default value (true).\n");
        }
    }
    if (readMember(cfg_file, RFF_char, MASCOT_ABS_RFF)) {
        if (!strcmp(RFF_char, "true")) {
            RFF = true;
        } else if (!strcmp(RFF_char, "false")) {
            RFF = false;
        } else {
            throw std::invalid_argument("Error: invalid flag for " + (std::string)(MASCOT_ABS_RFF) + "; using default value (false).\n");
        }
    }
    if (readMember(cfg_file, warm_start_under_approx_char, MASCOT_WARM_START_UNDER_APPROX)) {
        if (!strcmp(warm_start_under_approx_char, "true")) {
            warm_start_under_approx = true;
        } else if (!strcmp(warm_start_under_approx_char, "false")) {
            warm_start_under_approx = false;
        } else {
            throw std::invalid_argument("Error: invalid flag for " + (std::string)(MASCOT_WARM_START_UNDER_APPROX) + "; using default value (true).\n");
        }
    }
    if (readMember(cfg_file, over_and_under_approx_char, MASCOT_OVER_AND_UNDER)) {
        if (!strcmp(over_and_under_approx_char, "true")) {
            over_and_under_approx = true;
        } else if (!strcmp(over_and_under_approx_char, "false")) {
            over_and_under_approx = false;
        } else {
            throw std::invalid_argument("Error: invalid flag for " + std::string(MASCOT_OVER_AND_UNDER) + "; using default value (false).\n");
        }
    }

    /* create the system states */
    int sDIM, iDIM, dDIM, ss_alignment;
    /* the state space dimension */
    readMember(cfg_file, sDIM, MASCOT_SDIM);
    if (sDIM <= 0) {
        throw std::invalid_argument("Error: state space dimension must be nonnegative.\n");
    }
    /* the state space grid alignment */
    ss_alignment = 0; /* default alignment: center at origin */
    readMember(cfg_file, ss_alignment, MASCOT_SS_ALIGNMENT);
    if (ss_alignment != 0 && ss_alignment != 1) {
        throw std::invalid_argument("Error: alignment has to be either 0 or 1.\n");
    }
    /* the input space dimension */
    readMember(cfg_file, iDIM, MASCOT_IDIM);
    if (iDIM <= 0) {
        throw std::invalid_argument("Error: input space dimension must be nonnegative.\n");
    }
    /* the input space dimension */
    readMember(cfg_file, dDIM, MASCOT_DDIM);
    if (dDIM <= 0) {
        throw std::invalid_argument("Error: disturbance space dimension must be nonnegative.\n");
    }
    /* the bounds and discretization parameters for the state space and the input space */
    state_type lb, ub, eta, W_ub;
    input_type ilb, iub, ieta;
    dist_type dlb, dub, deta;
    readVec(cfg_file, lb, MASCOT_SLB);
    readVec(cfg_file, ub, MASCOT_SUB);
    for (int i = 0; i < sDIM; i++) {
        if (ub[i] <= lb[i]) {
            throw std::invalid_argument( "Error: the state space upper bounds must be greather than the lower bounds.\n");
        }
    }
    readVec(cfg_file, eta, MASCOT_SETA);
    for (int i = 0; i < sDIM; i++) {
        if (eta[i] <= 0) {
            throw std::invalid_argument("Error: the state space discretization size must be positive in all dimensions.\n");
        }
    }
    readVec(cfg_file, ilb, MASCOT_ILB);
    readVec(cfg_file, iub, MASCOT_IUB);
    for (int i = 0; i < iDIM; i++) {
        if (iub[i] <= ilb[i]) {
            throw std::invalid_argument("Error: the input space upper bounds must be greather than the lower bounds.\n");
        }
    }
    readVec(cfg_file, ieta, MASCOT_IETA);
    for (int i = 0; i < iDIM; i++) {
        if (ieta[i] <= 0) {
            throw std::invalid_argument("Error: the input space discretization size must be positive in all dimensions.\n");
        }
    }
    readVec(cfg_file, dlb, MASCOT_DLB);
    readVec(cfg_file, dub, MASCOT_DUB);
    for (int i = 0; i < dDIM; i++) {
        if (dub[i] <= dlb[i]) {
            throw std::invalid_argument("Error: the disturbance space upper bounds must be greather than the lower bounds.\n");
        }
    }
    readVec(cfg_file, deta, MASCOT_DETA);
    for (int i = 0; i < dDIM; i++) {
        if (deta[i] <= 0) {
            throw std::invalid_argument("Error: the input space discretization size must be positive in all dimensions.\n");
        }
    }
    /* The maximum magnitude of the disturbances. Note that the valus of the disturbances in all dimensions must be centered about 0.*/
    readVec(cfg_file, W_ub, MASCOT_NOISE_UB);
    for (int i = 0; i < sDIM; i++) {
        if (W_ub[i] < 0) {
            throw std::invalid_argument("Error: the maximum magnitude of the disturbances must be nonnegative in all dimensions.\n");
        }
    }
    /* ********************************************************** */
    /* create the temporary library containing the system dynamics equation */
    /* ********************************************************** */
    /* read the name of the file where the system dynamics equations have been written  */
    std::string sys_filename;
    readMember(cfg_file, sys_filename, MASCOT_SYS_FILE);
    /* the system dynamics has to be given as a c++ lambda expression, which is then compiled as a temporary library and is linked at run-time */
    /* temporary cpp/library output files */

    std::string sys_file = dir + "/" + sys_filename;
    std::string outpath = dir + "/tmp";
    std::string libfile = outpath + "/system.so";
    std::string logfile_tmp = outpath + "/system.log";
    helper::checkMakeDir(outpath.c_str());
    int ret = system(("rm " + outpath + "/*").c_str());

    /* compile the code */
    //std::string cmd = "g++ -Wall -Wextra " + sys_file + " -o " + libfile
    //                  + " -O2 -shared -fPIC &> " + logfile_tmp; /* the "&> logile_tmp" part does not work in Ubuntu */
    std::string cmd = "g++ -Wall -Wextra " + sys_file + " -o " + libfile + " -O2 -shared -fPIC 2>" + logfile_tmp;
    ret = system(cmd.c_str());
    if (WEXITSTATUS(ret) != EXIT_SUCCESS) {
        throw std::runtime_error("compilation failed, see " + logfile_tmp + "\n");
    }
    /* load dynamic library */
    void *dynlib = dlopen(libfile.c_str(), RTLD_LAZY);
    if (!dynlib) {
        throw std::runtime_error("error loading library:\n" + (std::string)(dlerror()) + "\n");
    }
    /* loading symbol from library and assign to pointer
    * (to be cast to function pointer later) */
    char sys_name[30];
    readMember(cfg_file, sys_name, MASCOT_SYS_NAME);
    state_type (*decomposition)(input_type &, dist_type &, state_type &, state_type &) = (state_type(*)(input_type &, dist_type &, state_type &, state_type &))dlsym(dynlib, sys_name);
    const char *dlsym_error = dlerror();
    if (dlsym_error != NULL) {
        throw std::runtime_error("error loading symbol " + (std::string)(MASCOT_SYS_NAME) + ":\n" + dlsym_error + "\n");
    }
    /* logfile entry */
    std::clog << "\n\n ******************\n";
    std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::clog << "Time = " << std::ctime(&time) << '\n';
    std::clog << "Configuration file name = " << cfg_file_str.c_str() << '\n';
    if (!parallel) {
        std::clog << "Sequential mode using CUDD.\n";
    } else {
        std::clog << "Parallel mode using Sylvan.\n";
    }
    std::string eta_log_str = MASCOT_SETA;
    eta_log_str += "= [";
    for (size_t i = 0; i < sDIM; i++) {
        eta_log_str += std::to_string(eta[i]);
        if (i == sDIM - 1) {
            eta_log_str += "]\n";
        } else {
            eta_log_str += ", ";
        }
    }
    std::clog << eta_log_str.c_str();
    // std::clog << MASCOT_SETA << " = [" << eta[0] << ", " << eta[1] << "]" << '\n';
    std::clog << "Verbosity = " << verbose << '\n';
    if (accl_on) {
        std::clog << "Acceleration = ON" << '\n';
    } else {
        std::clog << "Acceleration = OFF" << '\n';
    }
    std::clog << MASCOT_ACCL_MEM_BOUND << " = " << M << '\n';
    if (RFF) {
        std::clog << "Read from file = TRUE" << '\n';
    } else {
        std::clog << "Read from file = FALSE" << '\n';
    }
    if (warm_start_under_approx) {
        std::clog << "Warm-start under-approximation = TRUE" << '\n';
    } else {
        std::clog << "Warm-start under-approximation = FALSE" << '\n';
    }

    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    UBDD base;
    helper::checkMakeDir("data");
    //    /* remove all the content of the data folder in case one existed from before */
    //    system("rm -r -f data/*");

    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from the config file */
    mascot::SymbolicSet<UBDD> ss(base, sDIM, lb, ub, eta, ss_alignment);
    ss.addGridPoints();
    ss.writeToFile("data/X.bdd");
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from config file */
    mascot::SymbolicSet<UBDD> is(base, iDIM, ilb, iub, ieta, 0);
    is.addGridPoints();
    is.writeToFile("data/U.bdd");
    /*********************************************************************************************/
    /* construct SymbolicSet for the disturbance input space */
    /********************************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from config file */
    mascot::SymbolicSet<UBDD> ds(base, dDIM, dlb, dub, deta, 0);
    ds.addGridPoints();
    ds.writeToFile("data/W.bdd");
    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    mascot::SymbolicSet<UBDD> sspost(ss, 1); /* create state space for post variables */
    mascot::SymbolicModelMonotonic<state_type, input_type, dist_type, UBDD> abstraction(&ss, &is, &ds, &sspost);
    if (RFF) {
        std::cout << "Reading transition relations from file." << '\n';
        mascot::SymbolicSet<UBDD> mt(base, "data/maybeTransitions.bdd");
        mascot::SymbolicSet<UBDD> st(base, "data/sureTransitions.bdd");
        abstraction.setTransitionRelations(mt.getSymbolicSet(), st.getSymbolicSet());
        /* get the number of elements in the transition relation */
        std::cout << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
    } else {
        /* compute the transition relation */
        timer.tic();
        abstraction.computeTransitions(decomposition, W_ub);
        std::cout << std::endl;
        double time_abstraction = timer.toc();
        std::clog << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
        /* get the number of elements in the transition relation */
        std::clog << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::clog << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        std::cout << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        /* save transition relations */
        (abstraction.getMaybeTransitions()).writeToFile("data/maybeTransitions.bdd");
        (abstraction.getSureTransitions()).writeToFile("data/sureTransitions.bdd");
    }
    /* log the number of states in the resulting abstract two-and-half player game */
    size_t nof_abs_states = ss.getSize();
    size_t nof_abs_inputs = is.getSize();
    size_t nof_abs_dist = ds.getSize();
    mascot::SymbolicSet<UBDD> st = abstraction.getSureTransitions();
    size_t size_sure_dom = ((st.getSymbolicSet()).existAbstract(sspost.getCube())).countMinterm(ss.getNVars() + is.getNVars() + ds.getNVars());
    size_t size_game_states = nof_abs_states + nof_abs_states * nof_abs_inputs + size_sure_dom + (abstraction.getSizeMaybe() - abstraction.getSizeSure());
    std::cout << "Number of states in the reduced two-and-half-player game: " << size_game_states << std::endl;
    std::clog << "Number of states in the reduced two-and-half-player game: " << size_game_states << std::endl;
    /* ********************************************************** */
    /* the specification */
    /* ********************************************************** */
    /* the specification format (LTL/rabin automaton) */
    char spec_type[20];
    readMember(cfg_file, spec_type, MASCOT_SPEC_TYPE);
    /* mapping from the state predicate labels to the indices of the polytopes */
    std::map<std::string, std::vector<int>> pred_to_poly;
    readMap(cfg_file, pred_to_poly, MASCOT_PRED_TO_POLY_BEGIN, MASCOT_PRED_TO_POLY_END);
    std::map<std::string, std::string> pred_approx_typ;
    readMap(cfg_file, pred_approx_typ, MASCOT_PRED_APPROX_TYP_BEGIN, MASCOT_PRED_APPROX_TYP_END);
    /* determine the type of approximation to be used for the predicates */
    std::string ltl_formula;
    if (!strcmp(spec_type, "LTL") || !strcmp(spec_type, "ltl")) { /* derive the approximation type from the equivalent NNF formula */
        readStrBlock(cfg_file, ltl_formula, MASCOT_LTL_FORMULA_BEGIN, MASCOT_LTL_FORMULA_END);
        ltl_formula.erase(std::remove(ltl_formula.begin(), ltl_formula.end(), '\n'), ltl_formula.end());
        char owl_path[500];
        readMember(cfg_file, owl_path, MASCOT_OWL_PATH);
        struct stat info;
        std::string owl_bin(owl_path);
        owl_bin += "/owl";
        if (stat(owl_bin.c_str(), &info) != 0) {
            std::cout << "The binary of owl cannot be found in the specified path. Exiting.\n";
            throw std::runtime_error("File not found");
        }

        std::string command(owl_path);
        command += "/owl ltl-utilities -f \'";
        command += ltl_formula;
        command += "\' -o " + outpath + "/ltl_converted_to_nnf.txt --rewriter=NNF";
        ret = system(command.c_str());
        if (WEXITSTATUS(ret) != EXIT_SUCCESS) {
            std::cout << "owl failed" << std::endl;
            exit(EXIT_FAILURE);
        }
        /* read the NNF formula */
        std::ifstream ifs(outpath + "/ltl_converted_to_nnf.txt");
        ltl_formula.assign((std::istreambuf_iterator<char>(ifs)),
                           (std::istreambuf_iterator<char>()));
        ltl_formula.erase(std::remove(ltl_formula.begin(), ltl_formula.end(), '\n'), ltl_formula.end());
        if (ltl_formula == "") {
            std::cout << "Something wrong with the specified LTL formula. Check the syntax.\n";
            throw std::runtime_error("Incorrect LTL formula.");
        }
        std::map<std::string, std::vector<int>> temp;
        for (auto i = pred_to_poly.begin(); i != pred_to_poly.end(); ++i) {
            size_t pos = 0;
            while (1) {
                //                size_t pos = std::min(ltl_formula.find(i->first+" "),
                //                                      ltl_formula.find(i->first+")"));
                pos = ltl_formula.find(i->first, pos);
                if (pos == std::string::npos)
                    break;
                if (pred_approx_typ.count(i->first) > 0) {
                    pos += i->first.size();
                    if (pred_approx_typ[i->first] == "OUTER") {
                        ltl_formula.insert(pos, MASCOT_PRED_OUTER_APPROX);
                        temp.insert({i->first + MASCOT_PRED_OUTER_APPROX,
                                     i->second});
                    } else if (pred_approx_typ[i->first] == "INNER") {
                        ltl_formula.insert(pos, MASCOT_PRED_INNER_APPROX);
                        temp.insert({i->first + MASCOT_PRED_INNER_APPROX,
                                     i->second});
                    } else {
                        std::cout << "Error: mascot-synt: wrong predicate approximation type given as input. Allowed values: OUTER, INNER.";
                        throw std::runtime_error("Incorrect input.");
                    }

                } else {
                    switch (ltl_formula[pos - 1]) {
                        case '!':
                            pos += i->first.size();
                            ltl_formula.insert(pos, MASCOT_PRED_OUTER_APPROX);
                            temp.insert({i->first + MASCOT_PRED_OUTER_APPROX,
                                         i->second});
                            break;

                        default:
                            pos += i->first.size();
                            ltl_formula.insert(pos, MASCOT_PRED_INNER_APPROX);
                            temp.insert({i->first + MASCOT_PRED_INNER_APPROX,
                                         i->second});
                            break;
                    }
                }
            }
        }
        pred_to_poly = temp;
    }
    /* bdd representation of the state space predicates */
    std::map<std::string, UBDD> pred;
    helper::checkMakeDir("data/rabin");
    helper::checkMakeDir("data/rabin/predicates");
    /* iterate over all the predicates */
    for (auto i = pred_to_poly.begin(); i != pred_to_poly.end(); ++i) {
        mascot::SymbolicSet<UBDD> *p = new mascot::SymbolicSet<UBDD>(ss);
        /* a predicate can be defined as the union of several polytopes
         * iterate over all the polytopes of the current predicate
         * (given using the predicate-to-polytope map */
        for (auto j = i->second.begin(); j != i->second.end(); ++j) {
            /* the number of half-spaces of the current polytope */
            size_t n;
            std::string str(MASCOT_STATE_POLYTOPE_NOF_HS);
            readMember(cfg_file, n, (str + std::to_string(*j)).c_str());
            /* the H-matrix and the h-vector of the current predicate */
            std::vector<double> H = std::vector<double>(n * sDIM);
            std::vector<double> h = std::vector<double>(n);
            std::string H_name(MASCOT_STATE_POLYTOPE_H);
            std::string h_name(MASCOT_STATE_POLYTOPE_h);
            readArr(cfg_file, &H[0], (H_name + std::to_string(*j)).c_str());
            readArr(cfg_file, &h[0], (h_name + std::to_string(*j).c_str()));
            if (!strcmp(spec_type, "rabin") || !strcmp(spec_type, "Rabin")) { /* user-defined */
                /* read the approximation type of the current polytope */
                char *poly_approx_typ = new char[30];
                std::string approx_type_name(MASCOT_STATE_POLYTOPE_APPROX_TYP);
                readMember(cfg_file, poly_approx_typ, (approx_type_name + std::to_string(*j)).c_str());
                if (!strcmp(poly_approx_typ, "INNER")) {
                    p->addPolytope(n, H, h, mascot::INNER);
                } else if (!strcmp(poly_approx_typ, "OUTER")) {
                    p->addPolytope(n, H, h, mascot::OUTER);
                } else {
                    std::cout << "invalid input for " << MASCOT_STATE_POLYTOPE_APPROX_TYP << "for polytope " << *j << "." << std::endl;
                    exit(EXIT_FAILURE);
                }
                delete[] poly_approx_typ;
            } else if (!strcmp(spec_type, "LTL") || !strcmp(spec_type, "ltl")) {
                if (i->first.rfind(MASCOT_PRED_INNER_APPROX) != std::string::npos) {
                    p->addPolytope(n, H, h, mascot::INNER);
                } else if (i->first.rfind(MASCOT_PRED_OUTER_APPROX) != std::string::npos) {
                    p->addPolytope(n, H, h, mascot::OUTER);
                }
            }
        }
        pred.insert({i->first, p->getSymbolicSet()});
        /* store the predicate */
        std::string fpath = "data/rabin/predicates/";
        fpath += i->first;
        fpath += ".bdd";
        p->writeToFile(fpath.c_str());
    }
    /* build the symbolic representation of the rabin automaton */
    std::string rabin_file_hoa;
    if (!strcmp(spec_type, "rabin") || !strcmp(spec_type, "Rabin")) { /* build from a given rabin automaton specified in the HOA format*/
        readMember(cfg_file, rabin_file_hoa, MASCOT_RABIN_FILE_HOA);
    } else if (!strcmp(spec_type, "LTL") || !strcmp(spec_type, "ltl")) { /* build froma a given LTL formula via Rabinizer */
        char owl_path[500];
        readMember(cfg_file, owl_path, MASCOT_OWL_PATH);
        std::string command(owl_path);
        command += "/owl ltl2dra -f \'";
        command += ltl_formula;
        // command += "\" -O " + outpath + "/spec_converted_to_rabin.txt --- ltl --- simplify-ltl --- ltl2dra --- complete --- hoa --state-acceptance";
        command += "\' -o " + outpath + "/spec_converted_to_rabin.txt --complete --state-acceptance";
        timer.tic();
        ret = system(command.c_str());
        if (WEXITSTATUS(ret) != EXIT_SUCCESS) {
            std::cout << "owl failed" << std::endl;
            exit(EXIT_FAILURE);
        }
        double time_conversion_to_rabin = timer.toc();
        std::clog << "Time taken by Rabinizer to convert the given LTL formula to a Rabin automaton: " << time_conversion_to_rabin << "s.\n";
        rabin_file_hoa = outpath + "/spec_converted_to_rabin.txt";
    } else {
        throw std::runtime_error("Unknown specification type.");
    }
    mascot::RabinAutomaton<UBDD> rabin(base, ss, pred, rabin_file_hoa);
    rabin.writeToFile("data/rabin");

    std::vector<UBDD> CO;
    double time_over_approx = 0;
    size_t CO_dom_size;
    if (over_and_under_approx) {
        /****************************************************************************/
        /* Over-approximation of almost sure winning region */
        /****************************************************************************/
        /* we setup a fixed point object to compute reach and stay controller */
        mascot::FixpointRabin<UBDD> fpo("over", &abstraction, &rabin);
        std::cout << "FixpointRabin initialized." << '\n';
        std::cout << "\n\n Starting computation of over-approximation of the a.s. controller.\n";
        timer.tic();
        CO = run_rabin(fpo, accl_on, M, base.one(), verbose, rabin,parallel);

        /* there will be as many controllers as the number of states of the rabin automaton */
        mascot::SymbolicSet<UBDD> controller_over(ss, is);
        helper::checkMakeDir("data/controllers_over");
        for (size_t i = 0; i < CO.size(); i++) {
            mascot::SymbolicSet<UBDD> controller_over(ss, is);
            controller_over.setSymbolicSet(CO[i]);
            std::string Str = "";
            Str += "data/controllers_over/C";
            Str += std::to_string(i);
            Str += ".bdd";
            char Char[50];
            size_t Length = Str.copy(Char, Str.length() + 1);
            Char[Length] = '\0';
            controller_over.writeToFile(Char);
        }
        time_over_approx = timer.toc();
        std::clog << "Time taken by the over-approximation computation: " << time_over_approx << "s.\n";
        CO_dom_size = CO[0].existAbstract(fpo.getCubeInput() * rabin.stateSpace_->getCube()).countMinterm(ss.getNVars());
        std::clog << "Number of states in the over-approximation: " << CO_dom_size << std::endl;
    }

    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    /* we setup a fixed point object to compute reach and stay controller */
    mascot::FixpointRabin<UBDD> fpu("under", &abstraction, &rabin);
    std::cout << "FixpointRabin initialized." << '\n';
    UBDD initial_seed;
    if (over_and_under_approx && warm_start_under_approx) {
        std::cout << "\n\n Starting computation of under-approximation of the a.s. controller (w/ initial seed).\n";
        initial_seed = base.zero();
        for (size_t i = 0; i < CO.size(); i++) {
            initial_seed |= CO[i];
        }
    } else {
        std::cout << "\n\n Starting computation of under-approximation of the a.s. controller (w/o initial seed).\n";
        initial_seed = base.one();
    }

    timer.tic();
    std::vector<UBDD> CU = run_rabin(fpu, accl_on, M, initial_seed, verbose, rabin, parallel);

    /* there will be as many controllers as the number of states of the rabin automaton */
    mascot::SymbolicSet<UBDD> controller_under(ss, is);
    helper::checkMakeDir("data/controllers");
    for (size_t i = 0; i < CU.size(); i++) {
        mascot::SymbolicSet<UBDD> controller_under(ss, is);
        controller_under.setSymbolicSet(CU[i]);
        std::string Str = "";
        Str += "data/controllers/C";
        Str += std::to_string(i);
        Str += ".bdd";
        char Char[50];
        size_t Length = Str.copy(Char, Str.length() + 1);
        Char[Length] = '\0';
        controller_under.writeToFile(Char);
    }
    double time_under_approx = timer.toc();
    std::clog << "Time taken by the under-approximation computation w/ initial seed: " << time_under_approx << "s.\n";
    std::clog << "Total time for synthesis: " << time_over_approx + time_under_approx << "s.\n";
    size_t CU_dom_size = CU[0].existAbstract(fpu.getCubeInput() * rabin.stateSpace_->getCube()).countMinterm(ss.getNVars());
    std::clog << "Number of states in the under-approximation: " << CU_dom_size << std::endl;


    double s_volume = 1.0; /* volume of each hyperrectangle */
    double total_volume = 1.0; /* total volume of state space */
    for (size_t i = 0; i < sDIM; i++) {
        s_volume *= eta[i];
        total_volume *= ub[i] - lb[i];
    }
    std::clog << "Relative volume of the error region: " << (CO_dom_size - CU_dom_size)*s_volume/total_volume << std::endl;

    double total_computation_time = overall_timer.toc();
    std::clog << "End-to-end computation time: " << total_computation_time << "s.\n";
    dlclose(dynlib);
    return 0;
}

int main(int argc, char **argv) {

    /* sanity check */
    if (argc != 3) {
        cout << "Error: mascot-synt: you need to specify the name of the configuration file and the type of UBDD (0-Cudd|1-Sylvan|2-Sylvan with parallel rabin).\n";
        return -1;
    }

    size_t is_sylvan = str2int(argv[2]);

    try {
        if (is_sylvan) {
            init_sylvan();
            std::cout << "Running in the parallel mode (using Sylvan).\n\n";
            run_experiment<SylvanUBDD>(argv[1], true);
            stop_sylvan();
        } else {
            std::cout << "Running in the sequential mode (using CUDD).\n\n";
            run_experiment<CuddUBDD>(argv[1], false);
        }
    } catch (std::exception &e) {
        cout << "\n"
             << e.what();
        return -1;
    }
    return 0;
}
