/*
 * mascot-sim.cc
 *
 *  created on: 20.10.2022
 *  author: kaushik
 */

 /*
  * An interface program to simulate controller synthesized using mascot-synt.
  *  The inputs are to be read from a configuration file.
  */

// #include "matplotlibcpp.h"
#include "mascot-header.hh"
#include "pyhelper.hh"

#include <random>

// namespace plt = matplotlibcpp;

template <class UBDD>
int run_simulation(const std::string &cfg_file_str) {
    UBDD base;
    /* total run-time */
    TicToc overall_timer;
    overall_timer.tic();

    /* ********************************************************** */
    /* open the configuration file and read various parameters */
    /* ********************************************************** */
    const char *cfg_file = cfg_file_str.c_str();
    std::string dir = cfg_file_str.substr(0, cfg_file_str.find_last_of("/"));
    dir = dir.size() ? dir : ".";

    if (!std::freopen(cfg_file, "r", stderr)) {
        throw std::invalid_argument( "Error: Cannot open '" + (std::string)(cfg_file) +  "' (cfg_file).");
    }
    char logfile[30];
    readMember(cfg_file, logfile, MASCOT_LOGFILE); /* read the name of the logfile */
    // std::freopen(logfile, "a", stderr);
    // char cont_file[50];
    // readMember(cfg_file, cont_file, MASCOT_CONTROLLER_FILE);
    std::string cont_folder, rabin_folder;
    readMember(cfg_file, cont_folder, MASCOT_CONTROLLER_FOLDER);
    readMember(cfg_file, rabin_folder, MASCOT_RABIN_AUT_FOLDER);
    /* create the system states */
    int sDIM, iDIM, dDIM, T;
    /* the state space dimension */
    readMember(cfg_file, sDIM, MASCOT_SDIM);
    if (sDIM <= 0) {
        throw std::invalid_argument("Error: state space dimension must be nonnegative.\n");
    }
    /* the input space dimension */
    readMember(cfg_file, iDIM, MASCOT_IDIM);
    if (iDIM <= 0) {
        throw std::invalid_argument("Error: input space dimension must be nonnegative.\n");
    }
    /* the input space dimension */
    readMember(cfg_file, dDIM, MASCOT_DDIM);
    if (dDIM <= 0) {
        throw std::invalid_argument("Error: disturbance space dimension must be nonnegative.\n");
    }
    readMember(cfg_file, T, MASCOT_SIM_HORIZON);
    if (dDIM <= 0) {
        throw std::invalid_argument("Error: simulation horizon must be nonnegative.\n");
    }
    state_type x0, W_ub;
    dist_type dlb, dub;
    readVec(cfg_file, x0, MASCOT_INIT_STATE);
    readVec(cfg_file, dlb, MASCOT_DLB);
    readVec(cfg_file, dub, MASCOT_DUB);
    for (int i = 0; i < dDIM; i++) {
        if (dub[i] <= dlb[i]) {
            throw std::invalid_argument("Error: the disturbance space upper bounds must be greather than the lower bounds.\n");
        }
    }
    /* The maximum magnitude of the disturbances. Note that the valus of the disturbances in all dimensions must be centered about 0.*/
    readVec(cfg_file, W_ub, MASCOT_NOISE_UB);
    for (int i = 0; i < sDIM; i++) {
        if (W_ub[i] < 0) {
            throw std::invalid_argument("Error: the maximum magnitude of the disturbances must be nonnegative in all dimensions.\n");
        }
    }
    /* Read the Rabin automaton */
    size_t n_rp, n_rs, rabin_x0;
    std::string info_file = rabin_folder + "/pairs/info.txt";
    readMember(info_file, n_rp, MASCOT_RABIN_NPAIRS);
    readMember(info_file, n_rs, MASCOT_RABIN_NSTATES);
    readMember(info_file, rabin_x0, MASCOT_RABIN_AUT_START_STATE);
    std::vector<mascot::SymbolicSet<UBDD> *> G_set, nR_set;
    std::string file;
    for (size_t i = 0; i < n_rp; i++) {
        file = rabin_folder + "/pairs/G";
        file += std::to_string(i);
        file += ".bdd";
        mascot::SymbolicSet<UBDD> *G = new mascot::SymbolicSet<UBDD>(base, file.c_str());
        G_set.push_back(G);
        file = rabin_folder + "/pairs/nR";
        file += std::to_string(i);
        file += ".bdd";
        mascot::SymbolicSet<UBDD> *nR = new mascot::SymbolicSet<UBDD>(base, file.c_str());
        nR_set.push_back(nR);
    }
    // /* debug */
    // for (size_t i = 0; i < n_rs; i++) {
    //     state_type rx = {(double)i};
    //     std::cout << "rabin state " << i << ": ";
    //     for (size_t j = 0; j < n_rp; j++) {
    //         if (!(nR_set[j]->isElement(rx))) {
    //             std::cout << j << "- ";
    //         }
    //         if (G_set[j]->isElement(rx)) {
    //             std::cout << j << "+ ";
    //         }
    //     }
    //     std::cout << std::endl;
    // }
    // /* debug ends */
    file = rabin_folder + "/transitions.bdd";
    mascot::SymbolicSet<UBDD> rabin_transitions(base, file.c_str());
    /* Load the controllers */
    std::vector<mascot::SymbolicSet<UBDD> *> controllers_set;
    for (size_t i = 0; i < n_rs; i++) {
        file = cont_folder + "C" + std::to_string(i) + ".bdd";
        mascot::SymbolicSet<UBDD> *c = new mascot::SymbolicSet<UBDD>(base, file.c_str());
        controllers_set.push_back(c);
        // /* debug */
        // std::cout << "Domain size of C" << i << ": " << c->getSize() << "\n";
        // /* debug ends */
    }
    std::vector<size_t> x_ind;
    for (size_t i = 0; i < sDIM; i++) {
        x_ind.push_back(i);
    }
    /* if the specified initial state is outside the winning domain,
        then pick an arbitrary initial state (inside the winning domain) */
    if (controllers_set[rabin_x0]->setValuedMap(x0, x_ind).size() == 0) {
        std::cout << "Specified initial state is not in the winning region. Choosing an arbitrary initial state that is winning.\n";
        // double* xu = new double[sDIM + iDIM];
        mascot::SymbolicSet<UBDD> *C = controllers_set[rabin_x0];
        if (C->getSize() == 0) {
            std::cout << "Controller does not exist. Exiting.\n";
            return 1;
        }
        // std::vector<size_t> ivars;
        // for (size_t i = 0; i < C.getDimension(); i++) {
        //     for (size_t j = 0; j < C.getNofBddVars()[i]; j++) {
        //         ivars.push_back(C.getIndBddVars()[i][j]);
        //     }
        // }
        // genie::UBDDMintermIterator *it = C.getSymbolicSet().generateMintermIterator(ivars);
        // it->begin();
        C->begin();
        // std::vector<size_t> curr_minterm = it->currentMinterm();
        std::vector<size_t> curr_minterm = C->currentMinterm();
        state_type xu(sDIM + iDIM);
        C->mintermToElement(curr_minterm, xu);
        std::cout << "Chosen initial state is : ";
        for (size_t i = 0; i < sDIM; i++) {
            x0[i] = xu[i];
            cout << x0[i] << " ";
        }
        cout << "\n";
        // delete[] xu;
    }


    /* ********************************************************** */
    /* create the temporary library containing the system dynamics equation */
    /* ********************************************************** */
    /* read the name of the file where the system dynamics equations have been written  */
    std::string sys_filename;
    readMember(cfg_file, sys_filename, MASCOT_SYS_FILE);
    /* the system dynamics has to be given as a c++ lambda expression, which is then compiled as a temporary library and is linked at run-time */
    /* temporary cpp/library output files */

    std::string sys_file = dir + "/" + sys_filename;
    std::string outpath = dir + "/tmp";
    std::string libfile = outpath + "/system.so";
    std::string logfile_tmp = outpath + "/system.log";
    helper::checkMakeDir(outpath.c_str());
    // system(("rm " + outpath + "/*").c_str());

    /* compile the code */
    //std::string cmd = "g++ -Wall -Wextra " + sys_file + " -o " + libfile
    //                  + " -O2 -shared -fPIC &> " + logfile_tmp; /* the "&> logile_tmp" part does not work in Ubuntu */
    std::string cmd = "g++ -Wall -Wextra " + sys_file + " -o " + libfile + " -O2 -shared -fPIC 2>" + logfile_tmp;
    int ret = system(cmd.c_str());
    if (WEXITSTATUS(ret) != EXIT_SUCCESS) {
        std::cout << "compilation failed, see " << logfile_tmp << std::endl;
        exit(EXIT_FAILURE);
    }
    /* load dynamic library */
    void *dynlib = dlopen(libfile.c_str(), RTLD_LAZY);
    if (!dynlib) {
        std::cout << "error loading library:\n"
                  << dlerror() << std::endl;
        exit(EXIT_FAILURE);
    }
    /* loading symbol from library and assign to pointer
    * (to be cast to function pointer later) */
    char sys_name[30];
    readMember(cfg_file, sys_name, MASCOT_SYS_NAME);
    state_type (*decomposition)(input_type &, dist_type &, state_type &, state_type &) = (state_type(*)(input_type &, dist_type &, state_type &, state_type &))dlsym(dynlib, sys_name);
    const char *dlsym_error = dlerror();
    if (dlsym_error != NULL) {
        std::cout << "error loading symbol " << MASCOT_SYS_NAME << ":\n"
                  << dlsym_error << std::endl;
        exit(EXIT_FAILURE);
    }

    /* start the simulation */
    std::vector<input_type> u;
    state_type x = x0;
    double rs = static_cast<double>(rabin_x0); /* rabin automaton's state space is a subset of the set of integers */
    state_type y, w, d;
    std::random_device rd; // Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd());// Standard mersenne_twister_engine seeded with rd()
    // std::uniform_real_distribution<> D(dlb, dub);
    /* initialize python-based simulator */
    CPyInstance pyInstance;
    /* plot state space
     * IMPORTANT: You need to manually set the variable PYTHONPATH to include the src folder and the example folder */
    std::string ss_plot_file;
    readMember(cfg_file, ss_plot_file, MASCOT_PLOT_STATE_SPACE_FILE_NAME);
    CPyObject pName, pModule, pFunc;
    // CPyObject pName = PyUnicode_FromString("plot_vehicle_state_space");
	// CPyObject pModule = PyImport_Import(pName);
    // CPyObject pFunc;
    // if (pModule) {
    //     pFunc = PyObject_GetAttrString(pModule, "plot_state_space");
    // }
    // if(pFunc && PyCallable_Check(pFunc)) {
    //     PyObject_CallObject(pFunc, NULL);
    // }
    FILE* f = _Py_fopen(ss_plot_file.c_str(), "r");
	PyRun_SimpleFile(f, ss_plot_file.c_str());
    PyObject *pValue;
    pName = PyUnicode_FromString("plot_trajectory");
	pModule = PyImport_Import(pName);
    // Py_DECREF(pName);
    pFunc;
    if (pModule) {
        pFunc = PyObject_GetAttrString(pModule, "plotTrajectory");
    }
    /* start simulation */
    for (size_t t = 0; t < T; t++) {
        // /* debug */
        // std::cout << "C=" << rs << " ";
        // /* debug ends */
        std::vector<double> rsi = {rs};
        for (size_t j = 0; j < sDIM; j++) {
            rsi.push_back(x[j]);
        }
        std::vector<size_t> ind;
        for (size_t i = 0; i < rsi.size(); i++) {
            ind.push_back(i);
        }
        state_type rs_next = rabin_transitions.setValuedMap(rsi, ind)[0];
        u = controllers_set[rs_next[0]]->setValuedMap(x, x_ind);
        if (u.size() == 0) {
            std::cout << "Error: state outside controller domain.\n";
            return 1;
        }
        for (size_t i = 0; i < sDIM; i++) {
            std::uniform_real_distribution<> W(-W_ub[i], W_ub[i]);
            w.push_back(W(gen));
        }
        for (size_t i = 0; i < dDIM; i++) {
            // std::uniform_real_distribution<> D(dlb[i], dub[i]);
            // d.push_back(D(gen));
            if (t%30 == 0) { // disturbance input appears every 30-th step
                d.push_back(1.0);
            } else {
                d.push_back(0.0);
            }
        }
        // w = W(gen);
        // d = D(gen);
        y = decomposition(u[0], d, x, x);
        for (size_t i = 0; i < sDIM; i++) {
            y[i] += w[i];
        }
        std::cout << t << ": ";
        for (size_t i = 0; i < iDIM; i++) {
            std::cout << u[0][i] << " ";
        }
        std::cout << "---> ";
        for (size_t i = 0; i < sDIM; i++) {
            std::cout << y[i] << " ";
        }
        std::cout << "\t ";
        for (size_t i = 0; i < n_rp; i++) {
            if (!(nR_set[i]->isElement(rs_next))) {
                std::cout << " " << i << "-";
            }
            if (G_set[i]->isElement(rs_next)) {
                std::cout << " " << i << "+";
            }
        }
        std::cout << "\n";
        /* 2d plot (the first 2 dimensions) */
        if (pModule) {
    		if(pFunc && PyCallable_Check(pFunc))
    		{
                PyObject* pArgs = PyTuple_New(4);
                for (size_t i = 0; i < 2; i++) {
                    pValue = PyFloat_FromDouble(x[i]);
                    if (!pValue) {
                        Py_DECREF(pArgs);
                        Py_DECREF(pValue);
                        fprintf(stderr, "Cannot convert argument\n");
                        return 1;
                    }
                    PyTuple_SetItem(pArgs, i, pValue);
                }
                for (size_t i = 0; i < 2; i++) {
                    pValue = PyFloat_FromDouble(y[i]);
                    if (!pValue) {
                        Py_DECREF(pArgs);
                        Py_DECREF(pValue);
                        fprintf(stderr, "Cannot convert argument\n");
                        return 1;
                    }
                    PyTuple_SetItem(pArgs, i + 2, pValue);
                }
    			PyObject_CallObject(pFunc, pArgs);
                Py_DECREF(pArgs);
    		}
    		else
    		{
    			printf("ERROR: function plotTrajectory\n");
    		}
        }

        x = y;
        rs = rs_next[0];
        w.clear();
        d.clear();
    }
    Py_DECREF(pValue);
    // Py_DECREF(pModule);
    // Py_XDECREF(pFunc);

    dlclose(dynlib);

    return 0;
}

int main(int argc, char **argv) {
    /* sanity check */
    if (argc != 3) {
        cout <<"Error: mascot-synt: you need to specify the name of the configuration file and the type of UBDD (0-Cudd|1-Sylvan|2-Sylvan with parallel rabin).\n";
        return -1;
    }

    size_t is_sylvan = str2int(argv[2]);
    try {
        if (is_sylvan) {
            init_sylvan();
            run_simulation<SylvanUBDD>(argv[1]);
            stop_sylvan();
        } else {
            run_simulation<CuddUBDD>(argv[1]);
        }
    } catch (std::exception &e) {
        cout << "\n"
             << e.what();
        return -1;
    }
    return 0;
}
