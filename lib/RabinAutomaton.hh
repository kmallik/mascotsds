/** @file RabinAutomaton.hh
 *
 *  @date 28.01.2021
 *  @author kaushik mallik
 */

#ifndef RABINAUTOMATON_HH_
#define RABINAUTOMATON_HH_

#include <cmath>
#include <cstdlib>

#include "Helper.hh"
#include "MascotHelper.hh"
#include "SymbolicSet.hh"
#include "lib/BaseRabinAutomaton.hh"
#include "utils/hoa_consumer_build_rabin.hh"

using namespace genie;

#define MASCOT_RABIN_NPAIRS "Rabin_nof_pairs"
#define MASCOT_RABIN_NSTATES "Rabin_nof_states"
#define MASCOT_RABIN_AUT_START_STATE "Rabin_aut_start_state"


namespace mascot {
    /**
     * @brief symbolic (ubdd) implementation of a Rabin automaton
     */
    template <class UBDD>
    class RabinAutomaton : public BaseRabinAutomaton<UBDD> {
    public:
        SymbolicSet<UBDD> *stateSpace_;
        SymbolicSet<UBDD> *inputSpace_;
        SymbolicSet<UBDD> *stateSpacePost_;
        using BaseRabinAutomaton<UBDD>::base_;
        using BaseRabinAutomaton<UBDD>::transitions_;
        using BaseRabinAutomaton<UBDD>::numRabinPairs_;
        using BaseRabinAutomaton<UBDD>::RabinPairs_;

    public:
        /**
         * @brief constructs the ubdd representation of the Rabin automaton from a given list representation of the states and the transitions
         *
         * @param base        - the UBDD manager
         * @param ns          - number of states
         * @param inputSpace  - symbolic set representing the input space
         * @param inputs      - vector containing the inputs; index should match with the transitions (predicate over the post states of the symbolic model)
         * @param transitions - vector containing the (directed) transitions (as arrays of size 3, containing the source vertex, the input, and the end vertex)
         * @param RabinPairs  - vector containing the list of Rabin pairs (as pairs of Gi,Ri, where each Gi and Ri are vectors of state indices)
         */
        RabinAutomaton(UBDD &base,
                       const size_t ns,
                       const SymbolicSet<UBDD> &inputSpace,
                       const std::vector<UBDD> inputs,
                       const std::vector<std::array<size_t, 3>>& transitions,
                       const std::vector<std::array<std::vector<size_t>, 2>>& RabinPairs) : BaseRabinAutomaton<UBDD>(base) {
            /* create a symbolic set representing the state space of the rabin automaton
             * the symbolic set is created by discretizing the range [1,ns] on the real line with eta=1 */
            size_t sdim = 1; /* we model the states as discrete points on the real line */
            std::vector<double> slb = {0};
            std::vector<double> sub = {static_cast<double>(ns - 1)};
            std::vector<double> seta = {1};
            int grid_alignment = 0; /* the origin is a grid point */
            stateSpace_ = new SymbolicSet<UBDD>(base_, sdim, slb, sub, seta, grid_alignment);
            stateSpace_->addGridPoints();
            inputSpace_ = new SymbolicSet<UBDD>(inputSpace);
            inputSpace_->addGridPoints();
            stateSpacePost_ = new SymbolicSet<UBDD>(*stateSpace_, 1);
            stateSpacePost_->addGridPoints();
            /* build the transitions bdd */
            transitions_ = base_.zero();
            for (size_t i = 0; i < transitions.size(); i++) {
                UBDD cube = base_.one();
                std::vector<size_t> v;
                cube &= element_to_ubdd({transitions[i][0]});
                cube &= inputs[transitions[i][1]];
                cube &= element_to_ubdd_post({transitions[i][2]});
                transitions_ |= cube;
            }
            /* build the rabin pairs in terms of the pre variables */
            std::function<std::vector<size_t>(size_t, size_t)> get_array = [&](size_t a, size_t b) { return RabinPairs[a][b]; };

            numRabinPairs_ = RabinPairs.size();
            BaseRabinAutomaton<UBDD>::build_rabin_pairs(get_array);
        }
        /**
         * @brief constructs the bdd representation of the Rabin automaton from a given list representation of the states and the transitions
         *
         * @param base       - the UBDD manager
         * @param inputSpace - symbolic set representing the input space
         * @param hoaf       - the file where the rabin automaton has been given in HOA format
        */
        RabinAutomaton(UBDD &base,
                       const SymbolicSet<UBDD> &inputSpace,
                       std::map<std::string, UBDD> inputs_name_to_ubdd,
                       const std::string &filename) : BaseRabinAutomaton<UBDD>(base) {

            std::ifstream file(filename);
            cpphoafparser::HOAConsumer::ptr consumer;
            cpphoafparser::rabin_data data;
            consumer.reset(new cpphoafparser::HOAConsumerBuildRabin(&data));
            cpphoafparser::HOAParser::parse(file, consumer);
            std::map<std::string, UBDD> inputs_id_to_ubdd;
            for (auto it = data.ap_id_map.begin(); it != data.ap_id_map.end(); ++it) {
                size_t key = it->first;
                inputs_id_to_ubdd.insert({std::to_string(key), inputs_name_to_ubdd[data.ap_id_map[key]]});
            }


            /* create a symbolic set representing the state space of the rabin automaton
             * the symbolic set is created by discretizing the range [1,ns] on the real line with eta=1 */
            size_t sdim = 1; /* we model the states as discrete points on the real line */
            std::vector<double> slb = {0};
            std::vector<double> sub = {static_cast<double>(data.States - 1)};
            std::vector<double> seta = {1};
            int grid_alignment = 0; /* the origin is a grid point */
            stateSpace_ = new SymbolicSet<UBDD>(base_, sdim, slb, sub, seta, grid_alignment);
            stateSpace_->addGridPoints();
            inputSpace_ = new SymbolicSet<UBDD>(inputSpace);
            inputSpace_->addGridPoints();
            stateSpacePost_ = new SymbolicSet<UBDD>(*stateSpace_, 1);
            stateSpacePost_->addGridPoints();
            /* build the transitions bdd */
            BaseRabinAutomaton<UBDD>::build_transitions(data, inputs_id_to_ubdd, inputSpace_->getSymbolicSet());
            /* build the rabin pairs in terms of the pre variables */
            std::function<std::vector<size_t>(size_t, size_t)> get_array = [&](size_t a, size_t b) -> std::vector<size_t> { return std::vector<size_t>{data.acc_signature[data.acc_pairs[a][b]]}; };
            numRabinPairs_ = data.acc_pairs.size();
            BaseRabinAutomaton<UBDD>::build_rabin_pairs(get_array);
        }


        UBDD element_to_ubdd(std::vector<size_t> id) override {
            return stateSpace_->elementToMinterm(id);
        }

        UBDD element_to_ubdd_post(std::vector<size_t> id) override {
            return stateSpacePost_->elementToMinterm(id);
        }

        UBDD complement_of_R(const std::vector<size_t> &array) override {
            UBDD result = base_.zero();
            for (auto j = static_cast<size_t>(stateSpace_->getFirstGridPoint()[0]); j <= static_cast<size_t>(stateSpace_->getLastGridPoint()[0]); j++) {
                bool isInR = false;
                for (size_t k = 0; k < array.size(); k++) {
                    if (array[k] == j) {
                        isInR = true;
                        break;
                    }
                }
                if (!isInR) {
                    result |= element_to_ubdd({j});
                }
            }
            return result;
        }

        /**
         * @brief save the transitions and the rabin pairs
         */
        void writeToFile(const char *foldername) {
            helper::checkMakeDir(foldername);
            /* create SymbolicSet representing the post stateSpace_*/
            /* make a copy of the SymbolicSet of the preSet */
            SymbolicSet<UBDD> post(*stateSpacePost_);
            /* create SymbolicSet representing the stateSpace_ x inputSpace_ */
            SymbolicSet<UBDD> sis(*stateSpace_, *inputSpace_);
            /* create SymbolicSet representing the stateSpace_ x inputSpace_ x stateSpace_ */
            SymbolicSet<UBDD> tr(sis, post);
            /* fill SymbolicSet with transition relation */
            tr.setSymbolicSet(transitions_);
            /* write the SymbolicSet to the file */
            std::string pathname = foldername;
            pathname += "/transitions.bdd";
            char Char[40];
            size_t Length = pathname.copy(Char, pathname.length() + 1);
            Char[Length] = '\0';
            tr.writeToFile(Char);
            /* create SymbolicSet-s representing the rabin pairs */
            SymbolicSet<UBDD> spec(*stateSpace_);
            std::string info_file = foldername;
            info_file += "/pairs";
            helper::checkMakeDir(info_file.c_str());
            info_file += "/info.txt";
            create(info_file);
            writeMember(info_file, MASCOT_RABIN_AUT_START_STATE, this->start_state_index, "w");
            writeMember(info_file, MASCOT_RABIN_NPAIRS, RabinPairs_.size(), "a");
            writeMember(info_file, MASCOT_RABIN_NSTATES, stateSpace_->getSize(), "a");
            for (size_t i = 0; i < RabinPairs_.size(); i++) {
                /* write the G sets */
                spec.setSymbolicSet(RabinPairs_[i].G_);
                pathname = foldername;
                pathname += "/pairs/G";
                pathname += std::to_string(i);
                pathname += ".bdd";
                size_t Length = pathname.copy(Char, pathname.length() + 1);
                Char[Length] = '\0';
                spec.writeToFile(Char);
                /* write the nR sets */
                spec.setSymbolicSet(RabinPairs_[i].nR_);
                pathname = foldername;
                pathname += "/pairs/nR";
                pathname += std::to_string(i);
                pathname += ".bdd";
                Length = pathname.copy(Char, pathname.length() + 1);
                Char[Length] = '\0';
                spec.writeToFile(Char);
            }
        }

    private:
        /**
         * @brief decimal to binary conversion
         *
         * @param n - a number in decimal
         * @return a vector representing the bits of the binary representation, where b[0] is LSB
         */
        std::vector<bool> dec2bin(const size_t n) {
            std::vector<bool> b;
            if (n == 0) {
                b.push_back(0);
                return b;
            }
            size_t m = n;
            while (m > 0) {
                div_t d = div(m, 2);
                b.push_back(d.rem);
                m = d.quot;
            }
            return b;
        }
    }; /* close class def */
}// namespace mascot

#endif /* RABINAUTOMATON_HH_ */
