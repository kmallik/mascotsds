/** @file FixpointRabin.hh
 *
 *  created on: 09.10.2015
 *      author: rungger
 *      modified by: kaushik mallik
 */

#ifndef FIXPOINTRABIN_HH
#define FIXPOINTRABIN_HH

#include <iostream>
#include <stdexcept>

#include "Helper.hh"
#include "MascotHelper.hh"
#include "RabinAutomaton.hh"
#include "SymbolicModel.hh"
#include "SymbolicSet.hh"
#include "lib/BaseFixpoint.hh"

using namespace genie;


namespace mascot {

    /**
     *  @brief provides the fixed point computation for the Buchi specification
     *
     * provides the fixed point computation for the Buchi specification
     * with almost sure and worst-case semantics
     *
     */
    template <class UBDD>
    class FixpointRabin : public BaseFixpoint<UBDD> {
    protected:
        using BaseFixpoint<UBDD>::base_;
        using BaseFixpoint<UBDD>::str_;
        using BaseFixpoint<UBDD>::preVars_;
        using BaseFixpoint<UBDD>::postVars_;
        using BaseFixpoint<UBDD>::cubePost_;
        using BaseFixpoint<UBDD>::RabinPairs_;
        using BaseFixpoint<UBDD>::factorial;

    public:
        size_t nssVars_;                     /**< number of BDD variables for state space */
        size_t nisVars_;                     /**< number of BDD variables for input space */
        size_t ndsVars_;                     /**< number of BDD variables for disturbance space */
        UBDD stateSpace_;                    /**< stores the state space UBDD */
        UBDD inputSpace_;                    /**< stores the input space UBDD */
        UBDD distSpace_;                     /**< stores the disturbance space UBDD */
        UBDD cubeInput_;                     /**< cube with input variables */
        UBDD cubeDist_;                      /**< cube with disturbance variables */
        SymbolicModel<UBDD> *symbolicModel_; /**< the symbolic model */
        SymbolicSet<UBDD> *stateSpaceSymbolicSet_;
        /* helper BDDs for maybe transitions */
        UBDD R_;  /**< maybe transition relation */
        UBDD RR_; /**< maybe transition relation with cubePost_ abstracted */
        /* helper BDDs for sure transitions */
        UBDD S_;  /**< sure transition relation */
        UBDD SS_; /**< sure transition relation with cubePost_ abstracted */

    public:
        /**
         * @brief constructor
         *  @details Initialize the FixpointRabin object as a product between a <SymbolicModel> containing the transition relation and a <RabinAutomaton> providing the specification
         */
        FixpointRabin(const char *str,
                      SymbolicModel<UBDD> *symbolicModel,
                      RabinAutomaton<UBDD> *rabin) {
            /* sanity check: the symbolic model and the rabin automaton must share the same manager */
            if (not symbolicModel->base_.isCoverEqual(rabin->base_)) {
                std::ostringstream os;
                os << "Error: The symbolic set and the rabin automaton do no have the same manager.";
                throw std::invalid_argument(os.str().c_str());
            }
            str_ = str;
            if (strcmp(str_, "over") &&
                strcmp(str_, "under") &&
                strcmp(str_, "wc")) {
                std::cerr << "Invalid argument for str." << '\n';
            }
            base_ = symbolicModel->base_;
            nssVars_ = symbolicModel->nssVars_ + rabin->stateSpace_->getNVars();
            nisVars_ = symbolicModel->nisVars_;
            ndsVars_ = symbolicModel->ndsVars_;
            preVars_ = std::vector<size_t>(nssVars_);
            for (size_t i = 0; i < symbolicModel->nssVars_; i++) {
                preVars_[i] = symbolicModel->preVars_[i];
            }
            for (int i = 0; i < rabin->stateSpace_->getNVars(); i++) {
                preVars_[i + symbolicModel->nssVars_] = rabin->stateSpace_->getIndBddVars()[0][i];
            }
            postVars_ = std::vector<size_t>(nssVars_);
            for (size_t i = 0; i < symbolicModel->nssVars_; i++) {
                postVars_[i] = symbolicModel->postVars_[i];
            }
            for (int i = 0; i < rabin->stateSpace_->getNVars(); i++) {
                postVars_[i + symbolicModel->nssVars_] = rabin->stateSpacePost_->getIndBddVars()[0][i];
            }
            stateSpaceSymbolicSet_ = new SymbolicSet<UBDD>(*symbolicModel->stateSpace_, *rabin->stateSpace_);
            stateSpaceSymbolicSet_->addGridPoints();
            stateSpace_ = stateSpaceSymbolicSet_->getSymbolicSet();
            inputSpace_ = symbolicModel->inputSpace_->getSymbolicSet();
            distSpace_ = symbolicModel->distSpace_->getSymbolicSet();
            /* create a cube with the input, disturbance, post Vars */
            std::vector<UBDD> vars(nisVars_);
            for (size_t i = 0; i < nisVars_; i++)
                vars[i] = base_.var(symbolicModel->inpVars_[i]);
            cubeInput_ = base_.cube(vars);
            /* create a cube with the disturbance Vars */
            vars.resize(ndsVars_);
            for (size_t i = 0; i < ndsVars_; i++)
                vars[i] = base_.var(symbolicModel->distVars_[i]);
            cubeDist_ = base_.cube(vars);
            /* create a cube with the post Vars */
            vars.resize(nssVars_);
            for (size_t i = 0; i < nssVars_; i++)
                vars[i] = base_.var(postVars_[i]);
            cubePost_ = base_.cube(vars);
            /* copy the transition relation */
            R_ = symbolicModel->maybeTransition_ & rabin->transitions_;
            RR_ = R_.existAbstract(cubePost_ * cubeDist_);
            S_ = symbolicModel->sureTransition_ & rabin->transitions_;
            SS_ = S_.existAbstract(cubePost_ * cubeDist_);

            symbolicModel_ = symbolicModel;

            /* create the rabin pairs */
            for (size_t i = 0; i < rabin->RabinPairs_.size(); i++) {
                rabin_pair_<UBDD> pair;
                pair.rabin_index_ = rabin->RabinPairs_[i].rabin_index_;
                stateSpaceSymbolicSet_->setSymbolicSet(rabin->RabinPairs_[i].G_);
                pair.G_ = stateSpaceSymbolicSet_->getSymbolicSet();
                stateSpaceSymbolicSet_->setSymbolicSet(rabin->RabinPairs_[i].nR_);
                pair.nR_ = stateSpaceSymbolicSet_->getSymbolicSet();
                RabinPairs_.push_back(pair);
            }
        }

        inline UBDD CubeNotState() {
            return cubeInput_;
        }

        void print_rabin_info(const UBDD &store,
                              const char *mode,
                              const int verbose,
                              int iteration = 0,
                              int depth = 0) {
            if (verbose > 0 && ((!strcmp(mode, "X")) || (!strcmp(mode, "Y")))) {
                std::cout << std::endl;
                bool X_mode = strcmp(mode, "X");
                if (!X_mode) {
                    printTabs(3 * depth + 1);
                    std::cout << "X" << depth << ", iteration ";
                } else {
                    printTabs(3 * depth);
                    std::cout << "Y" << depth << ", iteration ";
                }
                std::cout << iteration << ", states = " << store.existAbstract(CubeNotState()).countMinterm(nssVars_);

                if (verbose >= 2) {
                    /* save the intermediate sets */
                    std::string dest("data/interim_fp_sets/");
                    helper::checkMakeDir(dest.c_str());
                    dest += str_;
                    SymbolicSet<UBDD> out(*symbolicModel_->stateSpace_, *symbolicModel_->inputSpace_);
                    out.setSymbolicSet(store);
                    if (depth == 0) {
                        if (X_mode)
                            dest += "/X_outer_";
                        else
                            dest += "/Y_outer_";
                    } else {
                        if (X_mode)
                            dest += "/X_" + std::to_string(depth) + "_";
                        else
                            dest += "/Y_" + std::to_string(depth) + "_";
                    }
                    //                    dest += std::to_string(lexi_order(*indexRP, RabinPairs_.size() - 1));
                    dest += std::to_string(iteration);
                    dest += ".bdd";
                    out.writeToFile(dest.c_str());
                }
            } else if (verbose > 0 && (!strcmp(mode, "end"))) {
                std::cout << std::endl;
                std::cout << "States in controller domain = " << store.existAbstract(CubeNotState()).countMinterm(nssVars_) << std::endl;
            }
        }

        /**
         * @brief returns the cubeInput
         */
        inline UBDD getCubeInput() {
            return cubeInput_;
        }

        /**
         * @brief return maybe transition domain
         */
        inline UBDD getMaybeTransitionDomain() {
            return RR_;
        }

        /* function: Rabin (with static obstacles)
         *
         * computes the over or under approximaton of almost sure/ worst-case Rabin fixed point for stochastic systems
         *
         * Input:
         *  str       - "under" for under-approximation or "over" for over-approximation
         *  accl_on   - true/false setting the accelerated fixpoint on/off
         *  M         - the bound on the iteration count for memorizing the BDDs from the past iterations
         *  rabin     - the rabin automaton
         *  initial_seed  - initial seed for warm starting the nu fixpoints (for example the under-approximation fixpoint can be warm-started from the result of the over-approximation fixpoint)
         *  Avoid     - the set of static obstacles (which is not part of the specification)
         *  verbose   - the verbosity level (0-2, default=0)
         */
        std::vector<UBDD> OldRabin(const char *str,
                                   const bool accl_on,
                                   const size_t M, /* the bound on the iteration count for memorizing the BDDs from the past iterations */
                                   const RabinAutomaton<UBDD> *rabin,
                                   const UBDD initial_seed,
                                   const UBDD Avoid,
                                   const int verbose = 0) {
            /* save the original transition relations */
            UBDD RR = RR_;
            UBDD SS = SS_;
            /* remove avoid (state/input) pairs from transition relation */
            RR_ &= !Avoid;
            SS_ &= !Avoid;
            /* solve GR1_A2_G2 on this modified transition system */
            std::vector<UBDD> C = Rabin(str, accl_on, M, rabin, initial_seed, verbose);
            /* restore transitions */
            RR_ = RR;
            SS_ = SS;
            /* return controller */
            return C;
        }
        /* function: Rabin
         *
         *  computes the over or under approximation of almost sure/worst-case Rabin fixed point for stochastic systems with k Rabin pairs
         *
         * Input:
         *  str       - "under" for under-approximation or "over" for over-approximation
         *  accl_on   - true/false setting the accelerated fixpoint on/off
         *  M         - the bound on the iteration count for memorizing the BDDs from the past iterations
         *  rabin     - the rabin automaton
         *  initial_seed  - initial seed for warm starting the nu fixpoints (for example the under-approximation fixpoint can be warm-started from the result of the over-approximation fixpoint)
         *  verbose   - the verbosity level (0-2, default=0)
         */
        std::vector<UBDD> OldRabin(const char *str,
                                   const bool accl_on,
                                   const size_t M, /* the bound on the iteration count for memorizing the BDDs from the past iterations */
                                   const RabinAutomaton<UBDD> *rabin,
                                   const UBDD initial_seed,
                                   const int verbose = 0) {
            /* copy the rabin pairs */
            std::vector<rabin_pair_<UBDD>> pairs = RabinPairs_;
            size_t nrp = pairs.size(); /* number of rabin pairs */
            /* initialize a pair of trivial bdd-s */
            //        BDD top = base_.one();
            UBDD top = initial_seed;
            UBDD bot = base_.zero();
            /* the exact scheme as per the piterman paper */
            auto *hist_Y = new std::vector<std::vector<std::vector<std::vector<UBDD>>>>;
            auto *hist_X = new std::vector<std::vector<std::vector<std::vector<UBDD>>>>;
            if (accl_on) {
                /* if acceleration is on, then populate hist_Y and hist_X with the respective initial values */
                /* the FIRST index is related to the depth, which is at most nrp+1 (the outer layer does not participate in the caching operation) */
                for (size_t i = 0; i < nrp; i++) {
                    std::vector<std::vector<std::vector<UBDD>>> x, y;
                    size_t npos = i + 1; /* the actual depth of the fixpoint variable, where the outermost variables have depth 0 */
                    /* the SECOND index is the lexicographic position of the rabin pair subsequence 1...npos */
                    for (size_t j = 0; j < factorial(nrp) / factorial(nrp - npos); j++) {
                        //            for (size_t j=0; j<pow(nrp,npos); j++) {
                        std::vector<std::vector<UBDD>> xx, yy;
                        /* the THIRD index is the value of the sequence 0...npos-1 of the respective variables (Y sequence for hist_Y and X sequence for hist_X) */
                        for (size_t k = 0; k < M; k++) {
                            std::vector<UBDD> yyy(pow(M, npos), top); /* the FOURTH index for hist_Y is the value of the sequence 0...npos-1 of the X variables */
                            yy.push_back(yyy);
                            std::vector<UBDD> xxx(pow(M, npos + 1), bot); /* the FOURTH index for hist_X is the value of the sequence 0...npos of the Y variables */
                            xx.push_back(xxx);
                        }
                        y.push_back(yy);
                        x.push_back(xx);
                    }
                    hist_Y->push_back(y);
                    hist_X->push_back(x);
                }
            }
            /* create variables for remembering the current indices of the fixpoint variables and the indices of the rabin pairs */
            auto *indexY = new std::vector<size_t>;
            auto *indexX = new std::vector<size_t>;
            auto *indexRP = new std::vector<size_t>;
            /* the controller */
            UBDD C = base_.zero();
            /* initialize the sets for the nu fixed point */
            UBDD Y = base_.zero();
            UBDD YY = initial_seed;
            for (int i = 0; Y.existAbstract(cubeInput_) != YY.existAbstract(cubeInput_); i++) {
                Y = YY;
                if (accl_on)
                    indexY->push_back(i);
                if (verbose >= 1) {
                    std::cout << "Y0, iteration " << i << ", states = " << Y.existAbstract(cubeInput_).countMinterm(nssVars_) << "\n";
                }
                if (verbose >= 2) {
                    /* save the intermediate sets */
                    std::string dest("data/interim_fp_sets");
                    helper::checkMakeDir(dest.c_str());
                    dest += "/";
                    dest += str;
                    helper::checkMakeDir(dest.c_str());
                    SymbolicSet<UBDD> out(*symbolicModel_->stateSpace_, *symbolicModel_->inputSpace_);
                    out.setSymbolicSet(Y);
                    dest += "/Y_outer_";
                    dest += std::to_string(i);
                    dest += ".bdd";
                    out.writeToFile(dest.c_str());
                }
                /* reset the controller */
                C = base_.zero();
                /* initialize the sets for the mu fixed point */
                UBDD X = base_.one();
                UBDD XX = base_.zero();
                for (int k = 0; X.existAbstract(cubeInput_) != XX.existAbstract(cubeInput_); k++) {
                    X = XX;
                    if (accl_on)
                        indexX->push_back(k);
                    if (verbose >= 1) {
                        std::cout << "\t X0, iteration " << k << ", states = " << X.existAbstract(cubeInput_).countMinterm(nssVars_) << "\n";
                    }
                    if (verbose >= 2) {
                        /* save the intermediate sets */
                        std::string dest("data/interim_fp_sets/");
                        helper::checkMakeDir(dest.c_str());
                        dest += "/";
                        dest += str;
                        helper::checkMakeDir(dest.c_str());
                        SymbolicSet<UBDD> out(*symbolicModel_->stateSpace_, *symbolicModel_->inputSpace_);
                        out.setSymbolicSet(X);
                        dest += "/X_outer_";
                        dest += std::to_string(k);
                        dest += ".bdd";
                        out.writeToFile(dest.c_str());
                    }
                    UBDD term;
                    term = apre(Y, X);
                    /* the state-input pairs added by the outermost loop get the smallest rank */
                    UBDD N = term & (!(C.existAbstract(cubeInput_)));
                    C |= N;
                    /* recursively solve the rest of the fp */
                    XX = OldRabinRecurse(str, accl_on, M, 1, pairs, initial_seed, base_.one(), term, C, indexRP, indexY, indexX, hist_Y, hist_X, verbose);
                    if (accl_on)
                        indexX->pop_back();
                }
                YY = X;
                if (accl_on)
                    indexY->pop_back();
            }
            /* controller vector: size of C_vec= number of rabin states */
            std::vector<UBDD> C_vec(rabin->stateSpace_->getNofGridPoints()[0], base_.zero());
            for (auto i = static_cast<size_t>(rabin->stateSpace_->getFirstGridPoint()[0]); i <= rabin->stateSpace_->getLastGridPoint()[0]; i++) {
                std::vector<size_t> x = {i};
                UBDD cube = rabin->stateSpace_->elementToMinterm(x);
                UBDD Ci = C & cube;
                C_vec[i] = Ci;
            }
            return C_vec;
        }

        std::vector<UBDD> RabinEnd(UBDD C, const RabinAutomaton<UBDD> *rabin) {
            /* controller vector: size of C_vec= number of rabin states */
            std::vector<UBDD> C_vec(rabin->stateSpace_->getNofGridPoints()[0], base_.zero());
            for (auto i = static_cast<size_t>(rabin->stateSpace_->getFirstGridPoint()[0]); i <= rabin->stateSpace_->getLastGridPoint()[0]; i++) {
                std::vector<size_t> x = {i};
                UBDD cube = rabin->stateSpace_->elementToMinterm(x);
                UBDD Ci = C & cube;
                C_vec[i] = Ci;
            }
            return C_vec;
        }

    private:
        /**
         * @brief computes the controllable predecessor
         *
         * cpre(Zi) = { (x,u) | exists w, exists x': (x,u,w,x') in transitionRelation
         *                    and forall w, forall x': (x,u,w,x') in transitionRelation  => x' in Zi }
         *
         */
        UBDD cpre_local(const char *str, const UBDD &Zi) {
            UBDD T; /* the actual transition system */
            UBDD TT;
            UBDD Z = Zi;
            if (!strcmp(str, "maybe")) {
                T = R_;
                TT = RR_;
            } else if (!strcmp(str, "sure")) {
                T = S_;
                TT = SS_;
            } else {
                std::cout << "Unknown type of transition " << str << std::endl;
            }
            /* project onto state alphabet */
            Z = Z.existAbstract(cubePost_ * cubeInput_ * cubeDist_);
            /* swap variables */
            Z = Z.permute(preVars_, postVars_);
            /* find the (state, input, disturbance) triples with a post outside the safe set */
            UBDD nZ = !Z;
            UBDD F = T.andAbstract(nZ, cubePost_ * cubeDist_);
            /* the remaining (state, input) pairs make up the pre */
            UBDD nF = !F;
            UBDD preZ = TT.andAbstract(nF, cubePost_ * cubeDist_); /* would "T.andAbstract" be enough? */
            return preZ;
        }

        /**
         * @brief computes the cooperative predecessor
         *
         * type "maybe":
         * pre("maybe", Zi) = { (x,u) | exists w, exists x': (x,u,w,x') in transitionRelation
         *                    and x' in Zi }
         *
         * type "sure":
         * pre("sure", Zi) = { (x,u) | forall w, exists x': (x,u,w,x') in transitionRelation
         *                    and x' in Zi }
         *
         */
        UBDD pre_local(const char *str, const UBDD &Zi) {
            UBDD T; /* the actual transition system */
            UBDD Z = Zi;
            UBDD preZ;
            if (!strcmp(str, "maybe")) {
                T = R_;
                /* project onto state alphabet */
                Z = Z.existAbstract(cubePost_ * cubeInput_ * cubeDist_);
                /* swap variables */
                Z = Z.permute(preVars_, postVars_);
                /* find the (state, inputs) pairs with a post inside the safe set */
                preZ = T.andAbstract(Z, cubePost_ * cubeDist_);
            } else if (!strcmp(str, "sure")) {
                T = S_;
                UBDD TT = SS_;
                /* project onto state alphabet */
                Z = Z.existAbstract(cubePost_ * cubeInput_ * cubeDist_);
                /* swap variables */
                Z = Z.permute(preVars_, postVars_);
                /* find the (state, input, disturbance) triples with a post inside safe set */
                UBDD F = T.andAbstract(Z, cubePost_);
                /* for the remaining (state, input, disturbance) pairs, all the posts are outside the safe set or there are no posts at all */
                UBDD nF = !F;
                /* find the (state,input) pairs for which there is some disturbance so that the resulting (state, input, disturbance) triplet is in nF */
                UBDD G = T.andAbstract(nF, cubePost_ * cubeDist_);
                /* the remaining (state, input) pairs make up the pre */
                UBDD nG = !G;
                preZ = TT.andAbstract(nG, cubePost_ * cubeDist_);
            } else {
                std::cout << "Unknown type of transition " << str << std::endl;
            }
            return preZ;
        }

        /**
         * @brief computes the almost sure predecessor
         *
         * apre(Y,Z) = { (x,u) | forall w, forall x': (x,u,w,x') in maybeTransition_ => x' in Y
         *                    and forall w, exists x' in Z: (x,u,w,x') in sureTransition_ } OR cpre("maybe",Z)
         *
         */
        UBDD apre_local(const UBDD &Y, const UBDD &Z) {
            UBDD preZ = (cpre_local("maybe", Y) * pre_local("sure", Z)) | cpre_local("maybe", Z);
            return preZ;
        }

        /**
         * @brief computes the uncertain predecessor
         *
         * upre(Y,Z) = { (x,u) | forall w, forall x': (x,u,x') in sureTransition_ => x' in Y
         *                    and exists w, exists x' in Z: (x,u,x') in maybeTransition_ }
         *
         * NOTE: upre(Y,Z) is not the same as "cpre("sure",Y) * pre("maybe",Z)", because for the former it is not required that the sure transition is non-emtpy
         */
        UBDD upre_local(const UBDD &Yi, const UBDD &Z) {
            UBDD Y = Yi;
            UBDD preZ = pre_local("maybe", Z);
            UBDD T = S_;
            /* TT represents the full product of state space X input space UBDD */
            UBDD TT = stateSpace_ * inputSpace_;

            /* project onto state alphabet */
            Y = Y.existAbstract(cubePost_ * cubeInput_);
            /* swap variables */
            Y = Y.permute(preVars_, postVars_);
            /* find the (state, inputs) pairs with a sure post outside Y */
            UBDD nY = !Y;
            UBDD F = T.andAbstract(nY, cubePost_ * cubeDist_);
            /* the remaining (state, input) pairs---including the ones with no outgoing transitions---make up the pre w.r.t. Y */
            UBDD nF = !F;
            UBDD cpreY = TT.andAbstract(nF, cubePost_ * cubeDist_);

            UBDD upreYZ = preZ * cpreY;
            return upreYZ;
        }

        UBDD apre(const UBDD &Y, const UBDD &X) {
            if (!strcmp(str_, "under")) {
                return apre_local(Y, X);
            } else if (!strcmp(str_, "over")) {
                return upre_local(Y, X);
            } else if (!strcmp(str_, "wc")) {
                return cpre_local("maybe", X);
            } else {
                throw std::runtime_error("Unrecognized value: FixpointRabin.str_");
            }
        }

        UBDD cpre(const UBDD &Zi) {
            if (!strcmp(str_, "under")) {
                return cpre_local("maybe", Zi);
            } else if (!strcmp(str_, "over")) {
                return upre_local(Zi, Zi);
            } else if (!strcmp(str_, "wc")) {
                return cpre_local("maybe", Zi);
            } else {
                throw std::runtime_error("Unrecognized value: FixpointRabin.str_");
            }
        }

        /* function: RabinRecurse
         *
         *  recursively solve the Rabin fixpoint (used internally by the function called Rabin) */
        UBDD OldRabinRecurse(UBDD controller,
                             const_arg_recursive_rabin<UBDD> rrConst,
                             nconst_arg_recursive_rabin<UBDD> rrVars) {
            /* initialize the final solution to be returned in the end */
            /* unpack the inputs */
            const bool accl_on = rrConst.accl_on;
            const size_t M = rrConst.M; /* the bound on the iteration count for memorizing the BDDs from the past iterations */
            const int depth = rrConst.depth;
            const int verbose = rrConst.verbose;
            auto pairs = rrConst.pairs;
            auto initial_seed = rrConst.initial_seed;
            auto seqR = rrVars.seqR;
            auto right = rrVars.right;
            auto hist_Y = rrVars.hist_Y;
            auto hist_X = rrVars.hist_X;
            auto indexY = rrVars.indexY;
            auto indexX = rrVars.indexX;
            auto indexRP = rrVars.indexRP;

            UBDD U = base_.zero();
            for (size_t i = 0; i < pairs.size(); i++) {
                if (verbose == 2) {
                    printTabs(3 * depth - 1);
                    std::cout << "Remaining pairs " << pairs.size() << "\n\n";
                }
                if (accl_on)
                    indexRP->push_back(pairs[i].rabin_index_);
                UBDD G = pairs[i].G_;
                UBDD nR = pairs[i].nR_;
                std::vector<rabin_pair_<UBDD>> remPairs = pairs;
                remPairs.erase(remPairs.begin() + i);
                /* initialize a local copy for the controller */
                UBDD C = base_.zero();
                ;
                /* initialize the sets for the nu fixed point */
                UBDD Y = base_.zero();
                UBDD YY;
                if (accl_on && check_threshold(*indexY, M - 1) && check_threshold(*indexX, M - 1)) {
                    //        YY = (*hist_Y)
                    //        [fp->lexi_order(*indexRP,fp->RabinPairs_.size()-1)]
                    //        [fp->to_dec(M,fp->pad_zeros(*indexX,remPairs.size()+1))]
                    //        [depth-1]
                    //        [std::min((*indexY)[0],M-1)];
                    //        YY = (*hist_Y)[fp->lexi_order(*indexRP,fp->RabinPairs_.size()-1)][fp->to_dec(M,fp->pad_zeros(*indexX,remPairs.size()+1))][depth-1];
                    YY = (*hist_Y)[depth - 1]
                                  [findRank(RabinPairs_.size(), *indexRP)]
                                  [std::min((*indexY)[0], M - 1)]
                                  [to_dec(M, *indexX)];
                } else {
                    YY = initial_seed;
                }

                //            if (accl_on && check_threshold(*indexX,M-1)) {
                //                YY = (*hist_Y)[lexi_order(*indexRP,RabinPairs_.size()-1)][to_dec(M,pad_zeros(*indexX,remPairs.size()+1))][depth];
                //            } else {
                //                YY = initial_seed;
                //            }
                for (int j = 0; Y.existAbstract(CubeNotState()) != YY.existAbstract(CubeNotState()); j++) {
                    Y = YY;
                    if (accl_on)
                        indexY->push_back(j);
                    print_rabin_info(Y, "Y", verbose, j, depth);

                    UBDD term1;
                    term1 = right | (seqR & nR & G & cpre(Y));
                    /* reset the local copy of the controller to the most recently added state-input pairs */
                    UBDD N = term1 & (!(controller.existAbstract(CubeNotState())));
                    C = controller | N;
                    /* initialize the sets for the mu fixed point */
                    UBDD X = base_.one();
                    UBDD XX;
                    if (accl_on && check_threshold(*indexY, M - 1) && check_threshold(*indexX, M - 1)) {
                        //            XX=(*hist_X)
                        //            [fp->lexi_order(*indexRP,fp->RabinPairs_.size()-1)]
                        //            [fp->to_dec(M,fp->pad_zeros(*indexY,remPairs.size()))]
                        //            [depth-1]
                        //            [std::min((*indexX)[0],M-1)];
                        //            XX=(*hist_X)[fp->lexi_order(*indexRP,fp->RabinPairs_.size()-1)][fp->to_dec(M,fp->pad_zeros(*indexY,remPairs.size()))][depth-1];
                        XX = (*hist_X)[depth - 1]
                                      [findRank(RabinPairs_.size(), *indexRP)]
                                      [std::min((*indexX)[0], M - 1)]
                                      [to_dec(M, *indexY)];
                    } else {
                        XX = base_.zero();
                    }

                    //                if (accl_on && check_threshold(*indexY,M-1)) {
                    //                    XX=(*hist_X)[lexi_order(*indexRP,RabinPairs_.size()-1)][to_dec(M,pad_zeros(*indexY,remPairs.size()))][depth];
                    //                } else {
                    //                    XX = base_.zero();
                    //                }
                    for (int k = 0; X.existAbstract(CubeNotState()) != XX.existAbstract(CubeNotState()); k++) {
                        X = XX;
                        if (accl_on)
                            indexX->push_back(k);
                        print_rabin_info(X, "X", verbose, k, depth);
                        UBDD term2;
                        term2 = term1 | (seqR & nR & apre(Y, X));
                        /* add the recently added state-input pairs to the controller */
                        N = term2 & (!(C.existAbstract(CubeNotState())));
                        C |= N;
                        if (remPairs.empty()) {
                            XX = term2;
                        } else {
                            genie::const_arg_recursive_rabin<UBDD> arg_const_new = {
                                    accl_on,
                                    M, /* the bound on the iteration count for memorizing the BDDs from the past iterations */
                                    depth + 1,
                                    remPairs,
                                    initial_seed,
                                    verbose};
                            genie::nconst_arg_recursive_rabin<UBDD> arg_nconst_new = {
                                    seqR & nR,
                                    term2,
                                    indexRP,
                                    indexY,
                                    indexX,
                                    hist_Y,
                                    hist_X};
                            XX = OldRabinRecurse(C, arg_const_new, arg_nconst_new);
                        }
                        if (accl_on)
                            indexX->pop_back();
                    }
                    YY = XX;
                    if (accl_on) {
                        if (check_threshold(*indexY, M - 1) && check_threshold(*indexX, M - 1)) {
                            if ((*hist_X)[depth - 1]
                                         [findRank(RabinPairs_.size(), *indexRP)]
                                         [std::min((*indexX)[0] + 1, M - 1)]
                                         [to_dec(M, *indexY)] <= (XX.existAbstract(cubePost_))) {
                                (*hist_X)[depth - 1]
                                         [findRank(RabinPairs_.size(), *indexRP)]
                                         [std::min((*indexX)[0] + 1, M - 1)]
                                         [to_dec(M, *indexY)] = (XX.existAbstract(cubePost_));
                            }

                            //                        (*hist_X)[lexi_order(*indexRP,RabinPairs_.size()-1)][to_dec(M,pad_zeros(*indexY,remPairs.size()))][depth]=X;
                        }
                        indexY->pop_back();
                    }
                }
                U |= YY;
                controller = C;
                if (accl_on) {
                    if (check_threshold(*indexY, M - 1) && check_threshold(*indexX, M - 1)) {
                        if ((YY.existAbstract(cubePost_)) <= (*hist_Y)[depth - 1]
                                                                      [findRank(RabinPairs_.size(), *indexRP)]
                                                                      [std::min((*indexY)[0] + 1, M - 1)]
                                                                      [to_dec(M, *indexX)]) {
                            (*hist_Y)[depth - 1]
                                     [findRank(RabinPairs_.size(), *indexRP)]
                                     [std::min((*indexY)[0] + 1, M - 1)]
                                     [to_dec(M, *indexX)] = (YY.existAbstract(cubePost_));
                        }

                        //                    (*hist_Y)[lexi_order(*indexRP,RabinPairs_.size()-1)][to_dec(M,pad_zeros(*indexX,remPairs.size()+1))][depth]=Y;
                    }
                }
                if (accl_on)
                    indexRP->pop_back();
            }
            return U;
        }
        /* function: printTabs
         *  used to print n number of tabs on the standard I/O during printing the results */
        void printTabs(const int n) {
            for (int i = 0; i < n; i++) {
                std::cout << "\t";
            }
        }
        /* function: to_dec
         *  convert a number with base "base" to a decimal number */
        size_t to_dec(const size_t base, const std::vector<size_t> &number) override {
            size_t N = 0;
            for (size_t i = 0; i < number.size(); i++) {
                N += number[i] * pow(base, i);
            }
            return N;
        }
        /* function: lexi_order
         *  determine the position of a given permutation "seq1" in the lexicographically ordered set of all permutations of N numbers */
        size_t lexi_order(const std::vector<size_t> &seq1, const size_t N) {
            /* first, make the sequence complete by filling the extra digits with the lexicographically smallest suffix */
            std::vector<size_t> seq2 = seq1;
            if (seq2.size() < N + 1) {
                for (size_t i = 0; i <= N; i++) {
                    bool number_present = false;
                    for (unsigned long j : seq2) {
                        if (j == i) {
                            number_present = true;
                            break;
                        }
                    }
                    if (!number_present)
                        seq2.push_back(i);
                }
            }
            /* second, compute the Lehmer code of the resulting padded sequence */
            std::vector<size_t> lehmer;
            for (size_t i = 0; i < seq2.size(); i++) {
                size_t right_inversion = 0;
                for (size_t j = i + 1; j < seq2.size(); j++) {
                    if (seq2[i] > seq2[j]) {
                        right_inversion++;
                    }
                }
                lehmer.push_back(right_inversion);
            }
            /* third, compute the decimal value of the Lehmer code, interpreting it as a factorial base number */
            size_t pos = 0;
            for (size_t i = 0; i < lehmer.size(); i++) {
                pos += factorial(i) * lehmer[i];
            }
            return pos;
        }
        /* function: pad_zeros
         *  pad zeros to a vector "vec1" to make its size equal to n */
        inline std::vector<size_t> pad_zeros(const std::vector<size_t>& vec1, const size_t n) {
            std::vector<size_t> vec2 = vec1;
            for (size_t i = 0; i < n; i++) {
                vec2.push_back(0);
            }
            return vec2;
        }
    }; /* close class def */
}// namespace mascot

#endif /* FIXPOINTRABIN_HH */
