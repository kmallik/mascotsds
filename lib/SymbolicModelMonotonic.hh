/** @file SymbolicModelMonotonic.hh
 *
 * @date 09.10.2015
 * @author rungger (original file=SymbolicModelGrowthBound.hh)
* @modified_by: kaushik mallik
 */

#ifndef SYMBOLICMODELMONOTONIC_HH
#define SYMBOLICMODELMONOTONIC_HH

#include <iostream>
#include <numeric>
#include <stdexcept>

#include "MascotHelper.hh"
#include "SymbolicModel.hh"
#include "lace.h"
#include "utils/TicToc.hh"


VOID_TASK_1(RUN_JOB, std::function<void(void)> *, job) {
    (*job)();
}

VOID_TASK_2(RUN_PARALLEL_JOBS, std::vector<std::function<void(void)>> *, jobs, size_t, size) {
    for (size_t i = 0; i < size; i++)
        SPAWN(RUN_JOB, &(*jobs)[i]);
    for (size_t i = 0; i < size; i++)
        SYNC(RUN_JOB);
}

namespace mascot {

    /**
     * @brief Derived class from <SymbolicModel>
     *
     * Constructs a symbolic model according to
     * the theory in http://arxiv.org/abs/1503.03715
     *
     */
    template <class stateType_, class inputType_, class distType_, class UBDD>
    class SymbolicModelMonotonic : public SymbolicModel<UBDD> {
    private:
        const size_t buffer_size = 100000;
        const size_t buffer_part_size = 200;

    public:
        using SymbolicModel<UBDD>::SymbolicModel;
        typedef stateType_ (*DecompositionType)(inputType_ &, distType_ &, stateType_ &, stateType_ &);

        /**
         * @brief Approximate the transitions using mixed-monotonicity property
         * @details
         * Assumption: a decomposition function is available
         *          which is a function of the input and two points in the state space
         *
         * We do not consider measurement errors
         *
         */
        void computeTransitions(DecompositionType &decomposition, const stateType_ &W_ub) {
            bool multi_threading_on = this->base_.isParallelSafe();
            std::vector<std::pair<UBDD, UBDD>> jobs_output(1, std::pair<UBDD, UBDD>{this->base_.zero(), this->base_.zero()});
            std::vector<std::pair<UBDD, UBDD>> merged_outputs(1, std::pair<UBDD, UBDD>{this->base_.zero(), this->base_.zero()});
            std::vector<std::function<void(void)>> jobs;
            std::vector<std::function<void(void)>> merging_output_jobs;
            if (multi_threading_on) {
                jobs_output.resize(buffer_size, std::pair<UBDD, UBDD>{this->base_.zero(), this->base_.zero()});
                merged_outputs.resize(buffer_size, std::pair<UBDD, UBDD>{this->base_.zero(), this->base_.zero()});
                jobs.resize(buffer_size);
                merging_output_jobs.resize(buffer_part_size);
                for (size_t i = 0; i < buffer_part_size; i++) {
                    merging_output_jobs[i] = [&, i]() {
                        UBDD maybeTransitionPartial = jobs_output[i].first;
                        UBDD sureTransitionPartial = jobs_output[i].second;
                        for (size_t j = i * (buffer_size / buffer_part_size); j < (i + 1) * (buffer_size / buffer_part_size); j++) {
                            maybeTransitionPartial += jobs_output[j].first;
                            sureTransitionPartial += jobs_output[j].second;
                        }
                        merged_outputs[i].first = maybeTransitionPartial;
                        merged_outputs[i].second = sureTransitionPartial;
                        return;
                    };
                }
            }

            /* create the UBDD's with numbers 0,1,2,.., #gridPoints */
            size_t dim = this->stateSpace_->getDimension();
            const std::vector<size_t> nvars = this->stateSpace_->getNofBddVars();
            std::vector<std::vector<UBDD>> bddVars(dim);
            for (size_t n = 0, i = 0; i < dim; i++) {
                for (size_t j = 0; j < nvars[i]; j++) {
                    bddVars[i].push_back(this->base_.var(this->postVars_[n + j]));
                }
                n += nvars[i];
            }
            const std::vector<size_t> ngp = this->stateSpace_->getNofGridPoints();
            std::vector<int> phase;
            std::vector<std::vector<UBDD>> num(dim);
            for (size_t i = 0; i < dim; i++) {
                num[i] = std::vector<UBDD>(ngp[i], this->base_.zero());
                // phase.resize(nvars[i], 0);
                phase = std::vector<int>(nvars[i], 0);

                for (int j = 0; j < ngp[i]; j++) {
                    for (int x = j, k = 0; x; x /= 2, k++) phase[k] = 0 + x % 2;
                    num[i][j] = num[i][j].cube(bddVars[i], phase);
                }
            }

            /* bdd nodes in pre and input variables */
            size_t ndom = this->nssVars_ + this->nisVars_ + this->ndsVars_;
            // phase.resize(ndom);
            phase = std::vector<int>(ndom);
            std::vector<UBDD> dvars(ndom);
            for (size_t i = 0; i < this->nssVars_; i++)
                dvars[i] = this->base_.var(this->preVars_[i]);
            for (size_t i = 0; i < this->nisVars_; i++)
                dvars[this->nssVars_ + i] = this->base_.var(this->inpVars_[i]);
            for (size_t i = 0; i < this->ndsVars_; i++)
                dvars[this->nssVars_ + this->nisVars_ + i] = this->base_.var(this->distVars_[i]);
            /* initialize cell radius
             * used to compute the corners of the cell */
            stateType_ eta(dim), r(dim);
            this->stateSpace_->copyEta(&eta[0]);
            /* the first grid points are used to compute the id of the current grid point */
            stateType_ first(dim);
            this->stateSpace_->copyFirstGridPoint(&first[0]);
            /* initialize transitions as empty set */
            this->maybeTransition_ = this->base_.zero();
            this->sureTransition_ = this->base_.zero();
            /* compute constraint set against the post is checked */

            UBDD ss = this->stateSpace_->getSymbolicSet();
            UBDD constraints = ss.permute(this->preVars_, this->postVars_);

            size_t buffer_index = 0;
            for (this->begin(); !this->done(); this->next()) {
                this->progress();
                const std::vector<size_t> minterm = this->currentMinterm();
                /* current state */
                stateType_ x(dim);
                this->stateSpace_->mintermToElement(minterm, x);
                /* current input */
                size_t iDim = this->inputSpace_->getDimension();
                inputType_ u(iDim);
                this->inputSpace_->mintermToElement(minterm, u);
                /* current disturbance input */
                size_t dDim = this->distSpace_->getDimension();
                distType_ d(dDim);
                this->distSpace_->mintermToElement(minterm, d);
                /* compute bdd for the current x, u, and w element and add x' */
                for (size_t i = 0; i < this->nssVars_; i++)
                    phase[i] = minterm[this->preVars_[i]];
                for (size_t i = 0; i < this->nisVars_; i++)
                    phase[this->nssVars_ + i] = minterm[this->inpVars_[i]];
                for (size_t i = 0; i < this->ndsVars_; i++)
                    phase[this->nssVars_ + this->nisVars_ + i] = minterm[this->distVars_[i]];
                //                    BDD current(this->base_, UBDDComputeCube(mgr, dvars, phase, ndom));

                if (multi_threading_on) {
                    // It creates functions to run "addTransitions" parallel
                    size_t phase_t = vectorToSize_t(phase);
                    jobs[buffer_index] = [&, x, u, d, phase_t, buffer_index]() {
                        std::vector<uint8_t> phase_lambda = size_tToVector(phase_t, ndom);
                        UBDD current = this->base_.cube(dvars, phase_lambda);

                        // This function saves copies of values: x, u, d, phase; and has references to every other.
                        jobs_output[buffer_index] = calculateTransitionsShift(x, u, d, current, decomposition, dim, eta, first, W_ub, constraints, ngp, num);
                        return;
                    };
                    buffer_index++;
                    if (buffer_index == buffer_size) {
                        RUN(RUN_PARALLEL_JOBS, &jobs, buffer_index);
                        RUN(RUN_PARALLEL_JOBS, &merging_output_jobs, buffer_part_size);
                        for (size_t i = 0; i < buffer_part_size; i++) {
                            this->maybeTransition_ += merged_outputs[i].first;
                            this->sureTransition_ += merged_outputs[i].second;
                        }
                        buffer_index = 0;
                    }
                } else {
                    UBDD current = this->base_.cube(dvars, phase);
                    addTransitions(x, u, d, current, decomposition, dim, eta, first, W_ub,
                                   constraints, ngp, num,
                                   &(this->maybeTransition_), &(this->sureTransition_));
                }
            }
            if (multi_threading_on) {// Run the rest of the jobs
                RUN(RUN_PARALLEL_JOBS, &jobs, buffer_index);
                RUN(RUN_PARALLEL_JOBS, &merging_output_jobs, buffer_part_size);
                for (size_t i = 0; i < buffer_part_size; i++) {
                    this->maybeTransition_ += merged_outputs[i].first;
                    this->sureTransition_ += merged_outputs[i].second;
                }
            }
        }

    private:
        void addTransitions(const stateType_ x, const inputType_ u, const distType_ d, const UBDD &current,
                            const DecompositionType &decomposition,
                            const size_t dim,
                            const stateType_ eta, const stateType_ first,
                            const stateType_ &W_ub,
                            const UBDD &constraints, const std::vector<size_t> &ngp, const std::vector<std::vector<UBDD>> &num,
                            UBDD *maybeTransition, UBDD *sureTransition) {
            std::pair<UBDD, UBDD> transitionsShift = calculateTransitionsShift(x, u, d, current, decomposition, dim, eta, first, W_ub, constraints, ngp, num);
            *maybeTransition += transitionsShift.first;
            *sureTransition += transitionsShift.second;
        }

        std::pair<UBDD, UBDD> calculateTransitionsShift(const stateType_ x, const inputType_ u, const distType_ d, const UBDD &current,
                                                        const DecompositionType &decomposition,
                                                        const size_t dim,
                                                        const stateType_ eta, const stateType_ first,
                                                        const stateType_ &W_ub,
                                                        const UBDD &constraints, const std::vector<size_t> &ngp, const std::vector<std::vector<UBDD>> &num) {
            UBDD maybeTransitionShift = this->base_.zero();
            UBDD sureTransitionShift = this->base_.zero();
            stateType_ r;
            /* cell radius (including measurement errors) */
            for (size_t i = 0; i < dim; i++)
                r.push_back(eta[i] / 2.0);

            /* Compute the corners of the current state */
            stateType_ x_lb(dim), temp(dim);
            stateType_ x_ub(dim);
            for (size_t i = 0; i < dim; i++) {
                x_lb[i] = x[i] - r[i];
                x_ub[i] = x[i] + r[i];
            }
            inputType_ u_copy = u;
            distType_ d_copy = d;
            /* compute the corners of the nominal recahble set */
            temp = decomposition(u_copy, d_copy, x_lb, x_ub); /* temporary storage for x_lb */
            x_ub = decomposition(u_copy, d_copy, x_ub, x_lb);
            x_lb = temp;

            /************ Compute the maybe transitions ******************/
            /* compute the over-approximation of the perturbed reachable set */
            stateType_ outer_lb(dim), outer_ub(dim);
            int lb, ub;
            for (size_t i = 0; i < dim; i++) {
                outer_lb[i] = x_lb[i] - W_ub[i];
                outer_ub[i] = x_ub[i] + W_ub[i];
            }
            /* determine the cells which intersect with the overapproximation set*/
            UBDD post = this->base_.one();
            for (size_t i = 0; i < dim; i++) {
                lb = std::lround(((outer_lb[i] - first[i]) / eta[i]));
                ub = std::lround(((outer_ub[i] - first[i]) / eta[i]));
                if (lb < 0 || ub >= (int)ngp[i]) {
                    post = this->base_.zero();
                    break;
                }
                UBDD zz = this->base_.zero();
                for (int j = lb; j <= ub; j++) {
                    zz |= num[i][j];
                }
                post &= zz;
            }

            bool compute_sure_flag;
            if (post != this->base_.zero() && post <= constraints) {
                maybeTransitionShift = current & post;
                compute_sure_flag = true;
            } else { /* both the maybe and the sure transitions are empty */
                /* skip compuation of sure transitions */
                compute_sure_flag = false;
            }

            if (compute_sure_flag) {
                /********** Compute the sure transitions *****************/
                /* compute the under-approximation of the perturbed reachable set */
                for (size_t i = 0; i < dim; i++) {
                    /* the reachable set is obtained by taking offset from the boundary */
                    outer_lb[i] = x_ub[i] - W_ub[i];
                    outer_ub[i] = x_lb[i] + W_ub[i];
                }
                /* determine the cells which intersect with the reachable set*/
                UBDD post_sure = this->base_.one();
                for (size_t i = 0; i < dim; i++) {
                    lb = std::lround(((outer_lb[i] - first[i]) / eta[i]));
                    ub = std::lround(((outer_ub[i] - first[i]) / eta[i]));
                    if (lb < 0 || ub >= (int)ngp[i] || ((outer_lb[i] >= outer_ub[i]) && (lb != ub))) {
                        post_sure = this->base_.zero();
                        break;
                    }
                    UBDD zz = this->base_.zero();
                    for (int j = lb; j <= ub; j++) {
                        zz |= num[i][j];
                    }
                    post_sure &= zz;
                }
                /* check the constraint satisfaction and non-emptiness of transition */
                if (post_sure != this->base_.zero() && post_sure <= constraints) {
                    sureTransitionShift = current & post_sure;
                }
            }

            return std::pair<UBDD, UBDD>{maybeTransitionShift, sureTransitionShift};
        }

        size_t vectorToSize_t(std::vector<int> phase) {
            size_t result = 0;
            if (64 < phase.size()) {
                cout << "Possible Error: phase too big";
            }
            for (size_t i = 0; i < phase.size(); i++) {
                result += (size_t)phase[i] << i;
            }

            return result;
        }

        std::vector<uint8_t> size_tToVector(size_t phase_t, size_t size) {
            std::vector<uint8_t> result(size);
            for (size_t i = 0; i < size; i++) {
                result[i] = ((phase_t >> i) & 1);
            }
            return result;
        }
    }; /* close class def */


}// namespace mascot

#endif /* SYMBOLICMODELMONOTONIC_HH */
