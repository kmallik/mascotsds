#ifndef MASCITHELPER_HH_
#define MASCITHELPER_HH_

namespace mascot {
    template <class UBDD>
    class SymbolicSet;

    template <class UBDD>
    class SymbolicModel;

    template <class stateType_, class inputType_, class distType_, class UBDD>
    class SymbolicModelMonotonic;

    template <class UBDD>
    class RabinAutomaton;

    template <class UBDD>
    class FixpointRabin;

    enum ApproximationType { INNER = 1,
                             OUTER = 0 };
}// namespace mascot
#endif /* MASCITHELPER_HH_ */