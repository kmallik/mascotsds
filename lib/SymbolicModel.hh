/** @file SymbolicModel.hh
 *
 * @date 09.10.2015
 * @author rungger
 * @modified_by: kaushik mallik
 */

#ifndef SYMBOLICMODEL_HH_
#define SYMBOLICMODEL_HH_

#include <iostream>
#include <stdexcept>

#include "FixpointRabin.hh"
#include "MascotHelper.hh"
#include "SymbolicSet.hh"
#include "ubdd/UBDDMintermIterator.hh"


namespace mascot {
    /**
     * @brief base class for <SymbolicModelGrowthBound>
     *
     * @details
     * it stores the ubdd of the transition relation and its ubdd variable information
     * it provides an iterator to loop over all elements in the stateSpace and inputSpace
     *
     * the actual computation of the transition relation is computs in the derived
     * classes
     */
    template <class UBDD>
    class SymbolicModel {
    public:
        friend class FixpointRabin<UBDD>;

    public:
        UBDD base_;
        SymbolicSet<UBDD> *stateSpace_;
        SymbolicSet<UBDD> *inputSpace_;
        SymbolicSet<UBDD> *distSpace_;
        SymbolicSet<UBDD> *stateSpacePost_;
        size_t nssVars_;                       /**< number of state space ubdd variables (= number of pre/post variables) */
        std::vector<size_t> preVars_;          /**< array of indices of the state space pre ubdd variables */
        std::vector<size_t> postVars_;         /**< array of indices of the state space post ubdd variables */
        size_t nisVars_;                       /**< number of input space ubdd variables */
        std::vector<size_t> inpVars_;          /**< array of indices of the input space bdd variables */
        size_t ndsVars_;                       /**< number of disturbance space ubdd variables */
        std::vector<size_t> distVars_;         /**< array of indices of the disturbance space ubdd variables */
        genie::UBDDMintermIterator *iterator_; /**< UBDDMintermIterator to iterate over all elements in stateSpace_ and inputSpace_ */
        UBDD it;
        UBDD maybeTransition_; /**< the bdd representation of the over-approximating transition relation  X x U x X
                                * the bdd variables of the transition relation are
                                * given by preVars_ x inpVars_ x postVars_ */
        UBDD sureTransition_;  /**< the bdd representation of the under-approximating transition relation  X x U x X
                                * the bdd variables of the transition relation are
                                * given by preVars_ x inpVars_ x postVars_ */

    public:
        /**
         * @brief Representation of the transition relation as UBDD in the space
         * @details
         *   preX  x U x W x postX
         *
         * provide SymbolicSet for preX
         * provide SymbolicSet for U
         * provide SymbolicSet for W
         * provide SymbolicSet for postX
         *
         * the SymbolicSet for preX and postX need to be identical, except the
         * BDD variable IDs need to differ
         */
        SymbolicModel(SymbolicSet<UBDD> *stateSpace,
                      SymbolicSet<UBDD> *inputSpace,
                      SymbolicSet<UBDD> *distSpace,
                      SymbolicSet<UBDD> *stateSpacePost) : stateSpace_(stateSpace), inputSpace_(inputSpace), distSpace_(distSpace), stateSpacePost_(stateSpacePost) {
            if (not(stateSpace_->base_.isCoverEqual(inputSpace_->base_) and stateSpacePost_->base_.isCoverEqual(inputSpace_->base_))) {
                std::ostringstream os;
                os << "Error: SymbolicModel: stateSpace and inputSpace need to have the same dd manager.";
                throw std::invalid_argument(os.str().c_str());
            }
            /* check if stateSpace and stateSpacePost have the same domain and grid parameter */
            int differ = 0;
            if (stateSpace_->dim_ != stateSpacePost_->dim_)
                differ = 1;
            for (size_t i = 0; i < stateSpace_->dim_; i++) {
                if (stateSpace_->firstGridPoint_[i] != stateSpacePost_->firstGridPoint_[i])
                    differ = 1;
                if (stateSpace_->nofGridPoints_[i] != stateSpacePost_->nofGridPoints_[i])
                    differ = 1;
                if (stateSpace_->eta_[i] != stateSpacePost_->eta_[i])
                    differ = 1;
            }
            if (differ) {
                std::ostringstream os;
                os << "Error: SymbolicModel: stateSpace and stateSpacePost need have the same domain and grid parameter.";
                throw std::invalid_argument(os.str().c_str());
            }
            /* check if stateSpace and stateSpacePost have different BDD IDs */
            for (size_t i = 0; i < stateSpace_->dim_; i++)
                for (size_t j = 0; j < stateSpace_->nofBddVars_[i]; j++)
                    if (stateSpace_->indBddVars_[i][j] == stateSpacePost_->indBddVars_[i][j])
                        differ = 1;
            if (differ) {
                std::ostringstream os;
                os << "Error: SymbolicModel: stateSpace and stateSpacePost are not allowed to have the same BDD IDs.";
                throw std::invalid_argument(os.str().c_str());
            }
            base_ = stateSpace_->base_;
            nssVars_ = 0;
            for (size_t i = 0; i < stateSpace_->dim_; i++)
                for (size_t j = 0; j < stateSpace_->nofBddVars_[i]; j++)
                    nssVars_++;
            nisVars_ = 0;
            for (size_t i = 0; i < inputSpace_->dim_; i++)
                for (size_t j = 0; j < inputSpace_->nofBddVars_[i]; j++)
                    nisVars_++;
            ndsVars_ = 0;
            for (size_t i = 0; i < distSpace_->dim_; i++)
                for (size_t j = 0; j < distSpace_->nofBddVars_[i]; j++)
                    ndsVars_++;
            /* set the preVars_ to the variable indices of stateSpace_
             * set the postVars_ to the variable indices of stateSpacePost_ */
            preVars_ = std::vector<size_t>(nssVars_);
            postVars_ = std::vector<size_t>(nssVars_);
            for (size_t k = 0, i = 0; i < stateSpace_->dim_; i++) {
                for (size_t j = 0; j < stateSpace_->nofBddVars_[i]; k++, j++) {
                    preVars_[k] = stateSpace_->indBddVars_[i][j];
                    postVars_[k] = stateSpacePost_->indBddVars_[i][j];
                }
            }
            inpVars_ = std::vector<size_t>(nisVars_);
            for (size_t k = 0, i = 0; i < inputSpace_->dim_; i++) {
                for (size_t j = 0; j < inputSpace_->nofBddVars_[i]; k++, j++) {
                    inpVars_[k] = inputSpace_->indBddVars_[i][j];
                }
            }
            distVars_ = std::vector<size_t>(ndsVars_);
            for (size_t k = 0, i = 0; i < distSpace_->dim_; i++) {
                for (size_t j = 0; j < distSpace_->nofBddVars_[i]; k++, j++) {
                    distVars_[k] = distSpace_->indBddVars_[i][j];
                }
            }
            /* initialize the transition relations */
            maybeTransition_ = stateSpace_->symbolicSet_ * inputSpace_->symbolicSet_ * distSpace_->symbolicSet_;
            it = maybeTransition_;
            sureTransition_ = stateSpace_->symbolicSet_ * inputSpace_->symbolicSet_ * distSpace_->symbolicSet_;
        }

        /**
         * @brief create a SymbolicModel with the same set representations but on a different manager
         */
        SymbolicModel(UBDD &base, const SymbolicModel &other) {
            base_ = base;
            auto *ss_new = new SymbolicSet<UBDD>(base_, *other.stateSpace_);
            stateSpace_ = ss_new;
            auto *is_new = new SymbolicSet<UBDD>(base_, *other.inputSpace_);
            inputSpace_ = is_new;
            auto *ds_new = new SymbolicSet<UBDD>(base_, *other.distSpace_);
            distSpace_ = ds_new;
            auto *sspost_new = new SymbolicSet<UBDD>(base_, *other.stateSpacePost_);
            stateSpacePost_ = sspost_new;
            nssVars_ = other.nssVars_;
            preVars_ = std::vector<size_t>(nssVars_);
            postVars_ = std::vector<size_t>(nssVars_);
            for (size_t i = 0; i < nssVars_; i++) {
                preVars_[i] = other.preVars_[i];
                postVars_[i] = other.postVars_[i];
            }
            nisVars_ = other.nisVars_;
            inpVars_ = std::vector<size_t>(nisVars_);
            for (size_t i = 0; i < nisVars_; i++) {
                inpVars_[i] = other.inpVars_[i];
            }
            ndsVars_ = other.ndsVars_;
            distVars_ = std::vector<size_t>(ndsVars_);
            for (size_t i = 0; i < ndsVars_; i++) {
                distVars_[i] = other.distVars_[i];
            }
            maybeTransition_ = other.maybeTransition_.transfer(base_);
            it = maybeTransition_;
            sureTransition_ = other.sureTransition_.transfer(base_);
        }

        /**
         * @brief get the number of elements in the maybe transition relation
         */
        inline double getSizeMaybe() {
            return maybeTransition_.countMinterm(2 * nssVars_ + nisVars_ + ndsVars_);
        }

        /**
         * @brief get the number of elements in the maybe transition relation
         */
        inline double getSizeSure() {
            return sureTransition_.countMinterm(2 * nssVars_ + nisVars_ + ndsVars_);
        }

        /**
         * @brief get the SymbolicSet which represents maybe transition relation in X x U x W x X
         */
        inline SymbolicSet<UBDD> getMaybeTransitions() const {
            /* create SymbolicSet representing the post stateSpace_*/
            /* make a cope of the SymbolicSet of the preSet */
            SymbolicSet<UBDD> post(*stateSpace_);
            /* set the BDD indices to the BDD indices used for the post stateSpace_ */
            post.setIndBddVars(postVars_);
            /* create SymbolicSet representing the stateSpace_ x inputSpace_ */
            SymbolicSet<UBDD> sis(*stateSpace_, *inputSpace_);
            /* create SymbolicSet representing the stateSpace_ x inputSpace_ x distSpace_ */
            SymbolicSet<UBDD> sids(sis, *distSpace_);
            /* create SymbolicSet representing the stateSpace_ x inputSpace_ x distSpace_ x stateSpace_ */
            SymbolicSet<UBDD> tr(sids, post);
            /* fill SymbolicSet with transition relation */
            tr.setSymbolicSet(maybeTransition_);
            return tr;
        };

        /**
         * @brief get the SymbolicSet which represents maybe transition relation in X x U x W x X
         */
        inline SymbolicSet<UBDD> getSureTransitions() const {
            /* create SymbolicSet representing the post stateSpace_*/
            /* make a cope of the SymbolicSet of the preSet */
            SymbolicSet<UBDD> post(*stateSpace_);
            /* set the BDD indices to the BDD indices used for the post stateSpace_ */
            post.setIndBddVars(postVars_);
            /* create SymbolicSet representing the stateSpace_ x inputSpace_ */
            SymbolicSet<UBDD> sis(*stateSpace_, *inputSpace_);
            /* create SymbolicSet representing the stateSpace_ x inputSpace_ x distSpace_ */
            SymbolicSet<UBDD> sids(sis, *distSpace_);
            /* create SymbolicSet representing the stateSpace_ x inputSpace_ x distSpace_ x stateSpace_ */
            SymbolicSet<UBDD> tr(sids, post);
            /* fill SymbolicSet with transition relation */
            tr.setSymbolicSet(sureTransition_);
            return tr;
        }

        /**
         * @brief set the transitionRelations to the ones given as input
         */
        void setTransitionRelations(UBDD maybeTransition, UBDD sureTransition) {
            maybeTransition_ = maybeTransition;
            sureTransition_ = sureTransition;
        }

        /**
         * @brief get the total number of vars
         */
        inline size_t nVars() {
            return nssVars_ + nisVars_ + ndsVars_;
        }

        /**
         * @brief get the total number of minterms
         */
        inline double numberOfMinterms() {
            return iterator_->numberOfMinterms();
        }

        /**
         * @brief initilize the iterator and compute the first element
         */
        inline void begin() {
            size_t nvars_ = nssVars_ + nisVars_ + ndsVars_;
            std::vector<size_t> ivars_;
            ivars_.reserve(nvars_);
            for (size_t i = 0; i < nssVars_; i++)
                ivars_.push_back(preVars_[i]);
            for (size_t i = 0; i < nisVars_; i++)
                ivars_.push_back(inpVars_[i]);
            for (size_t i = 0; i < ndsVars_; i++)
                ivars_.push_back(distVars_[i]);
            /* set up iterator */
            iterator_ = it.generateMintermIterator(ivars_);
        }
        /**
         * @brief compute the next minterm
         */
        inline void next() {
            iterator_->operator++();
        }

        /**
         * @brief print progess of iteratin in percent
         */
        inline void progress() const {
            iterator_->printProgress();
        }

        /**
         * @brief changes to one if iterator reached the last element
         */
        inline int done() {
            if (iterator_->done()) {
                delete iterator_;
                iterator_ = nullptr;
                return 1;
            } else
                return 0;
        }

        /**
         * @brief returns the pointer to the current minterm in the iteration
         */
        inline const std::vector<size_t> &currentMinterm() const {
            if (iterator_) {
                return iterator_->currentMinterm();
            } else {
                static const std::vector<size_t> empty;
                return empty;
            }
        }
    }; /* close class def */
}// namespace mascot

#endif /* SYMBOLICMODEL_HH_ */
