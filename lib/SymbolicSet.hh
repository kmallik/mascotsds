/** @file SymbolicSet.hh
 *
 * @date 12.09.2015
 * @author rungger
 * @modified_by: kaushik mallik
 */

#ifndef SYMBOLICSET_HH_
#define SYMBOLICSET_HH_

#include <algorithm>
#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "MascotHelper.hh"
#include "ubdd/UBDDMintermIterator.hh"

namespace mascot {
    /**
     * @brief symbolic (bdd) implementation of a set of points aligned on a uniform grid
     *
     * @details
     * Properties:
     * - the grid points are distributied uniformly in each dimension
     * - the domain of the unfiorm grid is defined by a hyper interval
     * - each grid pont is associated with a cell, i.e. a hyper rectangle with
     *   radius eta/2+z centered at the grid point
     *
     * Grid point alignment:
     * - the origin is a grid point (not necessarily contained in the set)
     * - the distance of the grid points in each dimension i is defined by eta[i]
     *
     * See
     * - http://arxiv.org/abs/1503.03715 for theoretical background
     *
     */
    template <class UBDD>
    class SymbolicSet {
    private:
        friend class SymbolicModel<UBDD>;
        UBDD base_;                                   /**< UBDD manager */
        size_t dim_;                                  /**< dimension of the real space */
        std::vector<double> eta_;                     /**< dim_-dimensional vector containing the grid node distances */
        std::vector<double> z_;                       /**< dim_-dimensional vector containing the measurement error bound */
        std::vector<double> firstGridPoint_;          /**< dim_-dimensinal vector containing the real values of the first grid point */
        std::vector<double> lastGridPoint_;           /**< dim_-dimensinal vector containing the real values of the last grid point */
        std::vector<size_t> nofGridPoints_;           /**< integer array[dim_] containing the grid points in each dimension */
        std::vector<size_t> nofBddVars_;              /**< integer array[dim_] containing the number of bdd variables in each dimension */
        std::vector<std::vector<size_t>> indBddVars_; /**< 2D integer array[dim_][nofBddVars_] containing the indices (=IDs) of the bdd variables */
        size_t nvars_;                                /**< total number of bdd variables representing the support of the set */
        UBDD symbolicSet_;                            /**< the bdd representing the set of points */
        genie::UBDDMintermIterator *iterator_;        /**< class to iterate over all elements in the symbolic set */

    public:
        /**
         * @brief  constructor: provide uniform grid parameters and domain defining hyper interval
         *
         * @param base           - the ubdd manager
         * @param lb             - lower left corner of the domain hyper interval
         * @param ub             - upper right corner of the domain hyper interval
         * @param eta            - grid point distances
         * @param grid_alignment - 0 (default): origin is a gridpoint (mascot default), 1: origin is a corner
         */
        SymbolicSet(UBDD &base, const size_t dim, const std::vector<double> &lb, const std::vector<double> &ub, const std::vector<double> &eta, const int grid_alignment = 0, const std::vector<double> &z = std::vector<double>()) {
            for (size_t i = 0; i < dim; i++) {
                if ((ub[i] - lb[i]) < eta[i]) {
                    std::ostringstream os;
                    os << "Error: SymbolicSet:  each interval must contain at least one cell.";
                    throw std::invalid_argument(os.str().c_str());
                }
            }
            base_ = base;
            dim_ = dim;
            z_ = std::vector<double>(dim);
            eta_ = std::vector<double>(dim);
            lastGridPoint_ = std::vector<double>(dim);
            firstGridPoint_ = std::vector<double>(dim);
            nofGridPoints_ = std::vector<size_t>(dim);
            nofBddVars_ = std::vector<size_t>(dim);
            indBddVars_ = std::vector<std::vector<size_t>>(dim);
            symbolicSet_ = base_.zero();
            iterator_ = nullptr;

            double Nl, Nu;

            /* determine number of bits in each dimension and initialize the Bdd variables */
            for (size_t i = 0; i < dim; i++) {
                if (z.empty())
                    z_[i] = 0;
                else
                    z_[i] = z[i];

                eta_[i] = eta[i];

                if (grid_alignment == 0) {
                    Nl = std::ceil(lb[i] / eta[i]);
                    Nu = std::floor(ub[i] / eta[i]);
                } else if (grid_alignment == 1) {
                    Nl = std::floor((lb[i] + eta[i] / 2) / eta[i]);
                    Nu = std::ceil((ub[i] - eta[i] / 2) / eta[i]);
                } else {
                    std::ostringstream os;
                    os << "Error: SymbolicSet: the variable grid_alignment should be either 0 or 1.";
                    throw std::invalid_argument(os.str().c_str());
                }

                /* number of grid points */
                if (grid_alignment == 0)
                    nofGridPoints_[i] = (size_t)std::abs(Nu - Nl) + 1;
                else if (grid_alignment == 1)
                    nofGridPoints_[i] = (size_t)std::abs(Nu - Nl);
                /* number of bits */
                if (nofGridPoints_[i] == 1)
                    nofBddVars_[i] = 1;
                else
                    nofBddVars_[i] = (size_t)std::ceil(std::log2(nofGridPoints_[i]));
                /* determine the indices */
                indBddVars_[i] = std::vector<size_t>(nofBddVars_[i]);
                for (size_t j = 0; j < nofBddVars_[i]; j++) {
                    UBDD var = base_.var();
                    indBddVars_[i][j] = var.nodeIndex();
                }
                /* first and last grid point coordinates */
                if (grid_alignment == 0) {
                    lastGridPoint_[i] = Nu * eta[i];
                    firstGridPoint_[i] = Nl * eta[i];
                } else if (grid_alignment == 1) {
                    lastGridPoint_[i] = Nu * eta[i] - eta[i] / 2;
                    firstGridPoint_[i] = Nl * eta[i] + eta[i] / 2;
                }
            }
            /* number of total variables */
            nvars_ = 0;
            for (size_t i = 0; i < dim_; i++)
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    nvars_++;
        }

        /**
         * @brief constructor: read the symbolic set from file
         * @details
         * newID=0 - the bdd variable ids (used for the symbolic set) are taken from file
         * newID=1 - the bdd variable ids (used for the symbolic set) are newly generated */
        SymbolicSet(UBDD &base, const char *filename, int newID = 0) {
            base_ = base;
            iterator_ = nullptr;
            /* read the SymbolicSet members from filename.meta */
            std::string fname_meta(filename);
            fname_meta += ".meta";
            readMembersFromFile(fname_meta.c_str());
            std::vector<int> composeids;
            /* do we need to create new variables ? */
            if (newID) {
                /* allocate memory for comopsids */
                size_t maxoldid = 0;
                for (size_t i = 0; i < dim_; i++)
                    for (size_t j = 0; j < nofBddVars_[i]; j++)
                        maxoldid = ((maxoldid < indBddVars_[i][j]) ? indBddVars_[i][j] : maxoldid);
                composeids = std::vector<int>(maxoldid + 1);
                /* match old id's (read from file) with newly created ones */
                for (size_t i = 0; i < dim_; i++) {
                    for (size_t j = 0; j < nofBddVars_[i]; j++) {
                        UBDD ubdd = base_.var();
                        composeids[indBddVars_[i][j]] = ubdd.nodeIndex();
                        indBddVars_[i][j] = ubdd.nodeIndex();
                    }
                }
                /* number of total variables */
            }
            /* load bdd */
            FILE *file = fopen(filename, "r");
            if (file == nullptr) {
                std::ostringstream os;
                os << "Error: Unable to open file:" << filename << "'.";
                throw std::runtime_error(os.str().c_str());
            }
            symbolicSet_ = base_.load(file, composeids, newID);
            fclose(file);
        }

        /**
         * @brief constructor: create a symbolicset with the same set representation but on a different manager
         */
        SymbolicSet(UBDD &base, const SymbolicSet &other) {
            *this = other;
            base_ = base;
            this->clear();
            symbolicSet_ = other.symbolicSet_.transfer(base_);
        }

        /**
         * @brief copy constructor with optinal newID flag
         * @details
         * newID=0 - the bdd variable ids (used for the SymbolicSet) are taken from other
         * newID=1 - the bdd variable ids (used for the SymbolicSet) are newly generated
         */
        SymbolicSet(const SymbolicSet &other, int newID = 0) {
            *this = other;
            this->clear();
            /* create new bdd variable ids */
            if (newID) {
                for (size_t i = 0; i < dim_; i++)
                    for (size_t j = 0; j < nofBddVars_[i]; j++) {
                        UBDD var = base_.var();
                        indBddVars_[i][j] = var.nodeIndex();
                    }
            }
        }

        /**
         * @brief copy constructor which returns the projection of the given <SymbolicSet> onto the
         * dimensions stored in std::vector<size_t> pdim
         *
         * @details
         * For example, if pdim={1,2}, then the <SymbolicSet> resulting from the
         * projeciton of the <SymbolicSet> other onto the first two dimension is returned
         *
         */
        SymbolicSet(const SymbolicSet &other, const std::vector<size_t> &pdim) {
            dim_ = pdim.size();
            base_ = other.base_;
            if (!dim_) {
                std::ostringstream os;
                os << "Error: SymbolicSet::SymbolicSet(const SymbolicSet& other, std::vector<size_t> pdim): projection indices pdim should be non-empty.";
                throw std::runtime_error(os.str().c_str());
            }
            /* check if values in pdim are in range [1;n] */
            size_t oor = 0;
            for (auto i : pdim) {
                if ((i == 0) || (i > other.dim_))
                    oor = 1;
            }
            if (oor) {
                std::ostringstream os;
                os << "Error: SymbolicSet::SymbolicSet(const SymbolicSet& other, std::vector<size_t> pdim): the elements in pdim need to be within [1...other.dim_].";
                throw std::runtime_error(os.str().c_str());
            }
            z_ = std::vector<double>(dim_);
            eta_ = std::vector<double>(dim_);
            lastGridPoint_ = std::vector<double>(dim_);
            firstGridPoint_ = std::vector<double>(dim_);
            nofGridPoints_ = std::vector<size_t>(dim_);
            nofBddVars_ = std::vector<size_t>(dim_);
            indBddVars_ = std::vector<std::vector<size_t>>(dim_);
            symbolicSet_ = base_.zero();
            iterator_ = nullptr;
            for (size_t i = 0; i < dim_; i++) {
                z_[i] = other.z_[pdim[i] - 1];
                eta_[i] = other.eta_[pdim[i] - 1];
                lastGridPoint_[i] = other.lastGridPoint_[pdim[i] - 1];
                firstGridPoint_[i] = other.firstGridPoint_[pdim[i] - 1];
                nofGridPoints_[i] = other.nofGridPoints_[pdim[i] - 1];
                nofBddVars_[i] = other.nofBddVars_[pdim[i] - 1];
            }
            for (size_t i = 0; i < dim_; i++) {
                indBddVars_[i] = std::vector<size_t>(other.nofBddVars_[pdim[i] - 1]);
                for (size_t j = 0; j < other.nofBddVars_[pdim[i] - 1]; j++)
                    indBddVars_[i][j] = other.indBddVars_[pdim[i] - 1][j];
            }
            /* number of total variables */
            nvars_ = 0;
            for (size_t i = 0; i < dim_; i++)
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    nvars_++;
        }

        /**
         * @details constructor: from two other SymbolicSets
         * @details
         * create SymbolicSet from two other SymbolicSets
         * the uniform grid results from the Cartesian product
         * of the uniform grids of the two input SymbolicSets
         */
        SymbolicSet(const SymbolicSet &set1, const SymbolicSet &set2) {
            if (set1.base_.isCoverEqual(set2.base_))
                base_ = set1.base_;
            else {
                std::ostringstream os;
                os << "Error: SymbolicSet::SymbolicSet(const SymbolicSet& set1, const SymbolicSet& set2): set1 and set2 do not have the same dd manager.";
                throw std::runtime_error(os.str().c_str());
            }
            dim_ = set1.dim_ + set2.dim_;
            z_ = std::vector<double>(dim_);
            eta_ = std::vector<double>(dim_);
            lastGridPoint_ = std::vector<double>(dim_);
            firstGridPoint_ = std::vector<double>(dim_);
            nofGridPoints_ = std::vector<size_t>(dim_);
            nofBddVars_ = std::vector<size_t>(dim_);
            indBddVars_ = std::vector<std::vector<size_t>>(dim_);
            symbolicSet_ = base_.zero();
            iterator_ = nullptr;
            for (size_t i = 0; i < set1.dim_; i++) {
                z_[i] = set1.z_[i];
                eta_[i] = set1.eta_[i];
                lastGridPoint_[i] = set1.lastGridPoint_[i];
                firstGridPoint_[i] = set1.firstGridPoint_[i];
                nofGridPoints_[i] = set1.nofGridPoints_[i];
                nofBddVars_[i] = set1.nofBddVars_[i];
            }
            for (size_t i = 0; i < set2.dim_; i++) {
                z_[set1.dim_ + i] = set1.z_[i];
                eta_[set1.dim_ + i] = set2.eta_[i];
                lastGridPoint_[set1.dim_ + i] = set2.lastGridPoint_[i];
                firstGridPoint_[set1.dim_ + i] = set2.firstGridPoint_[i];
                nofGridPoints_[set1.dim_ + i] = set2.nofGridPoints_[i];
                nofBddVars_[set1.dim_ + i] = set2.nofBddVars_[i];
            }
            for (size_t i = 0; i < set1.dim_; i++) {
                indBddVars_[i] = std::vector<size_t>(set1.nofBddVars_[i]);
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    indBddVars_[i][j] = set1.indBddVars_[i][j];
            }
            for (size_t i = 0; i < set2.dim_; i++) {
                indBddVars_[set1.dim_ + i] = std::vector<size_t>(set2.nofBddVars_[i]);
                for (size_t j = 0; j < nofBddVars_[set1.dim_ + i]; j++)
                    indBddVars_[set1.dim_ + i][j] = set2.indBddVars_[i][j];
            }
            /* number of total variables */
            nvars_ = 0;
            for (size_t i = 0; i < dim_; i++)
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    nvars_++;
        }
        /* function: copy assignment operator */
        SymbolicSet &operator=(const SymbolicSet &other) {
            base_ = other.base_;
            dim_ = other.dim_;
            nvars_ = other.nvars_;
            symbolicSet_ = other.symbolicSet_;
            iterator_ = nullptr;
            z_ = std::vector<double>(dim_);
            eta_ = std::vector<double>(dim_);
            lastGridPoint_ = std::vector<double>(dim_);
            firstGridPoint_ = std::vector<double>(dim_);
            nofGridPoints_ = std::vector<size_t>(dim_);
            nofBddVars_ = std::vector<size_t>(dim_);
            indBddVars_ = std::vector<std::vector<size_t>>(dim_);
            for (size_t i = 0; i < dim_; i++) {
                z_[i] = other.z_[i];
                eta_[i] = other.eta_[i];
                lastGridPoint_[i] = other.lastGridPoint_[i];
                firstGridPoint_[i] = other.firstGridPoint_[i];
                nofGridPoints_[i] = other.nofGridPoints_[i];
                nofBddVars_[i] = other.nofBddVars_[i];
            }
            for (size_t i = 0; i < dim_; i++) {
                indBddVars_[i] = std::vector<size_t>(other.nofBddVars_[i]);
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    indBddVars_[i][j] = other.indBddVars_[i][j];
            }
            return *this;
        }
        /* function: mintermToElement
         * compute the element of the symbolic set associated with the minterm */
        inline void mintermToElement(const std::vector<size_t> &minterm, std::vector<double> &element) const {
            for (size_t i = 0; i < dim_; i++) {
                size_t idx = 0;
                for (size_t c = 1, j = 0; j < nofBddVars_[i]; c *= 2, j++)
                    idx += minterm[indBddVars_[i][j]] * c;
                element[i] = eta_[i] * idx + firstGridPoint_[i];
            }
        }

        /**
         * @brief find the respective minterm for an element x
         * @details
         *  x -- x is an element of the domain
         */
        template <class T>
        UBDD elementToMinterm(T &x) {
            if (x.size() != dim_) {
                std::ostringstream os;
                os << "Error: SymbolicSet::elementToMinterm(x): x must be of size dim_.";
                throw std::invalid_argument(os.str().c_str());
            }
            /* check if x is inside domain */
            for (size_t i = 0; i < dim_; i++)
                if (x[i] > (lastGridPoint_[i] + eta_[i] / 2) || x[i] < (firstGridPoint_[i] - eta_[i] / 2)) {
                    std::ostringstream os;
                    os << "Error: SymbolicSet::elementToMinterm(x): x must be in the domain of the symbolic set.";
                    throw std::invalid_argument(os.str().c_str());
                }

            /* combute UBDD representation of x */
            UBDD cube = base_.one();
            for (size_t i = 0; i < dim_; i++) {
                std::vector<uint8_t> phase(nofBddVars_[i]);
                std::vector<UBDD> vars(nofBddVars_[i]);
                int idx = std::lround((x[i] - firstGridPoint_[i]) / eta_[i]);
                for (size_t j = 0; j < nofBddVars_[i]; j++) {
                    phase[j] = idx % 2;
                    idx /= 2;
                    vars[j] = base_.var(indBddVars_[i][j]);
                }
                cube &= base_.cube(vars, phase);
            }
            return cube;
        }

        /**
         * @brief get the dimension of the real space
         * @details
         * get the dimension of the real space in which the points that are represented by
         * the symbolic set live in
         */
        inline size_t getDimension() const {
            return dim_;
        }

        /**
         * @brief get the UBDD  that stores the SymbolicSet
         */
        inline UBDD getSymbolicSet() const {
            return symbolicSet_;
        }

        /**
         *  @brief returns a UBDD with ones in the domain of the SymbolicSet
         */
        inline UBDD getCube() const {
            /* create cube */
            std::vector<UBDD> vars(nvars_);
            for (size_t k = 0, i = 0; i < dim_; i++) {
                for (size_t j = 0; j < nofBddVars_[i]; k++, j++)
                    vars[k] = base_.var(indBddVars_[i][j]);
            }
            UBDD cube = base_.cube(vars);

            return cube;
        }

        /**
         * @brief set the symbolic set to symset
         */
        inline void setSymbolicSet(const UBDD &symset) {
            /*get variable ids not used in this set*/
            std::vector<size_t> coIndBddVars;
            for (size_t i = 0; i < (size_t)base_.nodeSize(); i++) {
                bool isInside = false;
                for (size_t d = 0; d < dim_; d++)
                    for (size_t j = 0; j < nofBddVars_[d]; j++)
                        if (i == indBddVars_[d][j])
                            isInside = true;

                if (!isInside)
                    coIndBddVars.push_back(i);
            }
            /* create cube to existentially abstract all bdd ids that are
             * not one of the indBddVar of this SymbolicSet
             */
            size_t t = coIndBddVars.size();
            std::vector<UBDD> vars(t);
            for (size_t k = 0; k < t; k++)
                vars[k] = base_.var(coIndBddVars[k]);
            UBDD cube = base_.cube(vars);

            symbolicSet_ = symset.existAbstract(cube);
        }

        /**
         * @brief get the pointer to the size_t array that contains the number of bdd variables
         */
        inline const std::vector<size_t> &getNofBddVars() const {
            return nofBddVars_;
        }

        /**
         * @brief get the total number of UBDD variables
         */
        inline int getNVars() const {
            return nvars_;
        }

        /**
         * @brief get the pointer to the size_t array of indices of the bdd variables
         */
        inline const std::vector<std::vector<size_t>> &getIndBddVars() const {
            return indBddVars_;
        }

        /**
         * @brief set the indices of the bdd variables to the indices provided in size_t* newIndBddVars
         * @details NOTE: the size of pointer newIndBddVars needs to be dim_*nofBddVars_!
         */
        void setIndBddVars(const std::vector<size_t> &newIndBddVars) {
            for (size_t i = 0, k = 0; i < dim_; i++)
                for (size_t j = 0; j < nofBddVars_[i]; k++, j++)
                    indBddVars_[i][j] = newIndBddVars[k];
        }
        /* function:  getSize
         * get the number of grid points in the symbolic set */
        inline double getSize() {
            return symbolicSet_.countMinterm(nvars_);
        }

        /**
         * @brief project the SymbolicSet on the dimensions specified in projectDimension and then count the number of elements
         */
        inline double getSize(const std::vector<size_t> &projectDimension) {
            size_t n = projectDimension.size();
            if (n > dim_) {
                std::ostringstream os;
                os << "Error: SymbolicSet::getSize(projectDimension): number of dimensions onto which the SymbolicSet should be projected exceeds dimension.";
                throw std::invalid_argument(os.str().c_str());
            }
            for (size_t i = 0; i < n; i++) {
                if (projectDimension[i] >= dim_) {
                    std::ostringstream os;
                    os << "Error: SymbolicSet::getSize(projectDimension): dimensions onto which the SymbolicSet should be projected exceeds dimension.";
                    throw std::invalid_argument(os.str().c_str());
                }
            }
            /* compute complement dim */
            std::vector<size_t> codim;
            for (size_t i = 0; i < dim_; i++) {
                if (!(std::find(projectDimension.begin(), projectDimension.end(), i) != projectDimension.end()))
                    codim.push_back(i);
            }
            /* create cube to be used in the existAbstract command */
            size_t t = 0;
            for (unsigned long i : codim)
                t += nofBddVars_[i];

            std::vector<UBDD> vars(t);
            for (size_t k = 0, i = 0; i < codim.size(); i++) {
                for (size_t j = 0; j < nofBddVars_[codim[i]]; k++, j++)
                    vars[k] = base_.var(indBddVars_[codim[i]][j]);
            }
            UBDD cube = base_.cube(vars);
            UBDD bdd = symbolicSet_.existAbstract(cube);

            return bdd.countMinterm(nvars_ - t);
        }

        /**
         * @brief empty the symbolic set
         */
        inline void clear() {
            symbolicSet_ = base_.zero();
        }
        /* iterator methods */

        /**
         * @brief iterator methods: initilize the iterator and compute the first element
         */
        inline void begin() {
            std::vector<size_t> ivars_;
            ivars_.reserve(nvars_);
            for (size_t i = 0; i < dim_; i++)
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    ivars_.push_back(indBddVars_[i][j]);
            /* set up iterator */
            iterator_ = symbolicSet_.generateMintermIterator(ivars_);
        }

        /**
         * @brief iterator methods: compute the next element
         */
        inline void next() {
            ++(*iterator_);
        }

        /**
         * @brief iterator methods: print progess of iteratin in percent
         */
        inline void progress() {
            iterator_->printProgress();
        }

        /**
         * @brief iterator methods: changes to one if iterator reached the last element
         */
        inline int done() {
            if (iterator_->done()) {
                delete iterator_;
                iterator_ = nullptr;
                return 1;
            } else
                return 0;
        }

        /**
         * @brief iterator methods: returns the pointer to the current minterm in the iteration
         */
        inline const std::vector<size_t> &currentMinterm() {
            if (iterator_) {
                return iterator_->currentMinterm();
            } else {
                static const std::vector<size_t> empty;
                return empty;
            }
        }

        /**
         * @brief add all grid points of the uniform grid to the set
         * @details
         * initially the symbolic set is empty
         * with this command all grid points of the uniform grid are added to the set */
        void addGridPoints() {
            symbolicSet_ |= SymbolicSet::computeGridPoints();
        }
        /**
         * @brief a grid point x is added to the SymbolicSet if(function(x)) is true
         */
        template <class F>
        void addByFunction(F &function) {
            /* set up grid to iterate over */
            UBDD grid = SymbolicSet::computeGridPoints();
            /* set to be added */
            UBDD set = base_.zero();

            /* idx of all variables */
            std::vector<size_t> ivars_;
            ivars_.reserve(nvars_);
            std::vector<UBDD> ubddVars(nvars_);
            size_t k = 0;
            for (size_t i = 0; i < dim_; i++) {
                for (size_t j = 0; j < nofBddVars_[i]; j++) {
                    ivars_.push_back(indBddVars_[i][j]);
                    ubddVars[k++] = base_.var(indBddVars_[i][j]);
                }
            }

            /* set up iterator */
            genie::UBDDMintermIterator *it = grid.generateMintermIterator(ivars_);
            /* current grid point in iteration */
            std::vector<double> x = std::vector<double>(dim_);
            /* get pointer to minterm */
            const std::vector<size_t> minterm = it->currentMinterm();
            std::vector<size_t> cube(minterm);

            /* loop over all elements */
            while (!it->done()) {
                /* convert current minterm to element (=grid point) */
                mintermToElement(minterm, x);
                /* cube is inside set */
                if (function(x))
                    /* this is a bottelneck! */
                    set |= base_.cube(ubddVars, cube);
                it->operator++();
                ++k;
            }

            symbolicSet_ |= set;
        }

        /**
         * @brief add grid points that are in polytope
         * @details
         * P = { x | H x <= h }
         *
         * @param p - number of halfspaces
         * @param H - is a double vector of size dim*p which contains the normal vectors n1...np of the half
         *     spaces. H = { n1, n2, n3 ... np }
         * @param h - double vector of size p
         * @param type - approximation type is either INNER or OUTER:
         *        - INNER -> grid points whose cells are completely contained in P are *added* to the symolic set
         *        - OUTER -> grid points whose cells overlap with P are *added* to the symolic set
         */
        void addPolytope(const size_t p, const std::vector<double> &H, const std::vector<double> &h, const ApproximationType type) {
            UBDD polytope = SymbolicSet::computePolytope(p, H, h, type);
            symbolicSet_ |= polytope;
        }
        /**
         * @brief remove grid points that are in polytope
         * @details
         * P = { x | H x <= h }
         *
         * @param p - number of halfspaces
         * @param H - is a double vector of size dim*p which contains the normal vectors n1...np of the half
         *     spaces. H = { n1, n2, n3 ... np }
         * @param h - double vector of size p
         * @param type - approximation type is either INNER or OUTER:
         *        - INNER -> grid points whose cells are completely contained in P are *removed* from the symolic set
         *        - OUTER -> grid points whose cells overlap with P are *removed* from the symolic set
         */
        void remPolytope(const size_t p, const std::vector<double> &H, const std::vector<double> &h, const ApproximationType type) {
            UBDD polytope = SymbolicSet::computePolytope(p, H, h, type);
            symbolicSet_ &= (!polytope);
        }

        /**
         * @brief add grid points that are in ellipsoid
         * @details
         * E = { x |  (x-c)' L' L (x-c) <= 1 }
         *
         * @param L - is a double vector of size dim*dim  so that L'*L is positive definite
         * @param c - double vector of size dim containing the center of the ellispoid
         * @param type - approximation type is either INNER or OUTER:
         *        - INNER -> grid points whose cells are completely contained in E are *added* to the symolic set
         *        - OUTER -> grid points whose cells overlap with E are *added* to the symolic set
         */
        void addEllipsoid(const std::vector<double> &L, const std::vector<double> &c, const ApproximationType type, double ellipsoidRadius) {
            UBDD ellisoid = SymbolicSet::computeEllipsoid(L, c, type, ellipsoidRadius);
            symbolicSet_ |= ellisoid;
        }

        /** function: remEllipsoid
         *
         * @brief remove grid points that are in ellipsoid
         * @details E = { x |  (x-c)' L' L (x-c) <= 1 }
         *
         * @param L - is a double vector of size dim*dim  so that L'*L is positive definite
         * @param c - double vector of size dim containing the center of the ellispoid
         * @param type - approximation type is either INNER or OUTER:
         *        - INNER -> grid points whose cells are completely contained in E are *removed* from the symolic set
         *        - OUTER -> grid points whose cells overlap with E are *removed* from the symolic set
         */
        void remEllipsoid(const std::vector<double> &L, const std::vector<double> &c, const ApproximationType type, double ellipsoidRadius) {
            UBDD ellisoid = SymbolicSet::computeEllipsoid(L, c, type, ellipsoidRadius);
            symbolicSet_ &= (!ellisoid);
        }

        /**
         * @brief print some numbers related to the symbolic set
         */
        void printInfo(int verbosity = 0) const {
            size_t totNoBddVars = 0;
            size_t totNoGridPoints = 1;
            for (size_t i = 0; i < dim_; i++) {
                totNoBddVars += nofBddVars_[i];
                totNoGridPoints *= nofGridPoints_[i];
            }

            std::cout << "First grid point: ";
            for (size_t i = 0; i < dim_; i++)
                std::cout << firstGridPoint_[i] << " ";
            std::cout << std::endl;
            if (verbosity) {
                std::cout << "Last grid point: ";
                for (size_t i = 0; i < dim_; i++)
                    std::cout << lastGridPoint_[i] << " ";
                std::cout << std::endl;
            }


            std::cout << "Grid node distance (eta)in each dimension: ";
            for (size_t i = 0; i < dim_; i++)
                std::cout << eta_[i] << " ";
            std::cout << std::endl;

            std::cout << "Number of grid points in each dimension: ";
            for (size_t i = 0; i < dim_; i++)
                std::cout << nofGridPoints_[i] << " ";
            std::cout << std::endl;


            std::cout << "Number of grid points in domain: " << totNoGridPoints << std::endl;
            if (symbolicSet_ != base_.zero())
                std::cout << "Number of elements in the symbolic set: " << symbolicSet_.countMinterm(totNoBddVars) << std::endl;
            else {
                std::cout << "Symbolic set does not contain any grid points." << std::endl;
            }

            if (verbosity) {
                std::cout << "Number of binary variables: " << totNoBddVars << std::endl;
                for (size_t i = 0; i < dim_; i++) {
                    std::cout << "Bdd variable indices (=IDs) in dim " << i + 1 << ": ";
                    for (size_t j = 0; j < nofBddVars_[i]; j++)
                        std::cout << indBddVars_[i][j] << " ";
                    std::cout << std::endl;
                }
            }
            if (verbosity > 1)
                symbolicSet_.printMinterm();
        }
        /**
         * @brief store the bdd with all information to file
         */
        void writeToFile(const char *filename) {
            /* produce string with parameters to be written into file */
            std::stringstream dim;
            std::stringstream z;
            std::stringstream eta;
            std::stringstream first;
            std::stringstream last;
            std::stringstream nofGridPoints;
            std::stringstream indBddVars;

            dim << "#mascot dimension: " << dim_ << std::endl;
            z << "#mascot measurement error bound: ";
            eta << "#mascot grid parameter eta: ";
            first << "#mascot coordinate of first grid point: ";
            last << "#mascot coordinate of last grid point: ";
            nofGridPoints << "#mascot number of grid points (per dim): ";
            indBddVars << "#mascot index of bdd variables: ";

            for (size_t i = 0; i < dim_; i++) {
                z << z_[i] << " ";
                eta << eta_[i] << " ";
                first << firstGridPoint_[i] << " ";
                last << lastGridPoint_[i] << " ";
                nofGridPoints << nofGridPoints_[i] << " ";
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    indBddVars << indBddVars_[i][j] << " ";
            }
            z << std::endl;
            eta << std::endl;
            first << std::endl;
            last << std::endl;
            nofGridPoints << std::endl;
            indBddVars << std::endl;

            std::stringstream paramss;
            paramss << "################################################################################" << std::endl
                    << "########### symbolic controller synthesis toolbox information added ############" << std::endl
                    << "################################################################################" << std::endl
                    << dim.str()
                    << eta.str()
                    << z.str()
                    << first.str()
                    << last.str()
                    << nofGridPoints.str()
                    << indBddVars.str();
            std::string param = paramss.str();

            /* the meta-information and the bdd need to be stored separately for compatibility with sylvan */
            std::string fname_meta(filename);
            fname_meta += ".meta";
            FILE *file = fopen(fname_meta.c_str(), "w");
            if (file == nullptr) {
                std::ostringstream os;
                os << "Error: Unable to open file for writing." << filename << ".meta.";
                throw std::runtime_error(os.str().c_str());
            }
            if (param.size() != fwrite(param.c_str(), 1, param.size(), file)) {
                throw std::runtime_error("Error: Unable to write set information to file.");
            }
            fclose(file);

            file = fopen(filename, "w");
            /* before we save the BDD to file, we save it to another manager,
             * because the current manager is classified as ADD manager */
            int storeReturnValue = symbolicSet_.save(file);
            fclose(file);

            if (!storeReturnValue)
                throw std::runtime_error("Error: Unable to write BDD to file.");
            else
                std::cout << "Symbolic set saved to file: " << filename << std::endl;
        }

        /**
         * @brief return constant pointer to the double array z_
         */
        inline const std::vector<double> &getZ() const {
            return z_;
        }

        /**
         * @brief return constant pointer to the double array eta_
         */
        inline const std::vector<double> &getEta() const {
            return eta_;
        }

        /**
         * @brief compute the of the symbolic set with respect to the uniform grid
         */
        void complement() {
            symbolicSet_ = base_.complement(symbolicSet_, nofGridPoints_, nofBddVars_, indBddVars_, dim_);
        }

        /**
         * @brief the first grid point is saved to a double array of size dim_
         */
        inline const std::vector<double> &getFirstGridPoint() const {
            return firstGridPoint_;
        }

        /**
         * @brief the last grid point is saved to a double array of size dim_
         */
        inline const std::vector<double> &getLastGridPoint() const {
            return lastGridPoint_;
        }

        /**
         * @brief return pointer to size_t array containing the number of grid points
         */
        inline const std::vector<size_t> &getNofGridPoints() const {
            return nofGridPoints_;
        }

        /**
         * @brief z_ is written to the double array z of size dim_
         */
        inline void copyZ(double z[]) const {
            for (size_t i = 0; i < dim_; i++)
                z[i] = z_[i];
        }

        /**
         * @brief eta_ is written to the double array eta of size dim_
         */
        inline void copyEta(double eta[]) const {
            for (size_t i = 0; i < dim_; i++)
                eta[i] = eta_[i];
        }

        /**
         * @brief the first grid point is saved to a double array of size dim_
         */
        inline void copyFirstGridPoint(double firstGridPoint[]) const {
            for (size_t i = 0; i < dim_; i++)
                firstGridPoint[i] = firstGridPoint_[i];
        }

        /**
         * @brief the last grid point is saved to a double array of size dim_
         */
        inline void copyLastGridPoint(double lastGridPoint[]) const {
            for (size_t i = 0; i < dim_; i++)
                lastGridPoint[i] = lastGridPoint_[i];
        }
        /**
         * @brief copy the size_t array containing the number of grid points to nofGridPoints
         */
        inline void getNofGridPoints(size_t nofGridPoints[]) const {
            for (size_t i = 0; i < dim_; i++)
                nofGridPoints[i] = nofGridPoints_[i];
        }

        /**
         * @brief the grid points in the symbolic set are saved to the double array of size (dim_) x (setSize)
         */
        void copyGridPoints(double gridPoints[]) const {
            std::vector<size_t> ivars_;
            ivars_.reserve(nvars_);
            for (size_t i = 0; i < dim_; i++)
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    ivars_.push_back(indBddVars_[i][j]);
            /* number of minterms in the symbolic set */
            size_t numberOfMinterms = symbolicSet_.countMinterm(nvars_);
            /* set up iterator */
            genie::UBDDMintermIterator *it = symbolicSet_.generateMintermIterator(ivars_);
            /* get pointer to minterm */
            std::vector<size_t> minterm = it->currentMinterm();
            size_t k = 0;
            while (!it->done()) {
                //it.printMinterm();
                //it.printProgress();
                minterm = it->currentMinterm();
                for (size_t i = 0; i < dim_; i++) {
                    int c = 1;
                    double num = 0;
                    for (size_t j = 0; j < nofBddVars_[i]; j++) {
                        num += c * (double)minterm[indBddVars_[i][j]];
                        c *= 2;
                    }
                    gridPoints[i * numberOfMinterms + k] = firstGridPoint_[i] + num * eta_[i];
                }
                it->operator++();
                ++k;
            }
        }

        /**
         * @brief the SymbolicSet is projected onto the dimensions specified in  projectDimension
         * @details
         * the grid points in the resultilng set are saved to the double array of size (dim_) x (setSize)
         */
        void copyGridPoints(double gridPoints[], const std::vector<size_t> &projectDimension) const {
            /* check input data */
            size_t n = projectDimension.size();
            if (n >= dim_) {
                std::ostringstream os;
                os << "Error: SymbolicSet::getSize(projectDimension): number of dimensions onto which the SymbolicSet should be projected exceeds dimension.";
                throw std::invalid_argument(os.str().c_str());
            }
            for (size_t i = 0; i < n; i++) {
                if (projectDimension[i] >= dim_) {
                    std::ostringstream os;
                    os << "Error: SymbolicSet::getSize(projectDimension): dimensions onto which the SymbolicSet should be projected exceeds dimension.";
                    throw std::invalid_argument(os.str().c_str());
                }
            }
            /* compute complement dim */
            std::vector<size_t> codim;
            for (size_t i = 0; i < dim_; i++) {
                if (!(std::find(projectDimension.begin(), projectDimension.end(), i) != projectDimension.end()))
                    codim.push_back(i);
            }
            /* create cube to be used in the existAbstract command */
            size_t t = 0;
            for (size_t i = 0; i < codim.size(); i++)
                t += nofBddVars_[codim[i]];
            std::vector<UBDD> vars(t);
            for (size_t k = 0, i = 0; i < codim.size(); i++) {
                for (size_t j = 0; j < nofBddVars_[codim[i]]; k++, j++)
                    vars[k] = base_.var(indBddVars_[codim[i]][j]);
            }
            UBDD cube = base_.cube(vars);
            /* projected set */
            UBDD bdd = symbolicSet_.existAbstract(cube);
            /* number of variables */
            size_t pnvars = nvars_ - t;
            std::vector<size_t> ivars;
            ivars.reserve(pnvars);
            for (size_t i = 0; i < n; i++)
                for (size_t j = 0; j < nofBddVars_[projectDimension[i]]; j++)
                    ivars.push_back(indBddVars_[projectDimension[i]][j]);
            /* number of minterms in the symbolic set */
            size_t numberOfMinterms = bdd.countMinterm(pnvars);
            /* set up iterator */
            genie::UBDDMintermIterator *it = bdd.generateMintermIterator(ivars);
            /* get pointer to minterm */

            size_t k = 0;
            while (!it->done()) {
                //it.printMinterm();
                //it.printProgress();
                const std::vector<size_t> minterm = it->currentMinterm();
                for (size_t i = 0; i < n; i++) {
                    int c = 1;
                    double num = 0;
                    for (size_t j = 0; j < nofBddVars_[projectDimension[i]]; j++) {
                        num += c * (double)minterm[indBddVars_[projectDimension[i]][j]];
                        c *= 2;
                    }
                    gridPoints[i * numberOfMinterms + k] = firstGridPoint_[projectDimension[i]] + num * eta_[projectDimension[i]];
                }
                //for(size_t i=0; i<dim_; i++)
                //  std::cout << gridPoints[i*numberOfMinterms+k] << " " ;
                //std::cout << std::endl;
                it->operator++();
                ++k;
            }
        }

        /**
         * @brief check if an element x is an element of the SymolicSet
         * @details
         *  x -- x is an element of the domain
         */
        bool isElement(const std::vector<double> &x) {
            if (x.size() != dim_) {
                std::ostringstream os;
                os << "Error: SymbolicSet::isElement(x): x must be of size dim_.";
                throw std::invalid_argument(os.str().c_str());
            }
            /* check if x is inside domain */
            for (size_t i = 0; i < dim_; i++)
                if (x[i] > (lastGridPoint_[i] + eta_[i] / 2) || x[i] < (firstGridPoint_[i] - eta_[i] / 2))
                    return false;

            /* combute UBDD representation of x */
            UBDD cube = base_.one();
            for (size_t i = 0; i < dim_; i++) {
                std::vector<uint8_t> phase(nofBddVars_[i]);
                std::vector<UBDD> vars(nofBddVars_[i]);
                int idx = std::lround((x[i] - firstGridPoint_[i]) / eta_[i]);
                for (size_t j = 0; j < nofBddVars_[i]; j++) {
                    phase[j] = idx % 2;
                    idx /= 2;
                    vars[j] = base_.var(indBddVars_[i][j]);
                }
                cube &= base_.cube(vars, phase);
            }
            /* check if cube is element of symbolicSet_ */
            if ((cube & symbolicSet_) != base_.zero())
                return true;
            else
                return false;
        }

        /**
         * @brief we interpret the SymbolicSet as relation R and identify the relation with a
         * @details set valued map:
         *
         * ind -- contains the  dimensions that span the domain of the set valued map
         * the ramaining indices of [0 .. dim_-1] make up the codimension;
         *
         *  x -- x is an element of the domain
         *
         *  if x is not an element the domain of the set valued map, an empty vector is returned
         *
         */
        std::vector<std::vector<double>> setValuedMap(const std::vector<double> &x, const std::vector<size_t> &ind) {
            if (x.size() != ind.size()) {
                std::ostringstream os;
                os << "Error: SymbolicSet::setValuedMap(x,ind): x and ind must be of the same length.";
                throw std::invalid_argument(os.str().c_str());
            }
            if (ind.size() >= dim_) {
                std::ostringstream os;
                os << "Error: SymbolicSet::setValuedMap(x,ind): size of ind cannot exceed dim_.";
                throw std::invalid_argument(os.str().c_str());
            }
            size_t n = x.size();
            for (size_t i = 0; i < n; i++) {
                if (ind[i] >= dim_) {
                    std::cout << ind[i] << std::endl;
                    std::ostringstream os;
                    os << "Error: SymbolicSet::setValuedMap(x,ind): ind[i] exceeds the number of the grid dimension.";
                    throw std::invalid_argument(os.str().c_str());
                }
            }
            /* is x outside domian of SymbolicSet ? */
            for (size_t i = 0; i < n; i++) {
                size_t j = ind[i];
                if (x[j] > (lastGridPoint_[j] + eta_[j] / 2) || x[j] < (firstGridPoint_[j] - eta_[j] / 2)) {
                    std::vector<std::vector<double>> image;
                    return image;
                }
            }
            /* compute co-domain indices */
            std::vector<size_t> coind;
            for (size_t i = 0; i < dim_; i++) {
                if (!(std::find(ind.begin(), ind.end(), i) != ind.end()))
                    coind.push_back(i);
            }

            /* create cube that represents x */
            UBDD cube = base_.one();
            for (size_t i = 0; i < n; i++) {
                std::vector<int> phase(nofBddVars_[ind[i]]);
                std::vector<UBDD> vars(nofBddVars_[ind[i]]);
                int idx = std::lround((x[i] - firstGridPoint_[ind[i]]) / eta_[ind[i]]);
                for (size_t j = 0; j < nofBddVars_[ind[i]]; j++) {
                    phase[j] = idx % 2;
                    idx /= 2;
                    vars[j] = base_.var(indBddVars_[ind[i]][j]);
                }
                cube &= base_.cube(vars, phase);
            }
            /* extract the elements from symbolicSet_ associated with proj */
            UBDD proj = cube & symbolicSet_;
            /* setup return matrix */
            std::vector<std::vector<double>> image;
            /* variables are used to iterate over all elements in proj */
            std::vector<size_t> ivars;
            for (size_t i = 0; i < dim_; i++) {
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    ivars.push_back(indBddVars_[i][j]);
            }
            std::vector<double> element = std::vector<double>(dim_);
            genie::UBDDMintermIterator *it = proj.generateMintermIterator(ivars);
            for (; !it->done(); it->operator++()) {
                mintermToElement(it->currentMinterm(), element);
                std::vector<double> v(coind.size());
                for (size_t i = 0; i < coind.size(); i++)
                    v[i] = element[coind[i]];
                image.push_back(v);
            }
            return image;
        }

        void readMembersFromFile(const char *filename) {
            /* open file */
            std::ifstream bddfile(filename);
            if (!bddfile.good()) {
                std::ostringstream os;
                os << "Error: Unable to open file:" << filename << "'.";
                throw std::runtime_error(os.str().c_str());
            }
            /* read dimension from file */
            std::string line;
            while (!bddfile.eof()) {
                std::getline(bddfile, line);
                if (line.substr(0, 7) == "#mascot") {
                    if (line.find("dimension") != std::string::npos) {
                        std::istringstream sline(line.substr(line.find(":") + 1));
                        sline >> dim_;
                    }
                }
            }
            if (dim_ == 0) {
                std::ostringstream os;
                os << "Error: Could not read dimension from file: " << filename << ". ";
                os << "Was " << filename << " created with SymbolicSet::writeToFile?";
                throw std::runtime_error(os.str().c_str());
            }
            z_ = std::vector<double>(dim_);
            eta_ = std::vector<double>(dim_);
            lastGridPoint_ = std::vector<double>(dim_);
            firstGridPoint_ = std::vector<double>(dim_);
            nofGridPoints_ = std::vector<size_t>(dim_);
            nofBddVars_ = std::vector<size_t>(dim_);
            /* read eta/first/last/no of grid points/no of bdd vars */
            bddfile.clear();
            bddfile.seekg(0, std::ios::beg);
            int check = 0;
            while (!bddfile.eof()) {
                std::getline(bddfile, line);
                if (line.substr(0, 7) == "#mascot") {
                    /* read eta */
                    if (line.find("eta") != std::string::npos) {
                        check++;
                        std::istringstream sline(line.substr(line.find(":") + 1));
                        for (size_t i = 0; i < dim_; i++)
                            sline >> eta_[i];
                    }
                    /* read z */
                    if (line.find("measurement") != std::string::npos) {
                        check++;
                        std::istringstream sline(line.substr(line.find(":") + 1));
                        for (size_t i = 0; i < dim_; i++)
                            sline >> z_[i];
                    }
                    /* read first grid point*/
                    if (line.find("first") != std::string::npos) {
                        check++;
                        std::istringstream sline(line.substr(line.find(":") + 1));
                        for (size_t i = 0; i < dim_; i++)
                            sline >> firstGridPoint_[i];
                    }
                    /* read last grid point*/
                    if (line.find("last") != std::string::npos) {
                        check++;
                        std::istringstream sline(line.substr(line.find(":") + 1));
                        for (size_t i = 0; i < dim_; i++)
                            sline >> lastGridPoint_[i];
                    }
                    /* read no of grid points */
                    if (line.find("number") != std::string::npos) {
                        check++;
                        std::istringstream sline(line.substr(line.find(":") + 1));
                        for (size_t i = 0; i < dim_; i++) {
                            sline >> nofGridPoints_[i];
                            if (nofGridPoints_[i] == 1)
                                nofBddVars_[i] = 1;
                            else
                                nofBddVars_[i] = (size_t)std::ceil(std::log2(nofGridPoints_[i]));
                        }
                    }
                }
                if (check == 5)
                    break;
            }
            if (check < 5) {
                std::ostringstream os;
                os << "Error: Could not read all parameters from file: " << filename << ". ";
                os << "Was " << filename << " created with SymbolicSet::writeToFile?";
                throw std::runtime_error(os.str().c_str());
            }
            /* read index of bdd vars  */
            indBddVars_ = std::vector<std::vector<size_t>>(dim_);
            bddfile.clear();
            bddfile.seekg(0, std::ios::beg);
            check = 0;
            while (!bddfile.eof()) {
                std::getline(bddfile, line);
                if (line.substr(0, 7) == "#mascot") {
                    if (line.find("index") != std::string::npos) {
                        check++;
                        std::istringstream sline(line.substr(line.find(":") + 1));
                        for (size_t i = 0; i < dim_; i++) {
                            indBddVars_[i] = std::vector<size_t>(nofBddVars_[i]);
                            for (size_t j = 0; j < nofBddVars_[i]; j++)
                                sline >> indBddVars_[i][j];
                        }
                    }
                }
            }
            if (check != 1) {
                std::ostringstream os;
                os << "Error: Could not read bdd indices from file: " << filename << ".";
                os << "Was " << filename << " created with SymbolicSet::writeToFile?";
                throw std::runtime_error(os.str().c_str());
            } /* close file */
            bddfile.close();
            /* number of total variables */
            nvars_ = 0;
            for (size_t i = 0; i < dim_; i++)
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    nvars_++;

        } /* end readMembersFromFile */

        /**
         * @brief compute a bdd containing all the grid points
         */
        UBDD computeGridPoints() {
            UBDD grid = base_.one();
            std::vector<std::vector<UBDD>> ubddVars(dim_);
            std::vector<UBDD> symset(dim_);
            for (size_t i = 0; i < dim_; i++) {
                symset[i] = base_.zero();
                for (size_t j = 0; j < nofBddVars_[i]; j++)
                    ubddVars[i].push_back(base_.var(indBddVars_[i][j]));
            }
            for (size_t i = 0; i < dim_; i++) {
                std::vector<int> phase(nofBddVars_[i], 0);
                for (int j = 0; j < nofGridPoints_[i]; j++) {
                    int x = j;
                    for (size_t k = 0; x; x /= 2, k++) phase[k] = x % 2;
                    symset[i] += base_.cube(ubddVars[i], phase);
                }
                grid &= symset[i];
            }

            return grid;
        }

        /**
         * @brief compute symbolic representation (=bdd) of a polytope { x | Hx<= h }
         */
        UBDD computePolytope(const size_t p, const std::vector<double> &H, const std::vector<double> &h, const ApproximationType type) {
            return base_.computePolytope(p, H, h, (type == INNER) ? 1 : 0,
                                         dim_, eta_, z_, firstGridPoint_,
                                         nofBddVars_, indBddVars_, nofGridPoints_);
        }

        /**
         * @brief compute symbolic representation (=bdd) of an ellisoid { x | (x-center)' L' L (x-center) <= 1 }
         */
        UBDD computeEllipsoid(const std::vector<double> &L, const std::vector<double> &center, const ApproximationType type, double ellipsoidRadius) {

            /* now threshold against the grid points are checked  |Lx+c|<= t */
            double t = 1;
            if (type == INNER)
                t = 1 - ellipsoidRadius;
            if (type == OUTER)
                t = 1 + ellipsoidRadius;
            if (t <= 0)
                return base_.zero();

            UBDD grid = SymbolicSet::computeGridPoints();

            UBDD ellipsoid = base_.zero();
            std::vector<double> x = std::vector<double>(dim_);
            std::vector<double> y = std::vector<double>(dim_);
            double val;
            /* idx of all variables */
            std::vector<size_t> ivars_;
            ivars_.reserve(nvars_);
            std::vector<UBDD> bddVars(nvars_);
            size_t k = 0;
            for (size_t i = 0; i < dim_; i++) {
                for (size_t j = 0; j < nofBddVars_[i]; j++) {
                    ivars_.push_back(indBddVars_[i][j]);
                    bddVars[k++] = base_.var(indBddVars_[i][j]);
                }
            }
            /* set up iterator */
            genie::UBDDMintermIterator *it = grid.generateMintermIterator(ivars_);
            /* get pointer to minterm */
            std::vector<int> cube(nvars_);
            UBDD bddcube;
            k = 0;
            while (!it->done()) {
                //it.printMinterm();
                //it.printProgress();
                const std::vector<size_t> minterm = it->currentMinterm();

                for (size_t l = 0, i = 0; i < dim_; i++) {
                    int c = 1;
                    double num = 0;
                    for (size_t j = 0; j < nofBddVars_[i]; j++) {
                        cube[l] = minterm[indBddVars_[i][j]];
                        num += c * (double)cube[l];
                        c *= 2;
                        l++;
                    }
                    x[i] = firstGridPoint_[i] + num * eta_[i] - center[i];
                    y[i] = 0;
                }
                for (size_t i = 0; i < dim_; i++)
                    for (size_t j = 0; j < dim_; j++)
                        y[i] += L[dim_ * i + j] * x[j];
                val = 0;
                for (size_t i = 0; i < dim_; i++)
                    val += y[i] * y[i];
                /* cube is inside set */
                if (val <= (t * t)) {
                    /* this is a bottelneck! */
                    bddcube = base_.cube(bddVars, cube);
                    ellipsoid += bddcube;
                }
                it->operator++();
                ++k;
            }

            return ellipsoid;
        }
    }; /* close class def */
}// namespace mascot
#endif /* SYMBOLICSET_HH_ */
