# A Description of the Variables in the Input Configuration File

## System model

The following variables are used to represent the dynamic model of the system.

| Syntax  | Type  | Description |
| :---    | :---  | :---      |
| `sys_name`  | string  | the function providing the system dynamics (defined in `system_file_name` as described later)          |
| `sDIM`      | positive integer  | state-space dimension|
| `iDIM`      | positive integer  | input-space dimension|
| `dDIM`      | positive integer  | environmental input dimension (adverserial, separate from the additive stochastic noise in the dynamics) |
| `sLB`, `sUB`  | vectors of length `sDIM` | lower and upper bounds of the state-space |
| `iLB`, `iUB`  | vectors of length `iDIM` | lower and upper bounds of the input-space |
| `dLB`, `dUB`  | vectors of length `dDIM` | lower and upper bounds of the environmental inputs |
| `W_ub`  | vector of length `sDIM` | bounds on the absolute values of the support of the additive stochastic noise in the dynamics |

## Abstraction parameters

The following variables are used to represent the abstraction parameters.


| Syntax  | Type  | Description |
| :---    | :---  | :---      |
| `sETA`| vector of length `sDIM`| length of grid cells for the state space |
| `iETA`| vector of length `iDIM`| length of grid cells for the input space |
| `dETA`| vector of length `dDIM`| length of grid cells for the environmental input space |

## Hyperparameters for the synthesis algorithm

The following variables are used to represent the parameters for the game-based synthesis algorithm.


| Syntax  | Type  | Description |
| :---    | :---  | :---      |
| `accl_on` | boolean (`true`/`false`), default = `true` | status of fixpoint acceleration; on for `true`, off for `false` |
| `accl_mem_bound` | positive integer, default = 25 | hyperparameter for the fixpoint acceleration algorithm (the maximum number of past fixpoint iterations to be stored) |
| `abs_RFF` | boolean (`true`/`false`), default = `false` | read abstraction from file if one is already available |
| `over_and_under_approx` | boolean (`true`/`false`), default = `true` | compute both the over and the under-approximation of the controller domain |
| `warm_start_under_approx` | boolean (`true`/`false`), default = `true` | use the over-approximation of the controller domain to warm start the controller synthesis (effective only when `over_and_under_approx` is `true`) |
| `verbose` | integer (0-2), default = 0 | 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point|

## Paths

The following variables are used to represent the paths of different input/output files and dependencies.

| Syntax  | Type  | Description |
| :---    | :---  | :---      |
| `logfile` | string | the path of the output file where logs will be stored |
| `system_file_name` | string | the path of the file where the dynamic model of the system is stored[^1] |
| `owl_path` | string | the path to the `bin` folder of the owl installation |

[^1]: MascotSDS natively supports dynamic models of systems which are mixed-monotone (see [this paper](https://dl.acm.org/doi/pdf/10.1145/2728606.2728607?casa_token=rW5U6JIDrXcAAAAA:H2kOGQ1noo86ckB6KdJuhzsT9E7RQ3Y2ACH4F5cR6SPwI95c4SXvnvG2eCS1rUF3Mq6vzyf8GcMn6A)), whose decomposition function `sys_name` is given in `system_file_name`, a C-compatible
header file, so that `Synthesize` can link to (use) the decomposition function at runtime (see
the `mascotsds/examples/` directory for examples)

## Specification

The following variables are used to represent the control specification.

| Syntax  | Type  | Description |
| :---    | :---  | :---      |
| `spec_type` | string (either LTL or Rabin) | the format in which the specification is provided |
| `rabin_hoa_file_name` | string (required when `spec_type=Rabin`) | the file in which the Rabin automaton is stored (in the HOA format, see [here](http://adl.github.io/hoaf/)) | `predicate_to_polytope_mapping_begin`, `predicate_approx_type_end` | block representing a map between strings and lists of integers | maps each specification predicate to a set of state polytope id-s |
| `predicate_approx_type_begin`, `predicate_approx_type_end` | block assigning `OUTER` or `INNER` to some of the predicates (optional) | `OUTER`/`INNER` represents whether the particular predicate will be over or under-approximated during the state-space discretization: this would override the default approximation types of predicates deduced from the specification |
| `nof_hs_i`, `H_i`, `h_i` | `nof_hs_i` is a postive integer, and `H_i`, `h_i` are real-valued vectors of sizes `nof_hs_i x sDIM` and `nof_hs_i`, respectively | the half-space representation of the state space polytope, with id `i`, defined as `{s ∈ [sLB,sUB] : H_i*s ≤ h_i}`; the integer `nof_hs_i` represents the number of half spaces used to define the polytope |
| `LTL_BEGIN`, `LTL_END` | block with a string expression (required when `spec_type=LTL`) | the LTL specification (we use the syntax used by owl, see [here](https://gitlab.lrz.de/i7/owl/-/blob/main/doc/FORMATS.md))

## Required by `Simulate`

The following variables are used for a visual simulation of the closed-loop trajectory using the `Simulate` command after the controller synthesis.

| Syntax  | Type  | Description |
| :---    | :---  | :---      |
| `initial_state` | vector of length `sDIM` | the initial state |
| `simulation_horizon` | scalar | the simulation time horizon |
| `plot_state_space_file_name` | string | python script use for visualizing the state space |
| `controller_folder_name` | string | path to the folder where the controllers are stored (use `./data/controllers/` unless the controllers were moved after the synthesis) |
| `rabin_aut_folder_name` | string | path to the folder where the Rabin automaton is stored (use `./data/rabin/` unless the automaton was moved after the synthesis) |
