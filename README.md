# MascotSDS

Controller synthesis for stochastic dynamical system using finite abstraction
# Installation

## Genie
To use this project you must download the [Genie](https://gitlab.mpi-sws.org/mrychlicki/genie) repository
and then compile it according to the instructions. Then run:

```
mkdir build
cd build
cmake .. -Dgenie_HOME=/path/to/genie/ -DCMAKE_PREFIX_PATH=/path/to/libraries/
make
cd ..
```

Note: the paths should be specified as aboslute paths. The flag `-DCMAKE_PREFIX_PATH` is needed when the libraries (cudd, sylvan, hoaf) are installed in location other than `/usr/local/`.

Now this project is compiled and can be used.

## Others
For running some of the included examples, you have to install extra libraries:

### OWL
Download and unzip [Owl](https://github.com/owl-toolkit/owl/releases/tag/release-21.0).
You may have to change the path to Owl in cfg file.


### Boost C++
This can be installed by executing the following commands from the command line: for Mac OS use

    brew install boost

and for Linux use

    sudo apt-get install libboost-all-dev   

### Python3.9

    sudo apt-get install python3.9-dev

## How to Use

For ease of use, we have written a pair of tools called `Synthesize`
and `Simulate` using the library of MascotSDS.

`Synthesize` synthesizes controllers for stochastic dynamical systems whose nominal dynamics is mixed-monotone (see [this paper](https://dl.acm.org/doi/pdf/10.1145/2728606.2728607?casa_token=rW5U6JIDrXcAAAAA:H2kOGQ1noo86ckB6KdJuhzsT9E7RQ3Y2ACH4F5cR6SPwI95c4SXvnvG2eCS1rUF3Mq6vzyf8GcMn6A)). The inputs to Synthesize include the dynamic model of the system and the control specification; the latter can be specified either in LTL or using a Rabin automaton. To use `Synthesize`, simply use the following syntax:
```
<path-to-Synthesize binary>/Synthesize <path-to-input-file>/<input.cfg> <sylvan/cudd flag>
```
where `sylvan/cudd flag` is either 1 or 0 depending on whether the parallel
version using Sylvan is to be run or the sequential version using CUDD, and `input.cfg` is an input configuration file containing inputs as described [here](https://gitlab.mpi-sws.org/kmallik/mascotsds/-/blob/master/FORMATS.md?ref_type=heads) in detail.

The output of `Synthesize` is a folder called `data` that contains pieces of the
controller encoded in BDDs and stored in binary files as well as various metadata
information stored in text files. These files can be processed by `Simulate` to
visualize simulated closed-loop trajectories of the system. The usage of Simulate
is similar to Synthesize:
```
<path-to-Simulate binary>/Simulate <path-to-input-file>/<input.cfg> <sylvan/cudd flag>
```
where the `input.cfg` file should, in this case, contain information that are
required to simulate the closed-loop, like simulation time steps, the python script
that will plot the state space predicates (see the examples), etc; see [here](https://gitlab.mpi-sws.org/kmallik/mascotsds/-/blob/master/FORMATS.md?ref_type=heads) for details.


## Documantation

To create documentation run:

```
cd docs
export genie_HOME=/path/to/genie/
doxygen Doxyfile.in
```

and now documentation can be found in `docs/html/index.html`.

CAV '23 Artifact
==================
Our CAV '23 paper can be obtained from this
[link](https://link.springer.com/chapter/10.1007/978-3-031-37709-9_1),
while the artefact with virtual machine can be found at this
[link](https://zenodo.org/record/7877791#.ZExTJZFBxH4).

<!---
System requirements
===================

-   Operating system: Linux, Mac OS. We haven't tested the tool on
    Windows.

-   The CUDD library, which can be freely downloaded from
    <https://github.com/ivmai/cudd>. See
    the installation section below
    for more information on the configuration required during the
    installation of the library.

-   A working distribution of MATLAB for visualization, and a compatible
    version of GCC compiler for Linux or a compatible version of Xcode
    for Mac OS for MEX-file compilation. A list of compatible GCC/Xcode
    versions for different versions of MATLAB can be found here (see the
    "supported compilers" column):
    <https://www.mathworks.com/support/requirements/previous-releases.html?s_cid=pi_scl_1_R2012a_win64>.

Quick Installation
============
Just run this:

    ./install.sh

[Old] Installation
============

The installation procedure is the same as Mascot and SCOTS, and is given in the
following (based on the SCOTS manual):

1.  Unzip the provided `.zip` package of the tool, or clone the
    repository `mascot-sds` from the following url:
    <https://gitlab.mpi-sws.org/kmallik/mascot-sds.git>.
    (See here a step-by-step guide for cloning a repository:
    <https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html>.)

2.  Install a working C++ developer environment:

    -   Mac OS: Install the latest version of Xcode.app including the
        command line tools. We used the compiler clang 12.0.0.

    -   Linux: Most Linux OS distributions already include all the
        necessary tools already. We used the compiler GCC 9.3.0.

    -   Windows: has not been tested on a Windows machine.

Additionally, for running some of the included examples, you will need the boost c++ libraries. This can be installed by executing the following commands from the command line: for Mac OS use

    brew install boost

and for Linux use

    sudo apt-get install libboost-all-dev    

3.  Install the CUDD library with

    -   the C++ object oriented wrapper,

    -   the dddmp library, and

    -   the shared library

    option enabled. To do that, first download CUDD from the specified
    link, unzip at any convenient location, open the terminal, navigate
    to the path of the extracted CUDD folder, and execute the following
    commands one by one:

        ./configure --enable-shared --enable-obj --enable-dddmp --prefix=/opt/local/
        make
        make check
        make install

    The above configuration was used for cudd-3.0.0. The `prefix` flag
    specifies the location where the CUDD library will be placed. The
    location of the CUDD library will be specified using the variable
    called `CUDD_PATH` in this document and in the Makefiles at several
    locations. So if the exact same setting is used for the `prefix`
    flag as given above, then use `CUDD_PATH = /opt/local/` all the time.

    Some notes while setting up the CUDD library:

    -   We have provided a dummy program in the folder `./test_cudd/`
        called `test.cc` to check the CUDD installation. First edit the
        `Makefile` to adjust the variable `CUDD_PATH` (if required). Then
        compile and run the test program by executing the following
        commands one by one:

            make clean
            make
            ./test

        If no error is generated while executing `make` and `./test`,
        then the CUDD library was installed correctly.

    -   It has been reported that on some linux machines, the header
        files `util.h` and `config.h` were missing in `/opt/local`, and
        the fix is to manually copy the files to `/opt/local/include` by
        running from the CUDD unzipped location the following:

             sudo cp ./util/util.h /opt/local/include/
             sudo cp ./config.h /opt/local/include/

    -   You need to create a copy of `template_env.sh` and name it `env.sh`.
        Open `env.sh` and change the paths to first a few environment variables.
        Every time you open a terminal you need to run:

            source env.sh

        or this command can be added (with proper path) into the ~/.bashrc file so
        that the path is automatically loaded each time.

    -   In some Linux Machine, it was reported that setting the variable
        `LD_LIBRARY_PATH` was not sufficient for running the
        mex-binaries from MATLAB. Instead the following worked: Assuming
        you have root privilege (for Linux), edit the file
        `/etc/ld.so.conf` and add `/usr/local/lib` on its own line at
        the bottom. Do not remove anything from this file. Run
        `ldconfig` to tell the system to update the cache:

            sudo ldconfig

4.  Download and unzip owl. Download link: https://github.com/owl-toolkit/owl/releases/tag/release-21.0

5.  Install cpphoafparser (download and copy paste in the search path): https://automata.tools/hoa/cpphoafparser/

6. We have provided a small program called Simulate to simulate the synthesized controller. In order to use it, install python3.9 and matplotlib library and a GUI backend by running:

        sudo apt install python3.9
        curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
        python3.9 get-pip.py
        pip3 install matplotlib
        pip3 install -U Pillow
        pip3 install pyqt5

During simulation, in the example folder, set the PYTHONPATH as:

        export PYTHONPATH=/home/kmallik/Codes/mascot-sds/src:/home/kmallik/Codes/mascot-sds/examples/rabin_examples/vehicle_cfg_format

8.  Install clang-format to maintain code style
    for Mac OS use

        brew install clang-format.

    and for Linux use

        sudo apt-get install clang-format

    To reform file run:

        clang-format -i /path/to/file.cc

    or run formatting in your editor:

    -   Clion:  `Ctrl` + `Shift` + `Alt` + `L`
    -   XCode:  Select text (`Cmd` + `A`) and then `Ctrl` + `I`.
    -   Atom: After saving

9. How to run tests:

   (a) download google test repository

        git clone https://github.com/google/googletest.git

Directory Structure
===================

In the repository, you will see the following directory structure:

[//]: # (-   `./bdd/` Contains the C++ source code which use Binary Decision)
[//]: # (    Diagrams as the underlying data structure.)

-   `./contrib/` Contains license files of SCOTS and Mascot.

-   `./doc/` C++ Class documentation directory.

-   `./examples/adhs21/` Examples shown in the ADHS '21 paper; instructions for running the examples are included in the file `./examples/rabin_examples/bistable_switch/README.md`.

-   `./examples/rabin_examples/bistable_switch` An example based on an oscillator with Rabin specifications; instructions for running the examples are included in the file `./examples/rabin_examples/bistable_switch/README.md`.

-   `./examples/rabin_examples/experimental` Some additional examples with Rabin specifications.

-   `./examples/hscc20/` Examples shown in the HSCC '20 paper with the repeatability evaluation instructions.

-   `./examples/others/` Other examples.

-   `./manual/` SCOTS manual.

-   `./mfiles/` Contains some mfiles needed for running the simulations.

-   `./mfiles/mexfiles/` mex-file to read the C++ output from file.

-   `./utils/` Some C++ header files

How to Use
===================
See `./examples/hscc20/RE_manual_hscc20.pdf` for instructions to synthesize controller and plot trajectories for the examples which were presented in the HSCC '20 paper.

-->
